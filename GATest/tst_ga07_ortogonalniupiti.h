#ifndef TST_GA07_ORTOGONALNIUPITI_H
#define TST_GA07_ORTOGONALNIUPITI_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga07_rangetree.h"
#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga07_ortogonalni_upiti.h"

using namespace testing;

// Pomocne funkcije

void proveriTacku(QPoint t1, QPoint t2)
{
    EXPECT_EQ(t1.x(), t2.x());
    EXPECT_EQ(t1.y(), t2.y());
};

void sortirajTacke(QVector<QPoint> &t)
{
    std::sort(std::begin(t), std::end(t), [](QPoint l, QPoint r) {
        return l.x() < r.x() || (l.x() == r.x() && l.y() < r.y());
    });
}

// Skup jedinicnih testova

TEST(ga07_rangetree, prazno_stablo_greska)
{
    QVector<QPoint> t;
    ASSERT_DEATH({ RangeTree stablo(t); }, "!tacke.isEmpty()");
}

TEST(ga07_rangetree, jednoclano_stablo)
{
    // Stablo sa jednim cvorom i opsegom koji je bas ta tacka
    QVector<QPoint> tacke{{5, 5}};
    RangeTree stablo{tacke};

    QVector<QPoint> prikupljeneTacke;
    int intervalX[2] = {-10, 10};
    int intervalY[2] = {-10, 10};
    stablo.sakupiTackeIzOpsega(intervalX, intervalY, prikupljeneTacke);

    EXPECT_EQ(prikupljeneTacke.size(), 1);
    proveriTacku(prikupljeneTacke.first(), {5, 5});
}

TEST(ga07_rangetree, testiraj_iste_koordinate)
{
    QVector<QPoint> tacke{{0, 10}, {2, 40}, {2, 30}, {4, 10}};
    RangeTree stablo{tacke};

    QVector<QPoint> prikupljeneTacke;
    int intervalX[2] = {1, 2};
    int intervalY[2] = {0, 50};
    stablo.sakupiTackeIzOpsega(intervalX, intervalY, prikupljeneTacke);

    EXPECT_EQ(prikupljeneTacke.size(), 2);

    sortirajTacke(prikupljeneTacke);
    proveriTacku(prikupljeneTacke[0], {2, 30});
    proveriTacku(prikupljeneTacke[1], {2, 40});
}

TEST(ga07_rangetree, testiraj_duplikate)
{
    QVector<QPoint> tacke{{0, 0}, {0, 0}};
    RangeTree stablo{tacke};

    QVector<QPoint> prikupljeneTacke;
    int intervalX[2] = {0, 0};
    int intervalY[2] = {0, 0};
    stablo.sakupiTackeIzOpsega(intervalX, intervalY, prikupljeneTacke);

    EXPECT_EQ(prikupljeneTacke.size(), 2);
    proveriTacku(prikupljeneTacke.first(), {0, 0});
    proveriTacku(prikupljeneTacke.first(), prikupljeneTacke.last());
}

TEST(ga07_rangetree, cvor_podele)
{
    QVector<QPoint> tacke{{137, 140}, {173, 426}, {200, 156}, {204, 319},
                          {234, 434}, {242, 236}, {327, 25}, {327, 253},
                          {327, 343}, {374, 95}, {511, 56}, {543, 187},
                          {580, 258}, {625, 545}, {638, 522}, {664, 349},
                          {668, 577}, {698, 140}, {713, 554}, {996, 401}};
    RangeTree stablo{tacke};

    int interval[2] = {713, 996};
    int ind = stablo.nadjiIndeksCvoraPodele(interval);
    EXPECT_EQ(ind, 12);
}

TEST(ga07_ortogonalni_upiti, isto_resenje_naivnog_i_optimalnog_FAJL)
{
    QString putanja1 = "../Geometrijski_Algoritmi/input_files/ga07_ortogonalniupiti_input1.txt";
    OrtogonalniUpiti upiti(nullptr, nullptr, 0, putanja1.toStdString(), 0, RAZLOG_UCITAVANJE);

    upiti.pokreniAlgoritam();
    QVector<QPoint> tacke1 = upiti.tackeIzOpsega();
    sortirajTacke(tacke1);

    upiti.pokreniNaivniAlgoritam();
    QVector<QPoint> tacke2 = upiti.tackeIzOpsega();
    sortirajTacke(tacke2);

    EXPECT_EQ(tacke1.size(), tacke2.size());
    for (int i = 0; i < tacke1.size(); ++i)
        proveriTacku(tacke1[i], tacke2[i]);
}

TEST(ga07_ortogonalni_upiti, isto_resenje_naivnog_i_optimalnog_RANDOM)
{
    OrtogonalniUpiti upiti(nullptr, nullptr, 0, "", 20, RAZLOG_NASUMICNE);

    upiti.pokreniAlgoritam();
    QVector<QPoint> tacke1 = upiti.tackeIzOpsega();
    sortirajTacke(tacke1);

    upiti.pokreniNaivniAlgoritam();
    QVector<QPoint> tacke2 = upiti.tackeIzOpsega();
    sortirajTacke(tacke2);

    EXPECT_EQ(tacke1.size(), tacke2.size());
    for (int i = 0; i < tacke1.size(); ++i)
        proveriTacku(tacke1[i], tacke2[i]);
}

#endif  // TST_GA07_ORTOGONALNIUPITI_H
