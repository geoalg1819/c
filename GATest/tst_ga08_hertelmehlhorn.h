#ifndef TST_GA08_HERTELMEHLHORN_H
#define TST_GA08_HERTELMEHLHORN_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga08_hertelmehlhorn.h"

using namespace testing;

void proveriKonveksnost(QVector<Poligon*> particije) {
    for (int i = 0; i < particije.size(); ++i) {
        if (particije[i] != nullptr)
            EXPECT_TRUE(particije[i]->konveksan());
    }
}

TEST(ga08_hertelmehlhorn, emptyPolygonTest)
{
    HertelMehlhorn* hm = new HertelMehlhorn(nullptr, nullptr, 0, "", 0);
    hm->pokreniAlgoritam();
    EXPECT_EQ(hm->particije().size(), 0);
}

TEST(ga08_hertelmehlhorn, convexPolygonTest)
{
    HertelMehlhorn* hm = new HertelMehlhorn(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/08_hertelmehlhorn_4.off");
    hm->pokreniAlgoritam();
    EXPECT_EQ(hm->brojParticija(), 1);
}

TEST(ga08_hertelmehlhorn, randomTest)
{
    HertelMehlhorn* hm = new HertelMehlhorn(nullptr, nullptr, 0, "", 100);
    hm->pokreniAlgoritam();
    proveriKonveksnost(hm->particije());
}

TEST(ga08_hertelmehlhorn, fileTest)
{
    HertelMehlhorn* hm = new HertelMehlhorn(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/08_hertelmehlhorn_2.off");
    hm->pokreniAlgoritam();
    EXPECT_EQ(hm->brojParticija(), 10);
}

#endif // TST_GA08_HERTELMEHLHORN_H
