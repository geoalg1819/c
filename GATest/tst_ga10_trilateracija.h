#ifndef TST_GA10_TRILATERACIJA_H
#define TST_GA10_TRILATERACIJA_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga10_trilateracija.h"

using namespace testing;

TEST(ga10_trilateracija, file1TestLat)
{
    Trilateracija *ch1 = new Trilateracija(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga10_trilateracija_input1.txt");
    double p1 = ch1->pokreniAlgoritamTest()[0];
    double p2 = ch1->pokreniAlgoritamTest()[1]; ;
    EXPECT_NEAR(p1, 44.8401, 0.0001);
}

TEST(ga10_trilateracija, file1TestLon)
{
    Trilateracija *ch1 = new Trilateracija(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga10_trilateracija_input1.txt");
    double p1 = ch1->pokreniAlgoritamTest()[1];
    double p2 = 20.3730;
    EXPECT_NEAR(p1, p2, 0.0001);
}

TEST(ga10_trilateracija, file2TestLat)
{
    Trilateracija *ch1 = new Trilateracija(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga10_trilateracija_input3.txt");
    double p1 = ch1->pokreniAlgoritamTest()[0];
    double p2 = 21.7546;
    EXPECT_NEAR(p1, p2, 0.0001);
}

TEST(ga10_trilateracija, file2TestLon)
{
    Trilateracija *ch1 = new Trilateracija(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga10_trilateracija_input3.txt");
    double p1 = ch1->pokreniAlgoritamTest()[1];
    double p2 = 77.9219;
    EXPECT_NEAR(p1, p2, 0.0001);
}

#endif // TST_GA10_TRILATERACIJA_H
