//#include "tst_ga02_konveksniomotac.h"
#include "tst_ga04_kdtree.h"
#include "tst_ga10_trilateracija.h"
#include "tst_ga02_konveksniomotac.h"
#include "tst_ga07_ortogonalniupiti.h"
#include "tst_ga08_hertelmehlhorn.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
