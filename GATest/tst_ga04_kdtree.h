#ifndef TST_GA04_KDTREE_H
#define TST_GA04_KDTREE_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga04_kdtree.h"

using namespace testing;

TEST(ga04_kdtree, file1Test)
{
    KdTree *tree=new KdTree(nullptr,nullptr,0,"../Geometrijski_Algoritmi/input_files/ga04_kdtree_input1.txt",0,2);
    std::vector<QPoint> p1=tree->pokreniAlgoritamBenchmark();
    std::vector<QPoint> p2=tree->pokreniNaivniAlgoritamBenchmark();
    for (int i=0;i<p1.size();i++) {
        std::cout << "poredim x1:" << p1[i].x() << " i x2: " << p2[i].x() << std::endl;
        EXPECT_EQ(p1[i].x(),p2[i].x());
        std::cout << "poredim x1:" << p1[i].y() << " i y2: " << p2[i].y() << std::endl;
        EXPECT_EQ(p1[i].y(),p2[i].y());
    }

}

TEST(ga04_kdtree, file2Test)
{
    KdTree *tree=new KdTree(nullptr,nullptr,0,"../Geometrijski_Algoritmi/input_files/ga04_kdtree_input2.txt",0,3);
    std::vector<QPoint> p1=tree->pokreniAlgoritamBenchmark();
    std::vector<QPoint> p2=tree->pokreniNaivniAlgoritamBenchmark();
    for (int i=0;i<p1.size();i++) {
        std::cout << "poredim x1:" << p1[i].x() << " i x2: " << p2[i].x() << std::endl;
        EXPECT_EQ(p1[i].x(),p2[i].x());
        std::cout << "poredim x1:" << p1[i].y() << " i y2: " << p2[i].y() << std::endl;
        EXPECT_EQ(p1[i].y(),p2[i].y());
    }
}

TEST(ga04_kdtree, file3Test)
{
    KdTree *tree=new KdTree(nullptr,nullptr,0,"../Geometrijski_Algoritmi/input_files/ga04_kdtree_input3.txt",0,4);
    std::vector<QPoint> p1=tree->pokreniAlgoritamBenchmark();
    std::vector<QPoint> p2=tree->pokreniNaivniAlgoritamBenchmark();
    for (int i=0;i<p1.size();i++) {
        std::cout << "poredim x1:" << p1[i].x() << " i x2: " << p2[i].x() << std::endl;
        EXPECT_EQ(p1[i].x(),p2[i].x());
        std::cout << "poredim x1:" << p1[i].y() << " i y2: " << p2[i].y() << std::endl;
        EXPECT_EQ(p1[i].y(),p2[i].y());
    }
}

TEST(ga04_kdtree, randomTest100)
{
    KdTree *tree=new KdTree(nullptr,nullptr,0,"",100,3);
    std::vector<QPoint> p1=tree->pokreniAlgoritamBenchmark();
    std::vector<QPoint> p2=tree->pokreniNaivniAlgoritamBenchmark();
    for (int i=0;i<p1.size();i++) {
        std::cout << "poredim x1:" << p1[i].x() << " i x2: " << p2[i].x() << std::endl;
        EXPECT_EQ(p1[i].x(),p2[i].x());
        std::cout << "poredim x1:" << p1[i].y() << " i y2: " << p2[i].y() << std::endl;
        EXPECT_EQ(p1[i].y(),p2[i].y());
    }
}

TEST(ga04_kdtree, randomTest5)
{
    KdTree *tree=new KdTree(nullptr,nullptr,0,"",5,2);
    std::vector<QPoint> p1=tree->pokreniAlgoritamBenchmark();
    std::vector<QPoint> p2=tree->pokreniNaivniAlgoritamBenchmark();
    for (int i=0;i<p1.size();i++) {
        std::cout << "poredim x1:" << p1[i].x() << " i x2: " << p2[i].x() << std::endl;
        EXPECT_EQ(p1[i].x(),p2[i].x());
        std::cout << "poredim x1:" << p1[i].y() << " i y2: " << p2[i].y() << std::endl;
        EXPECT_EQ(p1[i].y(),p2[i].y());
    }
}



#endif // TST_GA04_KDTREE_H
