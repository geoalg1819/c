#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../Geometrijski_Algoritmi/algoritmi_sa_vezbi/ga02_konveksniomotac.h"

using namespace testing;

TEST(ga02_konveksniomotac, GATesst)
{
    EXPECT_EQ(1, 1);
    ASSERT_THAT(0, Eq(0));
}

TEST(ga02_konveksniomotac, firstRandomTest)
{
    KonveksniOmotac* ch1 = new KonveksniOmotac(nullptr, nullptr, 0, "", 30);
    ch1->pokreniAlgoritam();
    ch1->pokreniNaivniAlgoritam();
    EXPECT_EQ(ch1->konveksniOmotac().size()-1, ch1->konveksniOmotacNaivni().size());
}

TEST(ga02_konveksniomotac, secondRandomTest)
{
    KonveksniOmotac* ch1 = new KonveksniOmotac(nullptr, nullptr, 0, "", 100);
    ch1->pokreniAlgoritam();
    ch1->pokreniNaivniAlgoritam();
    EXPECT_EQ(ch1->konveksniOmotac().size()-1, ch1->konveksniOmotacNaivni().size());
}

TEST(ga02_konveksniomotac, thirdRandomTest)
{
    KonveksniOmotac* ch1 = new KonveksniOmotac(nullptr, nullptr, 0, "", 200);
    ch1->pokreniAlgoritam();
    ch1->pokreniNaivniAlgoritam();
    EXPECT_EQ(ch1->konveksniOmotac().size()-1, ch1->konveksniOmotacNaivni().size());
}



