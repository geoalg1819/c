#ifndef TST_GA02_UNIJAPRAVOUGAONIKA_H
#define TST_GA02_UNIJAPRAVOUGAONIKA_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../Geometrijski_Algoritmi/algoritmi_studentski_projekti/ga02_unija_pravougaonika.h"

using namespace testing;

TEST(ga02_unija_pravougaonika, file1Test)
{
    UnijaPravougaonika *ch1 = new UnijaPravougaonika(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga02_unijapravougaonika_input1.txt", 0);
    int p1 = ch1->pokreniAlgoritamBenchmark();
    int p2 = ch1->pokreniNaivniAlgoritamBenchmark();
    EXPECT_EQ(p1, p2);
}

TEST(ga02_unija_pravougaonika, file2Test)
{
    UnijaPravougaonika *ch1 = new UnijaPravougaonika(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga02_unijapravougaonika_input2.txt", 0);
    int p1 = ch1->pokreniAlgoritamBenchmark();
    int p2 = ch1->pokreniNaivniAlgoritamBenchmark();
    EXPECT_EQ(p1, p2);
}

TEST(ga02_unija_pravougaonika, file3Test)
{
    UnijaPravougaonika *ch1 = new UnijaPravougaonika(nullptr, nullptr, 0, "../Geometrijski_Algoritmi/input_files/ga02_unijapravougaonika_input3.txt", 0);
    int p1 = ch1->pokreniAlgoritamBenchmark();
    int p2 = ch1->pokreniNaivniAlgoritamBenchmark();
    EXPECT_EQ(p1, p2);
}

TEST(ga02_unija_pravougaonika, randomTest5)
{
    UnijaPravougaonika *ch1 = new UnijaPravougaonika(nullptr, nullptr, 0, "", 5);
    int p1 = ch1->pokreniAlgoritamBenchmark();
    int p2 = ch1->pokreniNaivniAlgoritamBenchmark();
    EXPECT_EQ(p1, p2);
}

TEST(ga02_unija_pravougaonika, randomTest100)
{
    UnijaPravougaonika *ch1 = new UnijaPravougaonika(nullptr, nullptr, 0, "", 100);
    int p1 = ch1->pokreniAlgoritamBenchmark();
    int p2 = ch1->pokreniNaivniAlgoritamBenchmark();
    EXPECT_EQ(p1, p2);
}


#endif // TST_GA02_UNIJAPRAVOUGAONIKA_H
