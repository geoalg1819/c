#ifndef CONFIG_H
#define CONFIG_H

//#define SKIP_OPTIMAL
//#define SKIP_NAIVE

//Measurement testing params
#define MIN_DIM         (3)
#define STEP            (100)
#define MAX_DIM         (1000)

//Axes params
#define Y_MAX_VAL       (1)
#define X_MAX_VAL       (MAX_DIM)

//Velicina kanvasa
#define CANVAS_WIDTH    (MAX_DIM)
#define CANVAS_HEIGHT   (MAX_DIM)

#endif // CONFIG_H
