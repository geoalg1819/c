#ifndef GA04_KDTREE_H
#define GA04_KDTREE_H

#include "algoritambaza.h"
#include <vector>
#include <list>
#include <utility>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <string>
#include <QDebug>

class KdTreeNode
{
public:
    KdTreeNode* left= nullptr;
    KdTreeNode* right= nullptr;
    QPoint p;
    QPoint dole,gore;

    KdTreeNode() = default;
    KdTreeNode(QPoint d, QPoint g);
    ~KdTreeNode();
};

class KdTree : public AlgoritamBaza
{
public:
    KdTree(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "",int broj_tacaka = BROJ_NASUMICNIH_TACAKA,int n = 1, QPoint point = QPoint(600,600));
    KdTreeNode* getRoot() const {return _root;}
    std::vector<QPoint> pokreniAlgoritamBenchmark();
    void pokreniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajStablo(QPainter& painter,KdTreeNode* root, int axis) const;
    void pokreniNaivniAlgoritam();
    std::vector<QPoint> pokreniNaivniAlgoritamBenchmark();
    virtual void crtajAlgoritam3D() const;
    bool is_3D() const;

    void printTree(KdTreeNode* head);
    QPoint median(int axis, std::vector<QPoint> &plist
                                      , std::vector<QPoint> &left
                                      , std::vector<QPoint> &right);

    void makeTree(std::vector<QPoint> &plist);

private:
    void pokreniAlgoritamBenchmarkHelper(KdTreeNode* head,int axis);
    void pokreniAlgoritamHelper(KdTreeNode* head,int axis);
    void makeTreeHelper(KdTreeNode* head, std::vector<QPoint> &plist, QPoint d, QPoint g, int depth);
    void benchmark();
    std::vector<QPoint> _tacke;
    int _n;
    std::vector<QPoint> trazeneTacke;
    std::vector<QPoint> trazeneTackeNaivni;
    QPoint trazena;
    KdTreeNode* _root;
};

#endif // GA04_KDTREE_H
