#ifndef GA00_PRAZANPROJEKAT_H
#define GA00_PRAZANPROJEKAT_H

#include "algoritambaza.h"

class PrazanProjekat : public AlgoritamBaza
{
public:
    PrazanProjekat(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "");

    void pokreniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    virtual void crtajAlgoritam3D() const;
    bool is_3D() const;
};

#endif // GA00_PRAZANPROJEKAT_H
