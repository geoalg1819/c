#include "ga08_hertelmehlhorn.h"
#include "pomocnefunkcije.h"

#include <fstream>
#include <iostream>
#include <QPainterPath>
#include <map>
#include <cmath>

#define MAX_TRANSP 255
#define EPS 1e-6

Poligon::Poligon()
{
    _temena = new QVector<QPoint>();
    _duzi = QVector<Duz>();
}

Poligon::~Poligon()
{

}

Poligon::Poligon(int brTemena)
    : Poligon()
{
    if (brTemena == 0) {
        _temena->clear();
        _duzi.clear();
    }
}

Poligon::Poligon(const Poligon &p)
{
    if (p.brTemena() > 0) {
        _temena = p._temena;
        _duzi = p._duzi;
    }
    else {
        _temena = nullptr;
    }
}

Poligon::Poligon(QVector<QPoint> *temena)
{
    // FIXME
    _temena = temena;

    for (int i = 0; i < brTemena(); ++i) {
        _duzi.push_back(Duz((*_temena)[i],
                            (*_temena)[(i + 1) % brTemena()]));
    }
}

int Poligon::brTemena() const
{
    if (_temena != nullptr)
        return _temena->size();
    return 0;
}

void Poligon::dodajTeme(QPoint p)
{
    _temena->push_back(p);
}

int Poligon::indeksDuzi(const QPoint &a, const QPoint &b) const
{
    int ind = _duzi.indexOf(Duz(a, b));
    if (ind == -1)
        ind = _duzi.indexOf(Duz(b, a));
    return ind;
}

int Poligon::indeksDuzi(const Duz &s) const
{
    return _duzi.indexOf(s);
}

void Poligon::dodajIncidentniPoligon(int i, Poligon *p)
{
    _duzi[i].dodajIncidentniPoligon(p);
}

bool Poligon::konveksan() const
{
    for (int i = 0; i < brTemena(); ++i)
        if (!Poligon::ccw((*_temena)[i % brTemena()],
                          (*_temena)[(i + 1) % brTemena()],
                          (*_temena)[(i + 2) % brTemena()]))
            return false;

    return true;
}

bool Poligon::sused(const Poligon &p) const
{
    for (int i = 0; i < _duzi.size(); ++i) {
        if (p._duzi.contains(_duzi[i]))
            return true;
    }
    return false;
}

void Poligon::sortirajDuzi()
{
    QVector<Duz> tmp;
    QVector<bool> dodata(_duzi.size()); // Inicijalno false
    tmp.push_back(_duzi[0]);

    for (int i = 0; i < _duzi.size(); ++i)
        for (int j = 0; j < _duzi.size(); ++j) {
            if (i == j)
                continue;
            if (dodata[j] == false && _duzi[j].a() == _duzi[i].b()) {
                tmp.push_back(_duzi[j]);
                dodata[j] = true;
            }
        }

    int ind = dodata.indexOf(false);
    if (ind != -1)
        tmp.push_back(_duzi[ind]);

    _duzi = tmp;
}

Poligon Poligon::spoji(Poligon &p)
{
    Poligon noviPoligon(0);

    for (int i = 0; i < _temena->size(); ++i) {
        QPoint a((*_temena)[(i + 1) % brTemena()]);
        QPoint b((*_temena)[i]);
        int j = p.temena()->indexOf(a);
        if (j == -1)
            continue;

        // Pomocne tacke za proveravanje konveksnosti
        QPoint c((*_temena)[(i + brTemena() - 1) % brTemena()]);
        QPoint d(p.teme((j + 2) % p.brTemena()));
        if (!Poligon::ccw(c, b, d))
            continue;

        c = QPoint((*_temena)[(i + 2) % brTemena()]);
        d = QPoint(p.teme((j + p.brTemena() - 1) % p.brTemena()));
        if (!Poligon::ccw(d, a, c))
            continue;

        noviPoligon = Poligon(brTemena() + p.brTemena() - 2);

        int k = (i + 1) % brTemena();
        int ind = 0;
        while (k != i) {
            if ((*_temena)[k] != QPoint(0, 0)) {
                noviPoligon.dodajTeme((*_temena)[k]);
                noviPoligon.dodajDuz(_duzi[k]);
                noviPoligon.dodajIncidentniPoligon(ind++, _duzi[k].incidentniPoligon());
            }
            k = (k + 1) % brTemena();
        }

        k = (j + 1) % p.brTemena();
        while (k != j) {
            if (p.teme(k) != QPoint(0, 0)) {
                    noviPoligon.dodajTeme(p.teme(k));
                    noviPoligon.dodajDuz(p.duz(k));
                    noviPoligon.dodajIncidentniPoligon(ind++, p.duz(k).incidentniPoligon());
            }
            k = (k + 1) % p.brTemena();
        }
    }

    return noviPoligon;
}

bool Poligon::imaTeme(const QPoint &a) const
{
    if (_temena->indexOf(a) == -1)
        return false;
    return true;
}

void Poligon::dodajDuz(QPoint a, QPoint b)
{
    Duz s(a, b);
    _duzi.push_back(s);
}


void Poligon::dodajDuz(Duz d)
{
    _duzi.push_back(d);
}

Poligon &Poligon::operator=(const Poligon &p)
{
    _temena->clear();
    _duzi.clear();

    if (p.brTemena() > 0) {
        _temena = p._temena;
        _duzi = p._duzi;
    }

    return *this;
}

bool Poligon::operator==(const Poligon &p) const
{
    if (p.brTemena() == 0 || p.brTemena() != brTemena())
        return false;

    int j = _temena->indexOf(p[0]);
    if (j == -1)
        return false;

    for (int i = 0; i < p.brTemena(); i =(j + 1) % brTemena()) {
        if ((*_temena)[j] != p[i])
            return false;
    }

    return true;
}

bool Poligon::ccw(const QPoint &a, const QPoint &b, const QPoint &c)
{
    if (((c.y() - a.y()) * (b.x() - a.x()) -
         (c.x() - a.x()) * (b.y() - a.y())) > 0)
        return true;
    return false;
}

QPoint Poligon::ekstremnaTacka(const std::vector<QPoint> &tacke)
{
    QPoint min = tacke[0];
    for (unsigned i = 0; i < tacke.size(); ++i) {
        if (tacke[i].x() < min.x() ||
                (tacke[i].x() == min.x() && tacke[i].y() < min.y()))
            min = tacke[i];
    }
    return min;
}

/**
 * @brief HertelMehlhorn::HertelMehlhorn
 * Ucitavanje triangulacije iz datoteke koja je u izmenjenom OFF formatu.
 * Izmena se ogleda u dodatoj sekciji na kraju datoteke, koja sluzi
 * za opisivanje poligona pre nego sto je izvrsena triangulacija.
 *
 * @note Nasumicno biranje tacaka je onemoguceno.
 * @param pCrtanje
 * @param pCrtanjeGL
 * @param pauzaKoraka
 * @param imeDatoteke
 */
HertelMehlhorn::HertelMehlhorn(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL,
                               int pauzaKoraka, std::__cxx11::string imeDatoteke,
                               int brojTacaka)
    : AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{
    if (imeDatoteke != "") {
        std::ifstream ulaz(imeDatoteke);
        std::string format;
        ulaz >> format;
        if (format.compare("OFF") != 0) {
            std::cout << "Pogresan format datoteke " << imeDatoteke << std::endl;
            exit(EXIT_FAILURE);
        }

        int brCvorova, brStranica, brRegiona;
        ulaz >> brCvorova >> brRegiona >> brStranica;
        QVector<QPoint> cvorovi;
        for (int i = 0; i < brCvorova; ++i) {
            int x, y, z;
            ulaz >> x >> y >> z;
            cvorovi.push_back(QPoint(x, y));
        }

        for (int i = 0; i < brRegiona; ++i) {
            int n;
            ulaz >> n;
            Poligon *p = new Poligon(n);
            for (int j = 0; j < n; ++j) {
                int cvorInd;
                ulaz >> cvorInd;
                p->dodajTeme(cvorovi[cvorInd]);
            }
            for (int j = 0; j < n; ++j)
                p->dodajDuz(p->teme(j), p->teme((j + 1) % n));

            _triangulacija.push_back(p);
        }

        _poligon = new Poligon(brCvorova);
        for (int i = 0; i < brCvorova; ++i) {
            int cvorInd;
            ulaz >> cvorInd;
            _poligon->dodajTeme(cvorovi[cvorInd]);
        }
    }
    else {
        if (brojTacaka == 0) {
            _particije.clear();
            _poligon = nullptr;
            return;
        }
        _poligon = new Poligon(brojTacaka);
        generisiTriangulaciju(generisiNasumicneTacke(brojTacaka));
    }

    // Azuriranje informacije o stranicama i njihovim incidentnim poligonima
    for (int i = 0; i < _triangulacija.size(); ++i)
        for (int j = 0; j < _triangulacija[i]->duzi().size(); ++j) {
            Duz tmp = _triangulacija[i]->duzi()[j];
            for (int k = i + 1; k < _triangulacija.size(); ++k) {
                int ind = _triangulacija[k]->indeksDuzi(tmp);
                if (ind == -1)
                    continue;
                _triangulacija[i]->dodajIncidentniPoligon(j, _triangulacija[k]);
                _triangulacija[k]->dodajIncidentniPoligon(ind, _triangulacija[i]);
            }
        }
}

HertelMehlhorn::~HertelMehlhorn()
{

}

void HertelMehlhorn::particionisanje()
{
    _particije = _triangulacija;
    for (int p = 0; p < _particije.size(); ++p) {
        if (_particije[p] == nullptr)
            continue;

        Poligon poligon1 = *_particije[p];
        int brTemena1 = poligon1.brTemena();

        for (int i = 0; i < poligon1.duzi().size(); ++i) {
            if (poligon1.duz(i).incidentniPoligon() == nullptr)
                continue;

            Poligon *poligon2 = poligon1.duz(i).incidentniPoligon();
            int q = indeksParticije(poligon2);
            if (q == -1 || _particije[q] == nullptr || q < p)
                continue;

            int brTemena2 = poligon2->brTemena();
            QPoint a(poligon1[(i + 1) % brTemena1]);
            QPoint b(poligon1[i]);
            int j = poligon2->temena()->indexOf(a);
            if (j == -1)
                continue;

            // Pomocne tacke za proveravanje konveksnosti
            QPoint c(poligon1[(i + brTemena1 - 1) % brTemena1]);
            QPoint d(poligon2->teme((j + 2) % brTemena2));
            if (!Poligon::ccw(c, b, d))
                continue;

            c = QPoint(poligon1[(i + 2) % brTemena1]);
            d = QPoint(poligon2->teme((j + brTemena2 - 1) % brTemena2));
            if (!Poligon::ccw(d, a, c))
                continue;

            Poligon *noviPoligon = new Poligon(brTemena1 + brTemena2 - 2);

            int k = (i + 1) % brTemena1;
            int ind = 0;
            while (k != i) {
                noviPoligon->dodajTeme(poligon1[k]);
                noviPoligon->dodajDuz(poligon1.duz(k));
                noviPoligon->dodajIncidentniPoligon(ind++, poligon1.duz(k).incidentniPoligon());
                k = (k + 1) % brTemena1;
            }

            k = (j + 1) % brTemena2;
            while (k != j) {
                noviPoligon->dodajTeme(poligon2->teme(k));
                noviPoligon->dodajDuz(poligon2->duz(k));
                noviPoligon->dodajIncidentniPoligon(ind++, poligon2->duz(k).incidentniPoligon());
                k = (k + 1) % brTemena2;
            }

            AlgoritamBaza_updateCanvasAndBlock();
            _particije[q] = nullptr;
            _particije[p]->temena()->resize(noviPoligon->brTemena());
            _particije[p]->duzi().resize(noviPoligon->brTemena());
            _particije[p] = noviPoligon;
            p--;
            break;
        }
    }
    brojParticija();
}

void HertelMehlhorn::pokreniAlgoritam()
{
    particionisanje();

    AlgoritamBaza_updateCanvasAndBlock();
    emit animacijaZavrsila();
}

void HertelMehlhorn::pokreniNaivniAlgoritam()
{
    _particijeNaivno = _triangulacija;
    for (int p = 0; p < _particijeNaivno.size(); ++p) {
        if (_particijeNaivno[p] == nullptr)
            continue;

        for (int q = 0; q < _particijeNaivno.size(); ++q) {
            if (p == q)
                continue;
            if (_particijeNaivno[q] == nullptr || _particijeNaivno[p] == nullptr)
                continue;

            if (_particijeNaivno[p]->sused(*_particijeNaivno[q])) {
                Poligon* ptmp = new Poligon(_particijeNaivno[p]->spoji(*_particijeNaivno[q]));
                if (ptmp == nullptr || ptmp->brTemena() == 0) // Nije moguce napraviti konveksan poligon
                    continue;

                _particijeNaivno[p]->temena()->resize(ptmp->brTemena());
                _particijeNaivno[p]->duzi().resize(ptmp->brTemena());
                _particijeNaivno[p] = ptmp;
                _particijeNaivno[q] = nullptr;
            }
        }
    }
}

void HertelMehlhorn::crtajAlgoritam(QPainter &painter) const
{
    QPen red = painter.pen();
    red.setColor(Qt::darkRed);
    red.setWidth(9);

    QPen gray = painter.pen();
    gray. setColor(Qt::darkGray);
    gray.setWidth(1);

    painter.setPen(gray);
    for (Poligon* p: _particije) {
        if (p == nullptr)
            continue;
        for (int i = 0; i < p->brTemena(); ++i) {
            painter.drawLine(p->teme(i), p->teme((i + 1) % p->brTemena()));
        }
    }

    // Crtanje particija
    QVector<int> transp(_particije.size());
    for (int i = 0; i < _particije.size(); ++i)
        transp[i] = (i + 1) * MAX_TRANSP / _particije.size();

    int j = -1;
    for (auto p : _particije) {
        j++;
        if (p == nullptr || p->brTemena() == 0)
            continue;
        QPolygonF poligon(*p->temena());
        QPainterPath path(poligon[0]);
        path.addPolygon(poligon);
        painter.fillPath(path, QBrush(QColor(104, 104, 104, transp[j])));
    }

    //    Crtanje poligona i njegovih temena
    gray.setWidth(3);
    for (int i = 0; i < _poligon->brTemena(); ++i) {
        painter.setPen(gray);
        painter.drawLine(_poligon->teme(i),
                         _poligon->teme((i + 1) % _poligon->brTemena()));
        painter.setPen(red);
        painter.drawPoint(_poligon->teme(i));
    }
}

void HertelMehlhorn::crtajAlgoritam3D() const
{

}

bool HertelMehlhorn::is_3D() const
{
    return false;
}

int HertelMehlhorn::brojParticija() const
{
    int brojac = 0;
    for (auto p : _particije)
        if (p != nullptr)
            brojac++;

    return brojac;
}

int HertelMehlhorn::indeksParticije(Poligon *p)
{
    int i;
    for (i = 0; i < _particije.size(); ++i) {
        if (_particije[i] == nullptr || _particije[i]->brTemena() == 0)
            continue;

        int j;
        for (j = 0; j < p->brTemena(); ++j)
            if (!_particije[i]->temena()->contains(p->teme(j)))
                break;

        if (j == p->brTemena())
            return i;
    }
    return -1;
}

void HertelMehlhorn::generisiTriangulaciju(std::vector<QPoint> tacke)
{
    QPoint t = Poligon::ekstremnaTacka(tacke);

    auto cmp = [t](QPoint &a, QPoint &b) {
        if (a.x() == t.x() && b.x() == t.x())
            return a.x() < b.x();
        if (a.x() == t.x()) return true;
        if (b.x() == t.x()) return false;
        double k1 = (a.y() - t.y()) * 1.0 / (a.x() - t.x());
        double k2 = (b.y() - t.y()) * 1.0 / (b.x() - t.x());
        if (std::fabs(k1 - k2) < EPS)
            return a.y() < b.y();
        return k1 < k2;
    };

    tacke.erase(std::find(tacke.begin(), tacke.end(), t));
    std::sort(tacke.begin(), tacke.end(), cmp);

    for (unsigned i = 0; i < tacke.size(); ++i)
        _poligon->dodajTeme(tacke[i]);
    _poligon->dodajTeme(t);

    for (int i = 0; i < _poligon->brTemena() - 1; ++i)
        _poligon->dodajDuz(_poligon->teme(i), _poligon->teme(i + 1));
    _poligon->dodajDuz(_poligon->teme(_poligon->brTemena() - 1),
                       _poligon->teme(0));

    _triangulacija.clear();
    for (int i = 0; i < _poligon->brTemena() - 2; ++i) {
        QVector<QPoint> *trougao = new QVector<QPoint>();
        trougao->push_back(_poligon->teme(_poligon->brTemena() - 1));
        trougao->push_back(_poligon->teme(i));
        trougao->push_back(_poligon->teme(i + 1));
        _triangulacija.push_back(new Poligon(trougao));
    }
}

Duz::Duz()
{
    _incidentniPoligon = nullptr;
}

Duz::Duz(QPoint a, QPoint b)
    : _a(a), _b(b)
{
    _incidentniPoligon = nullptr;
}

QPair<QPoint, QPoint> Duz::temena()
{
    return QPair<QPoint, QPoint>(_a, _b);
}

Poligon* Duz::incidentniPoligon()
{
    return _incidentniPoligon;
}

void Duz::dodajIncidentniPoligon(Poligon *p)
{
    _incidentniPoligon = p;
}

bool Duz::operator==(const Duz &s) const
{
    if ((s.a() == _a && s.b() == _b) ||
            (s.a() == _b && s.b() == _a))
        return true;
    return false;
}
