#ifndef ORTOGONALNI_UPITI_H
#define ORTOGONALNI_UPITI_H

#include "algoritambaza.h"

class RangeTree;
class QRubberBand;
class QStatusBar;
class QLineEdit;

class OrtogonalniUpiti : public AlgoritamBaza {
  public:
    OrtogonalniUpiti(QWidget *pCrtanje,
                     QOpenGLWidget *pCrtanjeGL,
                     int pauzaKoraka,
                     std::string imeDatoteke = "",
                     int brojTacaka = BROJ_NASUMICNIH_TACAKA,
                     RazlogPravljenjaAlg razlog = RAZLOG_ISPOCETKA);
    ~OrtogonalniUpiti();

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter &painter) const;

    const QVector<QPoint> tackeIzOpsega() const;
    void azurirajPravougaonik();
    void napraviQRubberBand();
    void anulirajQRubberBand();

    virtual void crtajAlgoritam3D() const {}
    bool is_3D() const { return false; }

  private:
    bool ucitajTackeIzDatoteke(QString imeDatoteke);
    void upozorenje(QString naslov, QString poruka);

    QVector<QPoint> m_tacke = {};
    RangeTree *m_stablo{nullptr};

    mutable QMatrix m_transformacija;
    QPoint m_prvoTeme{}, m_drugoTeme{};      // prave (painter) koordinate
    QPoint m_prvoTemeQt{};                   // Qt koordinate
    QRubberBand *m_selektovanje{nullptr};
    QVector<QPoint> m_tackeIzOpsega = {};

    static bool mousePressed;

    bool eventFilter(QObject *obj, QEvent *event);

    enum KOORD {
        KOORD_X = 0,
        KOORD_Y = 1,
        KOORD_DIM = 2,
    };
    static constexpr int _brojTacaka = 2;
    QLineEdit *m_lineEdits[_brojTacaka][KOORD_DIM];
    QStatusBar *m_statusBar{nullptr};
};
#endif  // ORTOGONALNI_UPITI_H
