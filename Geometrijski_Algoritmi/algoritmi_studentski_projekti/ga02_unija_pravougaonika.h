#ifndef GA02_UNIJA_PRAVOUGAONIKA_H
#define GA02_UNIJA_PRAVOUGAONIKA_H

#include "algoritambaza.h"
#include <set>

struct StranicaDogadjaja
{
    const QRect *p;
    bool begin;

    int y() const {
        return this->begin ? this->p->y() : this->p->y() + p->height();
    }
};

struct StranicaDogadjajaComp
{
    bool operator()(const StranicaDogadjaja t1, const StranicaDogadjaja t2) const
    {
        return (t1.y() < t2.y()) || (t1.y() == t2.y() && t1.p->x() <= t2.p->x());
    }
};

struct RectComp
{
    bool operator()(const QRect *p1, const QRect *p2) const
    {
        return (p1->y() + p1->height() < p2->y() + p2->height()) || (p1->y() + p1->height() == p2->y() + p2->height() && p1->x() <= p2->x());
    }
};


class UnijaPravougaonika : public AlgoritamBaza
{
public:
    UnijaPravougaonika(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    int pokreniAlgoritamBenchmark();
    void crtajAlgoritam(QPainter& painter) const;
    void pokreniNaivniAlgoritam();
    int pokreniNaivniAlgoritamBenchmark();
    void crtajAlgoritam3D() const;
    bool is_3D() const;

private:
    struct Node
    {
    public:
        int value;
        int other;
        int max;
        int min;
        int leftmin;
        int rightmax;
        int measure;
        bool isLeft;
        Node *left;
        Node *right;
    };

    std::vector<QRect> pravougaonici;
    int y;
    int updatedP;
    int P;

    std::set<StranicaDogadjaja, StranicaDogadjajaComp> dogadjaji;
    std::set<const QRect*, RectComp> status;
    Node *T;

    void ucitajPraougaonikeIzDatoteke(std::string imeDatoteke);
    void generisiNasumicnePravougaonike(int broj_duzi);

    void dodajUStatus(const QRect *r);
    bool uStatusu(const QRect *r) const;
    void izbrisiIzStatusa(const QRect *r);
    int statusUnion() const;

    void insert(const QRect *r);
    void remove(const QRect *r);
    Node* insert(Node *node, int x1, int x2);
    Node* remove(Node *node, int x1, int x2);
    Node* insert(Node *node, int v, int other, bool isLeft);
    Node* remove(Node *node, int v);
    void update(Node* node);
    void printT(const Node *node) const;
    bool subtreeContainsPoint(const Node *node, int v) const;
    bool find(const Node *node, int v) const;
    Node* findMin(Node *node) const;
    int getUnion(const Node *node) const;
    int getRightMax(const Node *node, const UnijaPravougaonika::Node *alt = nullptr) const;
    int getLeftMin(const Node *node, const UnijaPravougaonika::Node *alt = nullptr) const;
    int getMax(const Node *node, const UnijaPravougaonika::Node *alt = nullptr) const;
    int getMin(const Node *node, const UnijaPravougaonika::Node *alt = nullptr) const;

    void benchmark();

    int getRandomInt() const;
    void reset();
};


#endif // GA02_UNIJA_PRAVOUGAONIKA_H
