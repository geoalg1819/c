#include "ga01_triangulacija.h"
#include <random>
#include <cmath>

QString Triangulacija2::tipAlgoritma = "Triangulacija";

Triangulacija2::Triangulacija2(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke,int nasumicnih, bool koristiNaivni)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka),
      _koristiNaivni{koristiNaivni},
      _iscrtajDcellInstant{true},
      _maxTemeZaIscrtavanje {0},
      _poslednjaDodataIvica {nullptr},
      _greska{""},
      _poljeZaObradu{nullptr}
{

    std::vector<QPoint> tacke;

    if("" != imeDatoteke){tacke = ucitajPodatkeIzDatoteke(imeDatoteke);}
    else{ kreirajNasumicneTacke(nasumicnih, tacke);}

    if(tacke.size() < 3)
    {
        throw "Mora biti barem cetiri tacke.";
    }

    _polygon = DCEL{tacke};
    _iviceInicijalnogPoligona = _polygon.edges();
}

void Triangulacija2::kreirajNasumicneTacke(int nasumicnih, std::vector<QPoint> &outTacke)
{
    constexpr int dimenzija = MAXDIM;
    constexpr int ofsetNaKraju = MAXDIM / 10;

    if(nasumicnih < 3)
    {
        throw "Mora biti dato barem tri tacke";
    }

    std::random_device r;
    std::default_random_engine e(r());
    std::uniform_real_distribution<double> uniformniProcenti(0.2, 0.8);
    double raspodela = uniformniProcenti(e);
    int gornjihTacaka = std::max<int>(1, static_cast<int>(raspodela * nasumicnih));
    if(gornjihTacaka == nasumicnih)
    {
        --gornjihTacaka;
    }

    int donjihTacaka = nasumicnih - gornjihTacaka;

    if(gornjihTacaka > dimenzija ||
       donjihTacaka > dimenzija ||
       donjihTacaka < 1 ||
       gornjihTacaka + donjihTacaka < 3)
    {
        throw "Neadekvatan broj tacaka";
    }

    std::uniform_int_distribution<int> uniformniOfsetZaY(1, dimenzija / 2);
    std::uniform_int_distribution<int> uniformniOfsetZaXGornje(1, dimenzija / gornjihTacaka);
    std::uniform_int_distribution<int> uniformniOfsetZaXDonje(1, dimenzija / donjihTacaka);

    int prethodniX;
    outTacke.clear();
    outTacke.push_back(QPoint(ofsetNaKraju, dimenzija / 2 + ofsetNaKraju));
    prethodniX = 0;
    for(int i = 1; i < donjihTacaka; ++i)
    {
        int x = prethodniX + uniformniOfsetZaXDonje(e);
        prethodniX = x;
        int y = (dimenzija/2) - uniformniOfsetZaY(e);
        outTacke.push_back(QPoint(x + ofsetNaKraju, y + ofsetNaKraju));
    }

    outTacke.push_back(QPoint(dimenzija + ofsetNaKraju, dimenzija / 2 + ofsetNaKraju));
    prethodniX = dimenzija;
    for(int i = 1; i < gornjihTacaka; ++i)
    {
        int x = prethodniX - uniformniOfsetZaXGornje(e);
        prethodniX = x;
        int y = (dimenzija/2) + uniformniOfsetZaY(e);
        outTacke.push_back(QPoint(x + ofsetNaKraju, y + ofsetNaKraju));
    }
}

void Triangulacija2::pokreniAlgoritam()
{
    if(_koristiNaivni)
    {
        pokreniNaivniAlgoritam();
        return;
    }

    AlgoritamBaza_updateCanvasAndBlock()
}

void Triangulacija2::pokreniNaivniAlgoritam()
{
    podesiOsnovniDcell();
    size_t brojTemena = _polygon.v().size();
    size_t obradjenih = 0;
    if(brojTemena < 3)
    {
        _greska = QString("Nedovoljan broj temena zadat. Mora biti barem tri temena");
        return;
    }

    _poljeZaObradu = _polygon.f().at(0);

    while(obradjenih < brojTemena - 2)
    {
        Vertex *v = nullptr;
        Vertex *u = nullptr;
        Vertex *w = nullptr;
        HalfEdge *ve = nullptr;

        do
        {
            ve = uzmiUnutrasnjuIvicu(ve);
            v = ve->origin();
            u = ve->next()->origin();
            w = ve->prev()->origin();
            _trougaoV = v;
            _trougaoU = u;
            _trougaoW = w;
            AlgoritamBaza_updateCanvasAndBlock()
        }
        while(losTrougao(v, u, w));

        _poslednjaDodataIvica = odseciTrougao(ve, v, u, w);

        ++obradjenih;
        AlgoritamBaza_updateCanvasAndBlock()
    }

    _poslednjaDodataIvica = nullptr;
    _trougaoU = nullptr;
    _trougaoV = nullptr;
    _trougaoW = nullptr;
    AlgoritamBaza_updateCanvasAndBlock()
}

void Triangulacija2::podesiOsnovniDcell()
{
    _iscrtajDcellInstant = false;
    for(size_t i = 0; i < this->_polygon.v().size(); ++i)
    {
        _maxTemeZaIscrtavanje = i;
        AlgoritamBaza_updateCanvasAndBlock()
    }

    _iscrtajDcellInstant = true;
}

HalfEdge *Triangulacija2::uzmiUnutrasnjuIvicu(HalfEdge* prethodna)
{
    if(nullptr == prethodna)
    {
        if(nullptr == _poslednjaDodataIvica)
        {
            return _poljeZaObradu->innerComponent();
        }

        return _poslednjaDodataIvica;
    }

    return prethodna->next();
}

bool Triangulacija2::losTrougao(Vertex* v, Vertex *u, Vertex *w)
{
    return imaTemenaUUnutrasnjosti(v,u,w) || pozitivnoOrijentisan(v, u, w);
}

bool Triangulacija2::imaTemenaUUnutrasnjosti(Vertex *v, Vertex *u, Vertex *w)
{
    QPolygon poly;
    poly << v->coordinates() << u->coordinates() << w->coordinates();

    for(auto it = _polygon.v().cbegin(); it != _polygon.v().cend(); ++it)
    {
        Vertex *x = *it;
        if(!x->obradjen() &&
           !(x->coordinates().x() == v->coordinates().x() && x->coordinates().y() == v->coordinates().y()) &&
           !(x->coordinates().x() == u->coordinates().x() && x->coordinates().y() == u->coordinates().y()) &&
           !(x->coordinates().x() == w->coordinates().x() && x->coordinates().y() == w->coordinates().y()) &&
                poly.containsPoint(x->coordinates(),Qt::FillRule::WindingFill))
        {
            return true;
        }
    }

    return false;
}

bool Triangulacija2::pozitivnoOrijentisan(Vertex *v, Vertex *u, Vertex* w)
{
    int p1x = v->coordinates().x();
    int p1y = v->coordinates().y();
    int p2x = u->coordinates().x();
    int p2y = u->coordinates().y();
    int p3x = w->coordinates().x();
    int p3y = w->coordinates().y();

    return ((p2y - p1y) * (p3x - p2x) - (p2x - p1x) * (p3y - p2y)) > 0;
}

HalfEdge *Triangulacija2::odseciTrougao(HalfEdge *e, Vertex *v, Vertex *u, Vertex *w)
{
    // Dodajemo dve poluivice kojima odsecamo novo polje.
    // Zadrzavamo referencu na pocetno polje za obradu, posto cemo njega nastaviti da opsecamo.
    //
    HalfEdge *uw = new HalfEdge();
    _polygon.e().push_back(uw);
    HalfEdge *wu = new HalfEdge();
    _polygon.e().push_back(wu);
    Field *odsecen = new Field();
    _polygon.f().push_back(odsecen);

    wu->setPrev(e->prev()->prev());
    wu->setNext(e->next());
    wu->setTwin(uw);
    wu->setOrigin(w);
    wu->setIncidentFace(_poljeZaObradu);

    uw->setPrev(e);
    uw->setNext(e->prev());
    uw->setTwin(wu);
    uw->setOrigin(u);
    uw->setIncidentFace(odsecen);

    odsecen->setInnerComponent(uw);
    odsecen->setOuterComponent(wu);

    e->next()->setPrev(wu);
    e->prev()->prev()->setNext(wu);

    e->setNext(uw);
    e->prev()->setPrev(uw);

    _poljeZaObradu->setInnerComponent(wu);
    _poljeZaObradu->setOuterComponent(uw);

    v->obradi();
    w->setIncidentEdge(wu);
    u->setIncidentEdge(wu->next());
    return wu;
}

void Triangulacija2::crtajAlgoritam(QPainter &painter) const
{
    ispisiGresku(painter);
    crtajDcell(painter);
}

void Triangulacija2::ispisiGresku(QPainter &painter)const
{
    Crtac::Crtaj(_greska, painter);
}

void Triangulacija2::crtajDcell(QPainter &painter) const
{
    // Israfiraj trenutno polje koje se razmatra.
    //
    if(nullptr != _trougaoU && nullptr != _trougaoV && nullptr != _trougaoW)
    {
        QPolygon poly;
        poly << _trougaoU->coordinates() << _trougaoV->coordinates() << _trougaoW->coordinates();
        QPainterPath path;
        path.addPolygon(poly);
        QBrush cetka(Qt::GlobalColor::red, Qt::BrushStyle::Dense6Pattern);
        painter.fillPath(path, cetka);
    }

    // Nacrtaj ivicu poligona.
    //
    size_t currIvica = 0;
    for(auto it = _iviceInicijalnogPoligona.cbegin();
        it != _iviceInicijalnogPoligona.cend() && (_iscrtajDcellInstant || currIvica <= 2 * _maxTemeZaIscrtavanje);
        ++it, currIvica++)
    {
        Crtac::Crtaj(*it, painter, Qt::GlobalColor::black, 2);
    }

    size_t currTeme = 0;
    for(auto it = _polygon.cv()->cbegin();
        it != _polygon.cv()->cend() && (_iscrtajDcellInstant || currTeme <= _maxTemeZaIscrtavanje);
        ++it, ++currTeme)
    {
        Crtac::Crtaj(*it, painter);
    }

    // Iscrtaj sve ivice koje postoje u DCELL strukturi.
    // Ne razlikujemo spoljasnje i unutrasnje, pa sve crtamo po dva puta,
    // ali nema veze.
    //
    if(_iscrtajDcellInstant)
    {       for(auto it = _polygon.ce()->cbegin(); it != _polygon.ce()->cend(); ++it)
        {
            const HalfEdge *ivica = *it;
            Crtac::Crtaj(ivica, painter, Qt::GlobalColor::black, 1);
        }
    }

    // Obelezi poslednju dodatu ivicu.
    //
    if(nullptr != _poslednjaDodataIvica)
    {
        Crtac::Crtaj(_poslednjaDodataIvica, painter, Qt::magenta, 3, Qt::PenStyle::DashLine);
    }
}

void Crtac::Crtaj(const HalfEdge *ivica, QPainter &painter, Qt::GlobalColor boja, int debljina, Qt::PenStyle stil)
{
    QPen olovka(boja, debljina, stil);
    painter.setPen(olovka);
    painter.drawLine(ivica->t1(), ivica->t2());
}

void Crtac::Crtaj(const Vertex *teme, QPainter &painter)
{
    QPen pen(Qt::magenta, 6, Qt::SolidLine);
    painter.setPen(pen);
    painter.drawPoint(teme->coordinates());
}

void Crtac::Crtaj(const QString &text, QPainter &painter)
{
    QPen olovka(Qt::GlobalColor::red, 3, Qt::SolidLine);
    painter.setPen(olovka);
    painter.drawText(QPoint(500, 20), text);
}
