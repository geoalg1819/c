#include "ga02_unija_pravougaonika.h"
#include <iostream>
#include <fstream>
#include <QDebug>
#include <cstdlib>
#include <ctime>


UnijaPravougaonika::UnijaPravougaonika(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_tacaka)
    : AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
    , y(0), updatedP(0), P(0), dogadjaji(), status(), T(nullptr)
{
    if (imeDatoteke == "")
        this->generisiNasumicnePravougaonike(broj_tacaka);
    else
        this->ucitajPraougaonikeIzDatoteke(imeDatoteke);

    for (const QRect &p : this->pravougaonici) {
        this->dogadjaji.insert({&p, true});
        this->dogadjaji.insert({&p, false});
    }

    std::srand(std::time(NULL));
}

void UnijaPravougaonika::pokreniAlgoritam()
{
    this->y = 0;
    this->P = 0;
    this->updatedP = 0;
    while (!this->dogadjaji.empty()) {
        StranicaDogadjaja s = *this->dogadjaji.begin();
        this->dogadjaji.erase(this->dogadjaji.begin());

        this->updatedP = (s.y() - this->y) * this->getUnion(this->T);
        this->P += this->updatedP;
        this->y = s.y();

        if (s.begin) {
            this->insert(s.p);
            this->dodajUStatus(s.p);
        } else {
            this->remove(s.p);
            this->izbrisiIzStatusa(s.p);
        }

        AlgoritamBaza_updateCanvasAndBlock();
    }

    emit this->animacijaZavrsila();
    this->reset();
}

void UnijaPravougaonika::crtajAlgoritam(QPainter &painter) const
{
    QPen p = painter.pen();
    p.setWidth(2);
    painter.setRenderHint(QPainter::Antialiasing, true);

    for (const auto &r : this->pravougaonici) {
        if (this->uStatusu(&r))
            painter.setPen(Qt::red);
        else
            painter.setPen(Qt::blue);

        painter.resetTransform();
        QPainterPath path;
        path.addText(r.x(), -r.y() + this->_pCrtanje->height(), QFont("Monospace"), tr(std::to_string(r.width() * r.height()).data()));
        painter.drawPath(path);
        painter.translate(this->_pCrtanje->rect().bottomLeft());
        painter.scale(1.0, -1.0);

        painter.drawRect(r);
        painter.fillRect(r, this->uStatusu(&r) ? QColor(255, 0, 0, 25) : QColor(0, 0, 255, 25));
    }

    painter.setPen(Qt::green);
    painter.drawLine(0, this->y, this->_pCrtanje->width(), y);

    painter.resetTransform();
    QPainterPath path;
    path.addText(2, -this->y + this->_pCrtanje->height() - 3, QFont("Monospace"), "+" + tr(std::to_string(this->updatedP).data()));
    path.addText(2, -this->y + this->_pCrtanje->height() + 17, QFont("Monospace"), "P: " + tr(std::to_string(this->P).data()));
    painter.drawPath(path);
    painter.translate(this->_pCrtanje->rect().bottomLeft());
    painter.scale(1.0, -1.0);
}

void UnijaPravougaonika::pokreniNaivniAlgoritam()
{
    this->y = 0;
    this->P = 0;
    this->updatedP = 0;
    while (!this->dogadjaji.empty()) {
        StranicaDogadjaja s = *this->dogadjaji.begin();
        this->dogadjaji.erase(this->dogadjaji.begin());

        this->updatedP = (s.y() - this->y) * this->statusUnion();
        this->P += this->updatedP;
        this->y = s.y();

        if (s.begin)
            this->dodajUStatus(s.p);
        else
            this->izbrisiIzStatusa(s.p);

        AlgoritamBaza_updateCanvasAndBlock();
    }

    emit this->animacijaZavrsila();
    this->reset();
}

bool UnijaPravougaonika::is_3D() const
{
    return false;
}

void UnijaPravougaonika::crtajAlgoritam3D() const
{

}

void UnijaPravougaonika::ucitajPraougaonikeIzDatoteke(std::string imeDatoteke)
{
    std::ifstream f(imeDatoteke);

    int x, y, w, h;
    while(f >> x >> y >> w >> h)
        this->pravougaonici.push_back(QRect(x, y, w, h));

    f.close();
}

void UnijaPravougaonika::generisiNasumicnePravougaonike(int broj)
{
    const int minsize = 10;
    int xmax, ymax;
    if (this->_pCrtanje) {
        xmax = _pCrtanje->width() - 2 * DRAWING_BORDER - minsize;
        ymax = _pCrtanje->height() - 2 * DRAWING_BORDER - minsize;
    } else {
        xmax = INT_MAX;
        ymax = INT_MAX;
    }

    for (int i = 0; i < broj; ++i) {
        int x = this->getRandomInt() % (xmax - DRAWING_BORDER) + DRAWING_BORDER;
        int y = this->getRandomInt() % (ymax - DRAWING_BORDER) + DRAWING_BORDER;
        int w = this->getRandomInt() % (xmax - x + 1) + minsize;
        int h = this->getRandomInt() % (ymax - y + 1) + minsize;

        // TODO upgrade T to allow multiple rects with same coordinates
//        for (const QRect &r : this->pravougaonici) {
//            if (x == r.x() || (x + w) == (r.x() + r.width())) {
//                --i;
//                continue;
//            }
//        }

        this->pravougaonici.push_back(QRect(x, y, w, h));
    }

    std::sort(this->pravougaonici.begin(), this->pravougaonici.end(), [] (const QRect &r1, const QRect &r2) {
        return r1.height() * r1.width() < r2.height() * r2.width();
    });

}

void UnijaPravougaonika::dodajUStatus(const QRect *r)
{
//    qDebug() << "Dodat u status: " << *r;
    this->status.insert(r);
}

bool UnijaPravougaonika::uStatusu(const QRect *r) const
{
    for (const QRect *p : this->status)
        if (p == r)
            return true;
    return false;
}

void UnijaPravougaonika::izbrisiIzStatusa(const QRect *r)
{
    for (auto it = this->status.begin(); it != this->status.end(); ++it) {
        if (*it == r) {
//            qDebug() << "Izbacen iz statusa: " << *r;
            this->status.erase(it);
            break;
        }
    }
}

int UnijaPravougaonika::statusUnion() const
{
    if (this->status.size() == 0)
        return 0;

    std::vector<const QRect*> st(this->status.size());
    std::copy(this->status.cbegin(), this->status.cend(), st.begin());
    std::sort(st.begin(), st.end(), [](const QRect *r1, const QRect *r2) { return r1->x() < r2->x(); });

    int U = 0;

    int xl = st[0]->x();
    int maxr = xl + st[0]->width();
    for (const QRect *curr : st) {
        if (curr->x() < maxr)
            U += curr->x() - xl;
        else
            U += maxr - xl;
        xl = curr->x();
        if (curr->x() + curr->width() > maxr)
            maxr = curr->x() + curr->width();
    }

    U += maxr - xl;

    return U;
}

void UnijaPravougaonika::insert(const QRect *r)
{
    int x1 = r->x();
    int x2 = r->x() + r->width();
    this->T = this->insert(this->T, x1, x2);
}

void UnijaPravougaonika::remove(const QRect *r)
{
    int x1 = r->x();
    int x2 = r->x() + r->width();
    this->T = this->remove(this->T, x1, x2);
}

UnijaPravougaonika::Node *UnijaPravougaonika::insert(UnijaPravougaonika::Node *node, int x1, int x2)
{
    if (node == nullptr) {
        node = this->insert(node, x1, x2, true);
        node = this->insert(node, x2, x1, false);
    } else if (x1 < node->value && x2 < node->value) {
        node->left = this->insert(node->left, x1, x2);
    } else if (x1 > node->value && x2 > node->value) {
        node->right = this->insert(node->right, x1, x2);
    } else {
        node = this->insert(node, x1, x2, true);
        node = this->insert(node, x2, x1, false);
    }
    this->update(node);
    return node;
}

UnijaPravougaonika::Node *UnijaPravougaonika::remove(UnijaPravougaonika::Node *node, int x1, int x2)
{
    if (node == nullptr) {
        return nullptr;
    } else if (x1 < node->value && x2 < node->value) {
        node->left = this->remove(node->left, x1, x2);
    } else if (x1 > node->value && x2 > node->value) {
        node->right = this->remove(node->right, x1, x2);
    } else {
        node = this->remove(node, x1);
        node = this->remove(node, x2);
    }

    this->update(node);
    return node;
}

UnijaPravougaonika::Node *UnijaPravougaonika::insert(UnijaPravougaonika::Node *node, int v, int other, bool isLeft)
{
    if (node == nullptr) {
        Node *n = new Node();
        n->value = n->max = n->min = v;
        n->other = other;
        n->isLeft = isLeft;
        n->measure = 0;
        n->leftmin = std::min(v, other);
        n->rightmax = std::max(v, other);
        return n;
    }

    if (v < node->value)
        node->left = this->insert(node->left, v, other, isLeft);
    else
        node->right = this->insert(node->right, v, other, isLeft);

    this->update(node);
    return node;
}

UnijaPravougaonika::Node *UnijaPravougaonika::remove(UnijaPravougaonika::Node *node, int v)
{
    if (node == nullptr)
        return nullptr;

    if (v < node->value) {
        node->left = this->remove(node->left, v);
    } else if (v > node->value) {
        node->right = this->remove(node->right, v);
    } else {
        if (node->left == nullptr) {
            Node *tmp = node->right;
            delete node;
            return tmp;
        } else if (node->right == nullptr) {
            Node *tmp = node->left;
            delete node;
            return tmp;
        }

        Node *tmp = this->findMin(node->right);
        node->value = tmp->value;
        node->right = this->remove(node->right, tmp->value);
    }

    this->update(node);
    return node;
}

void UnijaPravougaonika::update(UnijaPravougaonika::Node *node)
{
    if (node == nullptr || (node->left == nullptr && node->right == nullptr))
        return;

    if ((node->left != nullptr && node->left->value == node->other) || (node->right != nullptr && node->right->value == node->other)) {
        node->max = std::max(node->value, node->other);
        node->measure = std::abs(node->value - node->other);
        if (node->left != nullptr && node->left->left == nullptr && node->left->right == nullptr)
            return;
        if (node->right != nullptr && node->right->left == nullptr && node->right->right == nullptr)
            return;
    }

    node->min = node->left != nullptr ? node->left->min : node->value;
    node->max = node->right != nullptr ? node->right->max : node->value;

    if (node->isLeft) {
        node->leftmin = std::min(node->value, std::min(this->getLeftMin(node->left), this->getLeftMin(node->right)));
        node->rightmax = std::max(node->other, std::max(this->getRightMax(node->left), this->getRightMax(node->right)));

        if ((node->left != nullptr && node->right != nullptr && (this->getRightMax(node->left) == this->getMax(node->left) && ((this->getLeftMin(node->right) == this->getMin(node->right) || this->getLeftMin(node->right) == node->value))))
         || (node->right == nullptr && this->getRightMax(node->left) == this->getMax(node->left))
         || (node->left == nullptr && (this->getLeftMin(node->right) == this->getMin(node->right) || this->getLeftMin(node->right) == node->value))) {
            if (this->subtreeContainsPoint(node->right, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) + this->getMin(node->right, node) - node->value;
            else
                node->measure = this->getUnion(node->left) + this->getMax(node->right, node) - node->value;
        } else if (node->left != nullptr && this->getRightMax(node->left) != this->getMax(node->left)) {
            if (this->subtreeContainsPoint(node->right, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) + this->getMin(node->right, node) - this->getMax(node->left, node);
            else
                node->measure = this->getUnion(node->left) + this->getMax(node->right, node) - this->getMax(node->left, node);
        } else if (node->right != nullptr && this->getLeftMin(node->right) != this->getMin(node->right) && this->getLeftMin(node->right) != node->value) {
            if (this->subtreeContainsPoint(node->right, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) + this->getMin(node->right, node) - node->value;
            else
                node->measure = this->getUnion(node->left) + this->getMin(node->right, node) - this->getMin(node->left, node);
        } else {
            throw std::runtime_error("THIS SHOULD NOT HAPPEN");
        }
    } else {
        node->leftmin = std::min(node->other, std::min(this->getLeftMin(node->left), this->getLeftMin(node->right)));
        node->rightmax = std::max(node->value, std::max(this->getRightMax(node->left), this->getRightMax(node->right)));

        if ((node->left != nullptr && node->right != nullptr && (this->getLeftMin(node->right) == this->getMin(node->right) && ((this->getRightMax(node->left) == this->getMax(node->left) || this->getRightMax(node->left) == node->value))))
         || (node->left == nullptr && this->getLeftMin(node->right) == this->getMin(node->right))
         || (node->right == nullptr && (this->getRightMax(node->left) == this->getMax(node->left) || this->getRightMax(node->left) == node->value))) {
            if (!this->subtreeContainsPoint(node->left, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) - this->getMax(node->left, node) + node->value;
            else
                node->measure = this->getUnion(node->right) - this->getMin(node->left, node) + node->value;
        } else if (node->right != nullptr && this->getLeftMin(node->right) != this->getMin(node->right)) {
            if (!this->subtreeContainsPoint(node->left, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) - this->getMax(node->left, node) + this->getMin(node->right, node);
            else
                node->measure = this->getUnion(node->right) - this->getMin(node->left, node) + this->getMin(node->right, node);
        } else if (node->left != nullptr && this->getRightMax(node->left) != this->getMax(node->left) && this->getRightMax(node->left) != node->value) {
            if (!this->subtreeContainsPoint(node->left, node->other))
                node->measure = this->getUnion(node->left) + this->getUnion(node->right) - this->getMax(node->left, node) + node->value;
            else
                node->measure = this->getUnion(node->right) - this->getMax(node->left, node) + this->getMax(node->right, node);
        } else {
            throw std::runtime_error("THIS SHOULD NOT HAPPEN");
        }
    }
}

void UnijaPravougaonika::printT(const Node *node) const
{
    if (node == nullptr)
        return;

    this->printT(node->left);
    std::cout << node->value << std::endl;
    this->printT(node->right);
}

bool UnijaPravougaonika::subtreeContainsPoint(const UnijaPravougaonika::Node *node, int v) const
{
    if (node == nullptr)
        return false;
    return node->min <= v && node->max >= v;
}

bool UnijaPravougaonika::find(const UnijaPravougaonika::Node *node, int v) const
{
    if (node == nullptr)
        return false;
    if (node->value == v)
        return true;
    return v < node->value ? this->find(node->left, v) : this->find(node->right, v);
}

UnijaPravougaonika::Node *UnijaPravougaonika::findMin(UnijaPravougaonika::Node *node) const
{
    Node *it = node;
    while (it->left != nullptr)
        it = it->left;
    return it;
}

int UnijaPravougaonika::getUnion(const UnijaPravougaonika::Node *node) const
{
    return node != nullptr ? node->measure : 0;
}

int UnijaPravougaonika::getRightMax(const Node *node, const UnijaPravougaonika::Node *alt) const
{
    return node != nullptr ? node->rightmax : (alt != nullptr ? alt->value : INT_MIN);
}

int UnijaPravougaonika::getLeftMin(const Node *node, const UnijaPravougaonika::Node *alt) const
{
    return node != nullptr ? node->leftmin : (alt != nullptr ? alt->value : INT_MAX);
}

int UnijaPravougaonika::getMax(const UnijaPravougaonika::Node *node, const UnijaPravougaonika::Node *alt) const
{
    return node != nullptr ? node->max : (alt != nullptr ? alt->value : INT_MIN);
}

int UnijaPravougaonika::getMin(const UnijaPravougaonika::Node *node, const UnijaPravougaonika::Node *alt) const
{
    return node != nullptr ? node->min : (alt != nullptr ? alt->value : INT_MAX);
}

void UnijaPravougaonika::benchmark()
{
    qDebug() << "Starting benchmark...";

    std::vector<int> x { 10, 100, 1000, 2000, 3500, 5000, 7500, 10000 };
    std::vector<double> naiveTimes;
    std::vector<double> optimalTimes;

    for (int i : x) {
        clock_t begin, end;
        double optimalTime, naiveTime;

        UnijaPravougaonika handle1(this->_pCrtanje, this->_pCrtanjeGL, 0, "", i);
        begin = clock();
        handle1.pokreniAlgoritamBenchmark();
        end = clock();
        optimalTime = double(end - begin) / CLOCKS_PER_SEC;

        UnijaPravougaonika handle2(this->_pCrtanje, this->_pCrtanjeGL, 0, "", i);
        begin = clock();
        handle2.pokreniNaivniAlgoritamBenchmark();
        end = clock();
        naiveTime = double(end - begin) / CLOCKS_PER_SEC;

        naiveTimes.push_back(naiveTime);
        optimalTimes.push_back(optimalTime);
    }

    std::cout << "Naive times: [ ";
    for (double t : naiveTimes)
        std::cout << t << " ";
    std::cout << ']' << std::endl;

    std::cout << "Optimal times: [ ";
    for (double t : optimalTimes)
        std::cout << t << " ";
    std::cout << ']' << std::endl;

    qDebug() << "Benchmark ended";
}

int UnijaPravougaonika::getRandomInt() const
{
    return std::abs(std::rand());
}

void UnijaPravougaonika::reset()
{
    this->status.clear();
    this->dogadjaji.clear();
    this->y = 0;
    this->updatedP = 0;
    this->T = nullptr;
    for (const QRect &p : this->pravougaonici) {
        this->dogadjaji.insert({&p, true});
        this->dogadjaji.insert({&p, false});
    }
}

int UnijaPravougaonika::pokreniAlgoritamBenchmark()
{
    this->y = 0;
    this->P = 0;
    this->updatedP = 0;
    while (!this->dogadjaji.empty()) {
        StranicaDogadjaja s = *this->dogadjaji.begin();
        this->dogadjaji.erase(this->dogadjaji.begin());

        this->P += (s.y() - this->y) * this->getUnion(this->T);
        this->y = s.y();

        if (s.begin) {
            this->insert(s.p);
        } else {
            this->remove(s.p);
        }
    }

    return this->P;
}


int UnijaPravougaonika::pokreniNaivniAlgoritamBenchmark()
{
    this->y = 0;
    this->P = 0;
    this->updatedP = 0;
    while (!this->dogadjaji.empty()) {
        StranicaDogadjaja s = *this->dogadjaji.begin();
        this->dogadjaji.erase(this->dogadjaji.begin());

        this->P += (s.y() - this->y) * this->statusUnion();
        this->y = s.y();

        if (s.begin)
            this->dodajUStatus(s.p);
        else
            this->izbrisiIzStatusa(s.p);
    }

    return this->P;
}
