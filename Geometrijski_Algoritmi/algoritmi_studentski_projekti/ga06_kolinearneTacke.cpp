#include "ga06_kolinearneTacke.h"
#include <QVector2D>
#include <math.h>
#include <iostream>
#include "algoritam_baza.h"
#include "config.h"
#include "pomocnefunkcije.h"

KolinearneTacke::KolinearneTacke(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_tacaka)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{
    if (imeDatoteke != "") {
        pocetneTacke = ucitajPodatkeIzDatoteke(imeDatoteke);
        for(unsigned i = 0; i < pocetneTacke.size(); i++){
            _tacke.push_back({pocetneTacke[i], 0});
        }
    } else
        generisiNasumicneTacke(broj_tacaka);
}

void KolinearneTacke::pokreniAlgoritam()
{
    kolinearne.clear();

    double ugao;
    for(unsigned i = 0; i < pocetneTacke.size(); i++){
        _trenutnaTacka = pocetneTacke[i];

        qSort(_tacke.begin(), _tacke.end(), [&](const Tacka& tacka1, const Tacka& tacka2){return poredi3(tacka1, tacka2);});
        for(unsigned i = 0; i < _tacke.size(); i++){
            ugao = 0;
            if(i == _tacke.size()-1){
                ugao = pomocnefunkcije::povrsinaTrougla2(_trenutnaTacka, _tacke[i-1].tacka, _tacke[i].tacka);
            }else{
                ugao = pomocnefunkcije::povrsinaTrougla2(_trenutnaTacka, _tacke[i].tacka, _tacke[i+1].tacka);
            }
            _tacke[i].ugao = ugao;
            _index = i;
            AlgoritamBaza_updateCanvasAndBlock();
            if(_tacke[i].ugao == 0 && i != 0) {
                this->kolinearne.push_back(std::make_pair(_trenutnaTacka, _tacke[i].tacka));
                this->kolinearne.push_back(std::make_pair(_trenutnaTacka, _tacke[i+1].tacka));
            }
        }
    }

    emit this->animacijaZavrsila();
    this->reset();
}


void KolinearneTacke::crtajAlgoritam(QPainter &painter) const
{
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen p = painter.pen();
    p.setColor(Qt::black);
    p.setWidth(3);
    p.setCapStyle(Qt::RoundCap);

    painter.setPen(p);

    for(QPoint tacka : pocetneTacke)
        painter.drawPoint(tacka);

    for(unsigned i = 0; i < kolinearne.size(); i++) {
        p.setColor(Qt::blue);
        painter.setPen(p);
        painter.drawLine(kolinearne[i].first, kolinearne[i].second);
    }

    unsigned i = 0;
    while(i <= _index){
        if(_tacke[i].ugao == 0 && i != _index && i != 0){
            p.setColor(Qt::blue);
            painter.setPen(p);
            painter.drawLine(_trenutnaTacka, _tacke[i].tacka);
            painter.drawLine(_trenutnaTacka, _tacke[i+1].tacka);
            i+=2;
        }else{
            p.setColor(Qt::red);
            painter.setPen(p);
            painter.drawLine(_trenutnaTacka, _tacke[i].tacka);
            i++;
        }
    }
}

void KolinearneTacke::crtajAlgoritam3D() const
{
}

bool KolinearneTacke::is_3D() const
{
    return false;
}

void KolinearneTacke::pokreniNaivniAlgoritam()
{
    kolinearne.clear();
    _index = 1;
    double ugao;

    for(unsigned i = 0; i < _tacke.size(); i++){
        for(unsigned j = 0; j < _tacke.size(); j++){
            for(unsigned k = 0; k < _tacke.size(); k++){
                if(i != j && j != k && i != k) {
                    ugao = pomocnefunkcije::povrsinaTrougla2(_tacke[i].tacka, _tacke[j].tacka, _tacke[k].tacka);
                    this->_trenutnaTacka = _tacke[i].tacka;
                    _tacke[j].ugao = ugao;
                    _tacke[k].ugao = ugao;
                    if(ugao == 0) {
                        this->kolinearne.push_back(std::make_pair(_tacke[i].tacka, _tacke[j].tacka));
                        this->kolinearne.push_back(std::make_pair(_tacke[i].tacka, _tacke[k].tacka));
                    }
                    AlgoritamBaza_updateCanvasAndBlock();
                }
            }
        }
    }

    emit this->animacijaZavrsila();
    this->reset();
}

double KolinearneTacke::izracunajUgao(QPoint o, QPoint b) {

    QVector2D vect1;
    QVector2D vect2;
    QPoint a = QPoint(o.x() + 1, o.y());
    vect1 = QVector2D(a.x() - o.x(), a.y() - o.y());
    vect2 = QVector2D(b.x() - o.x(), b.y() - o.y());
    float dot = vect1.x()*vect2.x() + vect1.y()*vect2.y();
    double d = sqrt(pow(vect1.x(), 2) + pow(vect1.y(), 2)) * sqrt(pow(vect2.x(), 2) + pow(vect2.y(), 2));
    double angle;
    double c = 0;
    if(d == 0){
        angle = 0;
    }else{
        c = dot/d;
        angle = acos(dot/d);
    }
    std::cout << c << " " << angle << std::endl;
    return angle;
}

bool KolinearneTacke::poredi(const Tacka &a, const Tacka &b)
{
    return a.ugao < b.ugao;
}

bool KolinearneTacke::poredi3(const Tacka &a, const Tacka &b)
{
    double param = pomocnefunkcije::povrsinaTrougla2(_trenutnaTacka, a.tacka, b.tacka);

    if (param > 0)
       return false;

    if (param < 0)
        return true;

    int x = abs(a.tacka.x() - _trenutnaTacka.x()) - abs(b.tacka.x() - _trenutnaTacka.x());
    int y = abs(a.tacka.y() - _trenutnaTacka.y()) - abs(b.tacka.y() - _trenutnaTacka.y());

    return (x < 0 || y < 0);

}

void KolinearneTacke::reset()
{
    this->pocetneTacke.clear();
    this->_tacke.clear();
    this->kolinearne.clear();
    generisiNasumicneTacke(20);
}

void KolinearneTacke::generisiNasumicneTacke(int broj_tacaka)
{
    int xMax;
    int yMax;

    srand(static_cast<unsigned>(time(0)));

    if (_pCrtanje)
    {
        xMax = _pCrtanje->width()-DRAWING_BORDER;
        yMax = _pCrtanje->height() - DRAWING_BORDER;
    }
    else
    {
        xMax = CANVAS_WIDTH;
        yMax = CANVAS_HEIGHT;
    }


    int xMin = DRAWING_BORDER;
    int yMin = DRAWING_BORDER;


    int xDiff = xMax-xMin;
    int yDiff = yMax-yMin;
    for(int i=0; i < broj_tacaka; i++) {
        QPoint novaTacka = QPoint(xMin + rand()%xDiff, yMin + rand()%yDiff);
        this->pocetneTacke.push_back(novaTacka);
        this->_tacke.push_back({novaTacka, 0});
    }
}
