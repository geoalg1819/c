#ifndef RANGETREE_H
#define RANGETREE_H

#include <vector>
#include <iostream>
#include <experimental/optional>

#include <QPoint>
#include <QVector>

/**
 * @brief Klasa RangeTree - iterativna varijanta stabla opsega
 *
 * Cvorovi se pohranjuju u niz i indeksirani su pocevsi od indeksa 1 (koren stabla).
 * Levi (desni) sin cvora sa indeksom M je cvor sa indeksom 2*M (2*M+1).
 * Listova ima najvise N i to u slucaju kada je N=2^k (za neko prirodno k).
 *
 * Informacija o konkretnoj tacki sadrzana je samo u listovima stabla.
 *
 * Stablo se izgradjuje iterativno i to pristupom odozdo-nagore i sa desna nalevo.
 * Svaka od metoda za rad nad stablom je takodje iterativna, zarad prakticne efikasnosti.
 *
 */
class RangeTree {

    using Tacka = std::experimental::optional<QPoint>;
    /**
     * @brief Struktura SekElement - opis elementa sekundarnog niza
     *
     * Ovakvi elementi sacinjavaju sekundarni niz koji omogucava optimizaciju strukture stabla
     * opsega za logaritamski faktor - sa O(log^2(N)+k) na O(log(N)+k)
     *
     * Rec je o tehnici "kaskadnog povezivanja" kojom se dobija "slojevito stablo" opsega.
     *
     */
    typedef struct SekElement {
        SekElement(int i, Tacka t = {}) : indeks(i), tacka(t) { levi = desni = nullptr; }
        int indeks;
        Tacka tacka;
        SekElement *levi, *desni;
        int x() const { return tacka.value().x(); }
        int y() const { return tacka.value().y(); }
        SekElement *dete(bool levo) { return (levo) ? levi : desni; }
    } SekElement;

    using SekNiz = QVector<SekElement *>;

    /**
     * @brief Struktura Cvor - opis pojedinacnog cvora stabla opsega
     *
     * Stablo sadrzi O(N) ovakvih cvorova, preciznije, ne vise of 4*N cvorova.
     *
     */
    typedef struct Cvor {
        Cvor() {}
        Cvor(int i, Tacka t = {}, int maks_l = {}, int maks_u = {})
            : indeks(i), tacka(t), maks_levo(maks_l), maks_ukupno(maks_u)
        {
        }
        int indeks{};       /// indeks cvora (u nizu m_cvorovi)
        Tacka tacka{};      /// informacija dostupna samo u listovima
        int maks_levo{};    /// maksimum levog podstabla
        int maks_ukupno{};  /// maksimum oba podstabla
        SekNiz sek{};       /// sekundarni niz (kaskadno povezivanje)

        SekElement *donjaGranica(int y);
        bool list() { return (tacka) ? true : false; }
    } Cvor, *CvorPok;

  public:
    RangeTree(QVector<QPoint> &tacke);
    ~RangeTree();

    void ispisiTackeIzOpsega(int intervalX[2], int intervalY[2]);
    void sakupiTackeIzOpsega(int intervalX[2], int intervalY[2], QVector<QPoint> &tacke);
    static void ispisiTacku(CvorPok list);

    int nadjiIndeksCvoraPodele(int interval[2]) const;

  private:
    CvorPok koren() const;
    CvorPok dete(CvorPok v, bool levo) const;
    int indeksDeteta(int i, bool levo) const;

    void sakupiKanonickiPodskup(CvorPok v, QVector<QPoint> &tacke) const;
    CvorPok nadjiCvorPodele(int interval[2]) const;

    int m_brojListova;
    QVector<CvorPok> m_cvorovi;
};

/**
* @note Klasa RangeTree - SLOZENOSTI
*
* Za skup od N tacaka zauzima se prostor od 2^(ceil(log2(N))+1) cvorova glavnog stabla (po prvoj
* koordinati), sto nije vise od 4*N cvorova.
*
* Na svakom od log(N) nivoa glavnog stabla, rezervise se O(N) dodatnog prostora za sekundarni niz
* (po drugoj koordinati), sto sveukupno rezultuje u O(n*log(N)) prostora za 2D stablo opsega.
*
* Vremenska slozenost konstrukcije je O(n*log(N)).
* Cena upita O(log(N)+k), gde je k broj tacaka u zadatom opsegu.
* Upit brojnosti, ali ne i konkretne liste tacaka, je jeftiniji i iznosi O(log(N)).
*
*/

#endif  // RANGETREE_H
