#ifndef GA08_HERTELMEHLHORN_H
#define GA08_HERTELMEHLHORN_H

#include "algoritambaza.h"
#include <QPointF>
#include <QVector>

class Poligon;

class Duz
{
public:
    Duz();
    Duz(QPoint a, QPoint b);
    QPair<QPoint, QPoint> temena();
    QPoint a() const { return _a; }
    QPoint b() const { return _b; }

    Poligon* incidentniPoligon();
    void dodajIncidentniPoligon(Poligon *p);

    bool operator==(const Duz &d) const;

protected:
    QPoint _a, _b;
    Poligon *_incidentniPoligon;
};

class Poligon
{
public:
    Poligon();
    ~Poligon();
    Poligon(int brTemena);
    Poligon(const Poligon &p);
    Poligon(QVector<QPoint> *temena);

    int brTemena() const;
    QPoint teme(int i) { return (*_temena)[i]; }
    QVector<QPoint>* temena() { return _temena; }
    void dodajTeme(QPoint p);
    bool imaTeme(const QPoint &a) const;

    void dodajDuz(QPoint a, QPoint b);
    void dodajDuz(Duz d);
    int indeksDuzi(const QPoint &a, const QPoint &b) const;
    int indeksDuzi(const Duz &s) const;
    QVector<Duz> duzi() { return _duzi; }
    Duz duz(int i) { return _duzi[i]; }
    void dodajIncidentniPoligon(int i, Poligon *p);

    bool konveksan() const;
    bool sused(const Poligon &p) const;
    void sortirajDuzi();

    Poligon spoji(Poligon &p);

    Poligon& operator=(const Poligon &p);
    QPoint operator[](int indeks) { return (*_temena)[indeks]; }
    const QPoint& operator[](int indeks) const { return (*_temena)[indeks]; }
    bool operator==(const Poligon &p) const;

    static bool ccw(const QPoint &a, const QPoint &b, const QPoint &c);
    static QPoint ekstremnaTacka(const std::vector<QPoint> &tacke);

protected:
    QVector<QPoint> *_temena;
    QVector<Duz> _duzi;
};

class HertelMehlhorn : public AlgoritamBaza
{
public:
    HertelMehlhorn(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int brojTacaka = BROJ_NASUMICNIH_TACAKA);
    ~HertelMehlhorn();

    void particionisanje();
    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    virtual void crtajAlgoritam3D() const;
    bool is_3D() const;

    QVector<Poligon*> particije() { return _particije; }
    QVector<Poligon*> particijeNaivno() { return _particijeNaivno; }
    int brojParticija() const;
    Poligon poligon() { return *_poligon; }

private:
    int indeksParticije(Poligon *p);
    void generisiTriangulaciju(std::vector<QPoint> tacke);
    bool vidljiva(const QPoint &a, const QPoint &b) const;

protected:
    Poligon* _poligon;
    QVector<Poligon*> _particije;
    QVector<Poligon*> _particijeNaivno;
    QVector<Poligon*> _triangulacija;

    void naivnoKonveksnoParticionisanje();
};

#endif // GA08_HERTELMEHLHORN_H
