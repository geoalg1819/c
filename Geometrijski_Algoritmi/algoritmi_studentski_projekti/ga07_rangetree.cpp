#include "ga07_rangetree.h"

#include <cmath>
#include <QStack>

/**
 * @brief RangeTree::Cvor::donjaGranica
 * @param y - vrednost za koju se vrsi pretraga
 * @return - pokazivac na element sek. niza koji ima y-koordinatu vecu od vrednosti argumenta y
 */
RangeTree::SekElement *RangeTree::Cvor::donjaGranica(int y)
{
    if (sek.empty())
        return nullptr;

    auto kriterijum = [](SekElement *e, const int &y) { return e->y() < y; };
    auto it = std::lower_bound(sek.begin(), sek.end(), y, kriterijum);
    if (it != sek.end())
        return *it;

    return nullptr;
}

/**
 * @brief RangeTree::RangeTree - konstruktor stabla
 * @param tacke - skup tacaka nad kojim se stablo konstruise
 */
RangeTree::RangeTree(QVector<QPoint> &tacke)
{
    Q_ASSERT(!tacke.isEmpty());

    int n = tacke.size();
    m_brojListova = static_cast<int>(pow(2, ceil(log2(n))));

    m_cvorovi = QVector<CvorPok>(m_brojListova << 1);

    std::sort(tacke.begin(), tacke.end(), [](QPoint l, QPoint d) {
        return (l.x() < d.x() || (l.x() == d.x() && l.y() < d.y()));
    });

    // Kreiranje listova (sa desna nalevo)
    for (int i = n - 1; i >= 0; --i) {
        int x = tacke.at(i).x();
        int ind = m_brojListova + i;
        m_cvorovi[ind] = new Cvor{ind, tacke.at(i), x, x};
        m_cvorovi[ind]->sek.push_back(new SekElement(0, tacke.at(i)));
    }

    // Konstruisanje unutrasnjosti stabla (sa desna nalevo)
    for (int i = m_brojListova - 1; i > 0; --i) {
        CvorPok leviCvor = m_cvorovi[indeksDeteta(i, true)];
        if (leviCvor == nullptr)
            continue;

        m_cvorovi[i] = new Cvor{i, {}, leviCvor->maks_ukupno};
        m_cvorovi[i]->maks_ukupno = leviCvor->maks_ukupno;

        CvorPok desniCvor = dete(m_cvorovi[i], false);
        if (desniCvor != nullptr)
            m_cvorovi[i]->maks_ukupno = desniCvor->maks_ukupno;

        // Kreiranje sekundarnog niza odozdo-nagore (poput merge-sort ucesljavanja)
        int indL = 0, indD = 0;
        int brTacaka = leviCvor->sek.size() + ((desniCvor) ? desniCvor->sek.size() : 0);
        SekNiz &sekNiz = m_cvorovi[i]->sek;

        for (int j = 0; j < brTacaka; ++j) {
            SekElement *novi = nullptr;
            SekElement *leviSE = (indL < leviCvor->sek.size()) ? leviCvor->sek[indL] : nullptr;
            SekElement *desniSE =
                (desniCvor && indD < desniCvor->sek.size()) ? desniCvor->sek[indD] : nullptr;

            if (leviSE == nullptr && desniSE != nullptr) {
                // Svi elementi levog niza su dodati u novi (sekundarni) niz
                novi = new SekElement(j, desniSE->tacka);

                if (j > 0 && novi->y() == sekNiz[j - 1]->y()) {
                    novi->levi = sekNiz[j - 1]->levi;
                    novi->desni = sekNiz[j - 1]->desni;
                }
                else {
                    // Sigurni smo da je indL-ti element u levom nizu strogo manji, pa
                    // pokazivac na >= element u levom nizu ostaje pokazivac na NULL
                    novi->desni = desniSE;
                }
                indD++;
            }
            else if (desniSE == nullptr && leviSE != nullptr) {
                // Svi elementi desnog niza su dodati u novi (sekundarni) niz
                novi = new SekElement(j, leviSE->tacka);

                if (j > 0 && novi->y() == sekNiz[j - 1]->y()) {
                    novi->levi = sekNiz[j - 1]->levi;
                    novi->desni = sekNiz[j - 1]->desni;
                }
                else {
                    // Sigurni smo da je indD-ti element u desnom nizu strogo manji, pa
                    // pokazivac na >= element u desnom nizu ostaje pokazivac na NULL
                    novi->levi = leviSE;
                }
                indL++;
            }
            else {
                novi = new SekElement(j);
                novi->levi = leviSE;
                novi->desni = desniSE;
                if (leviSE && leviSE->y() <= desniSE->y()) {
                    novi->tacka = leviSE->tacka;
                    indL++;
                }
                else {
                    novi->tacka = desniSE->tacka;
                    indD++;
                }
                // Izvrsi korekciju pokazivaca (u slucaju jednakih elemenata)
                if (j > 0 && novi->y() == sekNiz[j - 1]->y()) {
                    novi->levi = sekNiz[j - 1]->levi;
                    novi->desni = sekNiz[j - 1]->desni;
                }
            }
            sekNiz.push_back(novi);
        }
    }
}

RangeTree::~RangeTree()
{
    int brElem = m_cvorovi.size();
    for (int i = 1; i < brElem; ++i) {

        SekNiz niz = ((m_cvorovi[i]) ? m_cvorovi[i]->sek : SekNiz());
        int brElemSek = niz.size();
        for (int j = 0; j < brElemSek; ++j) {
            delete niz[j];
        }
        niz.clear();
        SekNiz().swap(niz);

        delete m_cvorovi[i];
    }
}

/**
 * @brief RangeTree::ispisiTackeIzOpsega
 * @param interval - posmatrani interval
 *
 * Funkcija ispisuje sve tacke stabla koje pripadaju intervalu.
 *
 */
void RangeTree::ispisiTackeIzOpsega(int intervalX[2], int intervalY[])
{
    QVector<QPoint> tacke;
    sakupiTackeIzOpsega(intervalX, intervalY, tacke);
    for (auto t : tacke) {
        QString format = QString("(%1, %2)").arg(t.x()).arg(t.y());
        std::cout << format.toStdString() << std::endl;
    }
}

/**
 * @brief RangeTree::sakupiTackeIzOpsega - prikuplja tacke u opsegu i dodaje ih u niz "tacke"
 * @param tacke - referenca na vektor u koji ce biti dodate sve tacke iz intervala
 * @param interval - posmatrani interval
 */
void RangeTree::sakupiTackeIzOpsega(int intervalX[2], int intervalY[2], QVector<QPoint> &tacke)
{
    std::sort(intervalX, intervalX + 2);
    std::sort(intervalY, intervalY + 2);

    // Provera za jednu konkretnu tacku (sadrzanu u listu stabla)
    auto tackaPripadaIntervalu = [intervalX, intervalY](Tacka tacka) {
        if (!tacka)
            return false;
        QPoint t = tacka.value();
        bool uIntervaluX = t.x() >= intervalX[0] && t.x() <= intervalX[1];
        return uIntervaluX && t.y() >= intervalY[0] && t.y() <= intervalY[1];
    };
    // Sa pretragom se zapocinje od cvora podele
    CvorPok cvorPodele = nadjiCvorPodele(intervalX);

    if (cvorPodele->list()) {
        if (tackaPripadaIntervalu(cvorPodele->tacka))
            tacke.append(cvorPodele->tacka.value());
    }
    else {
        SekElement *sekElPodele = cvorPodele->donjaGranica(intervalY[0]);
        if (sekElPodele == nullptr)
            return; // tacke kanonickog skupa cvora podele su van opsega po y

        auto nastaviSkretanje = [intervalX](CvorPok v, bool ulevo) {
            if (ulevo && (v->maks_levo >= intervalX[0]))
                return true;
            if (!ulevo && (v->maks_levo <= intervalX[1]))  // umesto < koristi se <= zbog duplikata
                return true;
            return false;
        };
        // Spustanje niz stablo i ispisivanje trazenih tacaka
        size_t smer = 1;
        do {
            CvorPok cvor = dete(cvorPodele, smer);
            SekElement *sekEl = sekElPodele->dete(smer);
            while (!cvor->list() && sekEl) {

                if (nastaviSkretanje(cvor, smer)) {
                    // Pokazivac na element odakle se zapocinje pretraga
                    SekElement *prviZaPretragu = sekEl->dete(!smer);
                    if (prviZaPretragu != nullptr) {
                        SekNiz &niz = dete(cvor, !smer)->sek;
                        for (int i = prviZaPretragu->indeks; i < niz.size(); ++i) {
                            if (!tackaPripadaIntervalu(niz[i]->tacka))
                                break;
                            tacke.append(niz[i]->tacka.value());
                        }
                    }

                    cvor = dete(cvor, smer);
                    sekEl = sekEl->dete(smer);
                }
                else {
                    cvor = dete(cvor, !smer);
                    sekEl = sekEl->dete(!smer);
                }

                if (cvor == nullptr)
                    break; // stablo nije kompletno, pa je ovo moguce
            }
            if (cvor && tackaPripadaIntervalu(cvor->tacka)) {
                tacke.append(cvor->tacka.value());
            }
        } while (smer--);
    }
}

/**
 * @brief RangeTree::ispisiTacku - ispisivanje tacke cvora v
 * @param v - cvor cija se tacka ispisuje (pretpostavka da je list)
 */
void RangeTree::ispisiTacku(CvorPok v)
{
    Q_ASSERT(v->list());
    Q_ASSERT(!v->tacka);
    QPoint t = v->tacka.value();
    QString format = QString("(%1, %2)").arg(t.x()).arg(t.y());
    std::cout << format.toStdString() << std::endl;
}

int RangeTree::nadjiIndeksCvoraPodele(int interval[2]) const
{
    CvorPok cvorPodele = nadjiCvorPodele(interval);
    if (cvorPodele == nullptr)
        return 0;
    return cvorPodele->indeks;
}

/**
 * @brief RangeTree::koren
 * @return - pokazivac na koren stabla
 */
RangeTree::CvorPok RangeTree::koren() const
{
    Q_ASSERT(m_brojListova > 0);
    return m_cvorovi.at(1);
}

/**
 * @brief RangeTree::dete
 * @param v
 * @param levo - true se trazi levo dete; inace se trazi desno
 * @return - ukoliko postoji vraca odgovarajuce dete cvora v; inace nullptr
 */
RangeTree::CvorPok RangeTree::dete(RangeTree::CvorPok v, bool levo) const
{
    Q_ASSERT(v != nullptr);
    Q_ASSERT(v->indeks > 0);
    int ind = indeksDeteta(v->indeks, levo);
    if (ind < m_cvorovi.size())
        return m_cvorovi[ind];
    return nullptr;
}

/**
 * @brief RangeTree::indeksDeteta
 * @param i - indeks cvora od cijeg deteta se trazi indeks
 * @param levi - true ukoliko se trazi levo dete; inace se trazi desno dete
 * @return - indeks odgovarajuceg deteta
 */
int RangeTree::indeksDeteta(int i, bool levo) const
{
    if (levo)
        return i << 1;
    return i << 1 | 1;
}

/**
 * @brief RangeTree::sakupiKanonickiPodskup
 * @param v - cvor koji se razmatra
 * @param tacke - kolekcija u koju se dodaju elementi kanonickog podskupa
 *
 * Funkcija prikuplja sve tacke sadrzane u listovima podstabla sa korenom v.
 *
 */
void RangeTree::sakupiKanonickiPodskup(CvorPok v, QVector<QPoint> &tacke) const
{
    Q_ASSERT(v != nullptr);
    QStack<CvorPok> stack;
    stack.push(v);
    while (!stack.isEmpty()) {
        CvorPok u = stack.pop();
        if (u->list()) {
            tacke.append(u->tacka.value());
        }
        else {
            CvorPok desno = dete(u, false);
            if (desno)
                stack.push(desno);
            CvorPok levo = dete(u, true);
            if (levo)
                stack.push(levo);
        }
    }
}

/**
 * @brief RangeTree::nadjiCvorPodele
 * @param interval - krajevi opsega koji se pretrazuje
 * @return - cvor u kojem se putevi do krajeva intervala razdvajaju ili zavrsavaju
 */
RangeTree::CvorPok RangeTree::nadjiCvorPodele(int interval[2]) const
{
    Q_ASSERT(interval[0] <= interval[1]);
    CvorPok cvor = koren();
    while (!cvor->list()) {
        if (interval[1] < cvor->maks_levo)  // umesto <= koristi se < zbog duplikata
            cvor = dete(cvor, true);
        else if (interval[0] > cvor->maks_levo)
            cvor = dete(cvor, false);
        else
            break;
    }
    // Dodatno proveri da li pronadjen cvor ima desno dete
    // Ukoliko nema, onda je cvor podele levo dole u stablu
    while (!cvor->list() && !dete(cvor, false)) {
        cvor = dete(cvor, true);
        if (cvor->list())
            break;
    }
    return cvor;
}
