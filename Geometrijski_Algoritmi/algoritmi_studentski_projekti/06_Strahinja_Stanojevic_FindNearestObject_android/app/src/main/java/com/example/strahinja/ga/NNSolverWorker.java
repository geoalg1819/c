//package com.example.strahinja.ga;
//
//import java.util.ArrayList;
//import java.util.List;
//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
///**
// *
// * @author strahinja
// */
////public class NNSolverWorker<T extends Number & Comparable<T>> {
////	private final Thread thread;
////
////	private List<KdPoint<T>> resultPoints;
////
////	public NNSolverWorker(final KdTree<T> tree, final List<KdPoint<T>> inputPoints) {
////		this.thread = new Thread(() -> {
////			resultPoints = new ArrayList<>(inputPoints.size());
////
////			final NNSolver<T> solver = new NNSolver<>(tree);
////
////			for (final KdPoint<T> point : inputPoints) {
////				resultPoints.add(solver.findNearestPoint(point));
////			}
////		});
////	}
//
//	public void start() {
//		this.thread.start();
//	}
//
//	/**
//	 * Waits for this worker thread to finish, then returns the list of result
//	 * points to the caller. The index of each result point corresponds to the index
//	 * of the input points when constructing this worker.
//	 */
//	public List<KdPoint<T>> getResultPoints() throws InterruptedException {
//		this.thread.join();
//
//		return resultPoints;
//	}
//}