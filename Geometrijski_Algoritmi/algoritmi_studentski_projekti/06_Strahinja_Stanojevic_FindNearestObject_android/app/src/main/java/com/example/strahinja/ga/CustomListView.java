package com.example.strahinja.ga;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListView extends ArrayAdapter<String>{

    private String[] names;
    private Integer[] imgID;
    private Activity context;

    public CustomListView(Activity context, String[] names, Integer[] imgID)
    {
        super(context, R.layout.listview_layout,names);

        this.context = context;
        this.names = names;
        this.imgID = imgID;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;

        ViewHolder viewHolder = null;

        if (r == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            r = inflater.inflate(R.layout.listview_layout,null,true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) r.getTag();
        }

        viewHolder.iv.setImageResource(imgID[position]);
        viewHolder.tv.setText(names[position]);

        return r;
    }

    class ViewHolder {
        TextView tv;
        ImageView iv;


        ViewHolder (View v)
        {
            tv = v.findViewById(R.id.TextView);
            iv = v.findViewById(R.id.imageView);
        }

    }
}
