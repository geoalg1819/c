/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.strahinja.ga;

import java.io.Serializable;

public class Event implements Comparable<Event>, Serializable {
    public final Point p;

    public Event(Point p) {
        this.p = p;
    }

    @Override
    public int compareTo(Event o) {
        return Point.minYOrderedCompareTo(this.p, o.p);
    }
}