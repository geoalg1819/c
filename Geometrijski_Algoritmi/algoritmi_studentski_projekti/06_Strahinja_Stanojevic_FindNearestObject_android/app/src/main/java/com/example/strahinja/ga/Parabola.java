package com.example.strahinja.ga;

import java.io.Serializable;

public class Parabola implements Serializable {
    private final double a, b, c;

    public Parabola(Point focus, double directrixY) {
        this.a = focus.x;
        this.b = focus.y;
        this.c = directrixY;
    }
}