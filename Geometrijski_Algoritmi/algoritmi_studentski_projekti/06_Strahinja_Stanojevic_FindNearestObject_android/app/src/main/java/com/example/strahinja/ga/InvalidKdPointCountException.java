package com.example.strahinja.ga;


import java.io.Serializable;

/**
 *
 * @author strahinja
 */
public class InvalidKdPointCountException extends KdTreeException implements Serializable {

	private static final long serialVersionUID = 6557152010648229680L;

}