/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.strahinja.ga;

import java.io.Serializable;

public class ArcQuery extends ArcKey implements Serializable {
    private final Point p;
    public ArcQuery(Point p) {
        this.p = p;
    }

    protected Point getLeft() {
        return p;
    }

    protected Point getRight() {
        return p;
    }
}