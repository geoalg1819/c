/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.strahinja.ga;

import java.io.Serializable;

public class CircleEvent extends Event implements Serializable {
    public final Arc arc;
    public final Point vert;

    public CircleEvent(Arc a, Point p, Point vert) {
        super(p);
        this.arc = a;
        this.vert = vert;
    }
}