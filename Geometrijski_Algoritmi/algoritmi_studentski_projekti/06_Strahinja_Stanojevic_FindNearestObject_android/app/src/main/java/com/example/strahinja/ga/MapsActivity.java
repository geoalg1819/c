package com.example.strahinja.ga;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.BoringLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import android.location.LocationListener;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String prefKey = "SHARED_PREFERENCE_KEY";
    public static final String RESTAURANT_VORONOI = "RESTAURANT_KEY";
    private static final String FIRST_ENTRANCE = "FIRST_ENTRANCE_KEY";


    private GoogleMap mMap;

    private LatLng latLngCurrent;

    private ImageButton btnLocate, btnSearch, btnVoronoi;

    private MarkerOptions options;

    private Marker marker;

    public static int screenHeight, screenWidth;

    private String searchObjectsType;

    private boolean isEnabled;

    private double longitude, latitude;

    private GetNearbyPlaces places;

    private boolean voronoiActive;

    private ProgressDialog progressRing;

    private boolean connected;

    private boolean locationPermissionGranted;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 99;

    private boolean haveInternetConnection;

    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    private boolean fisrtTime;

    private Voronoi voronoi;

    private ArrayList<Polyline> lines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        voronoiActive = false;
        connected = false;

        sharedPreferences = getSharedPreferences(prefKey, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        initialize();

        haveInternetConnection = isNetworkAvailable();

        if (!haveInternetConnection) {
            Toast.makeText(MapsActivity.this, "You need internet connection to use this application! Please connect to internet and restart application to proceed", Toast.LENGTH_LONG).show();
            return ;
        }

//        while (!haveInternetConnection)
//            haveInternetConnection = isNetworkAvailable();

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        isEnabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        locationPermissionGranted = false;
        getLocationPermission();

    }

    public void findPlaces(String object, boolean first) {
        if (object != null)
            searchObjectsType = object;

        if (!first) {
            mMap.clear();
            marker = mMap.addMarker(options);
            marker.showInfoWindow();
        }

        StringBuilder stringBuilder = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        stringBuilder.append("location=" + latLngCurrent.latitude + "," + latLngCurrent.longitude);
        stringBuilder.append("&radius=" + 20000);
//        stringBuilder.append("&rankby=distance");
        stringBuilder.append("&type=" + searchObjectsType);
        stringBuilder.append("&key=" + getResources().getString(R.string.google_maps_key));

        String url = stringBuilder.toString();

        Object dataTransfer[] = new Object[6];

        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        dataTransfer[2] = this;
        dataTransfer[3] = searchObjectsType;
        dataTransfer[4] = first;

        if (first) {
            GetNearbyPlaces place = new GetNearbyPlaces();
            place.execute(dataTransfer);
        }
        else {
            places = new GetNearbyPlaces();
            places.execute(dataTransfer);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (!locationPermissionGranted) {
            return ;
        }

//        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();

                if (!connected) {
                    btnLocate.setEnabled(true);
                    progressRing.cancel();
                    Toast.makeText(getApplicationContext(), "Successfully connected", Toast.LENGTH_LONG).show();
                    connected = true;
                    if (fisrtTime = sharedPreferences.getBoolean(FIRST_ENTRANCE, true)) {
                        latLngCurrent = new LatLng(latitude, longitude);
                        findPlaces("restaurant", true);
                        findPlaces("supermarket", true);
//                        findPlaces("hotel", true);
                        editor.putBoolean(FIRST_ENTRANCE, false);
                        editor.commit();
                    }
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                5, mLocationListener);

        ContextThemeWrapper themedContext = new ContextThemeWrapper(MapsActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        progressRing = ProgressDialog.show(themedContext, "GPS", "Connecting to GPS...", true);

        if (!isEnabled) {
            turnLocationOn();
        }

    }

    private void drawVoronoi(Voronoi voronoi)
    {
        List<VoronoiEdge> edges = voronoi.getEdges();
        Log.e("Velicina", edges.size() + "");
        Polyline line;

//        addLines(left - move, right + move, up + move, down - move);

//        for (VoronoiEdge e : edges) {
//            if (e.p1.x < left)
//                e.p1.x = left - move;
//            if (e.p2.x < left)
//                e.p2.x = left - move;
//
//            if (e.p1.x > right)
//                e.p1.x = right + move;
//            if (e.p2.x > right)
//                e.p2.x = right + move;
//
//            if (e.p1.y < down)
//                e.p1.y = down - move;
//            if (e.p2.y < down)
//                e.p2.y = down - move;
//
//            if (e.p1.y > up)
//                e.p1.y = up + move;
//            if (e.p2.y > up)
//                e.p2.y = up + move;
//        }

        for (VoronoiEdge e : edges) {
            Log.e("EDGE", e.p1.y + " " + e.p1.x + " " + e.p2.y + " " + e.p2.x);
            line = mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(e.p1.y, e.p1.x), new LatLng(e.p2.y, e.p2.x))
                    .width(5)
                    .color(Color.RED));
            lines.add(line);
        }
    }

    private void removeLines()
    {
        for (Polyline line : lines)
            line.remove();
    }

    public void turnLocationOn()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);

        alertDialog.setTitle("GPS settings");

        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                isEnabled = true;
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                MapsActivity.this.startActivity(intent);
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                progressRing.cancel();
            }
        });

        alertDialog.show();
    }

    public LatLng getCurrentLocation()
    {
        return latLngCurrent;
    }

    public void openDialog() {
        final String[] places = {"airport","restaurant", "bakery", "post office", "cafe", "hospital", "supermarket", "hotel"};

        final Integer[] placesID = new Integer[places.length];
        for (int i=0;i<places.length;i++)
            if (places[i].equals("post office"))
                placesID[i] = getPlaceID("post_office");
            else
                placesID[i] = getPlaceID(places[i]);

        ContextThemeWrapper themedContext = new ContextThemeWrapper(MapsActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);

        builder.setTitle("You are looking for the closest...");

        ListView listView = new ListView(builder.getContext());
        CustomListView customListView = new CustomListView(MapsActivity.this, places, placesID);
        listView.setAdapter(customListView);

        builder.setAdapter(customListView, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (places[i].equals("post office")) {
                    placesID[i] = getPlaceID("post_office");
                    searchObjectsType = "post_office";
                }
                else
                    searchObjectsType = new String(places[i]);
                findPlaces(null, false);
                marker.showInfoWindow();
                btnVoronoi.setEnabled(true);
            }
        });

        AlertDialog dialog = builder.create();
        ListView listViews = dialog.getListView();
        listViews.setDivider(new ColorDrawable(Color.BLACK));
        listViews.setDividerHeight(2);
        dialog.create();
        dialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.height = 652;
        dialog.getWindow().setAttributes(layoutParams);
    }

    private void initialize()
    {
        lines = new ArrayList<>();

        int btnSize = screenWidth / 8;
        int btnMoveDown = screenHeight / btnSize;

        btnLocate = findViewById(R.id.locate);
        btnLocate.setEnabled(false);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(btnSize, btnSize);
        layoutParams.setMargins(7 * btnSize, (btnMoveDown - (btnMoveDown / 4)) * btnSize, 0, 0);
        btnLocate.setLayoutParams(layoutParams);
        btnLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (marker != null)
                    marker.remove();

                latLngCurrent = new LatLng(latitude, longitude);

                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLngCurrent, 15);
                mMap.animateCamera(update);
                options = new MarkerOptions();
                options.position(latLngCurrent);
                options.title("Current location");
                marker = mMap.addMarker(options);
                marker.showInfoWindow();

                btnSearch.setEnabled(true);
            }
        });

        btnSize = screenWidth / 12;
        btnSearch = findViewById(R.id.search);
        btnSearch.setEnabled(false);
        layoutParams = new RelativeLayout.LayoutParams(btnSize, btnSize);
        layoutParams.setMargins(11 * btnSize, 2 * btnSize, 0, 0);
        btnSearch.setLayoutParams(layoutParams);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                marker.showInfoWindow();
                openDialog();
            }
        });

        btnVoronoi = findViewById(R.id.voronoi);
        btnVoronoi.setEnabled(false);
        layoutParams = new RelativeLayout.LayoutParams(btnSize, btnSize);
        layoutParams.setMargins(0, 2 * btnSize, 0, 0);
        btnVoronoi.setLayoutParams(layoutParams);
        btnVoronoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!voronoiActive) {
                    drawVoronoi(voronoi);
                    voronoiActive = true;
                }
                else {
                    places.removeLines();
                    voronoiActive = false;
                }
            }
        });
    }

    public void setVoronoi (Voronoi v)
    {
        this.voronoi = v;
    }

    private int getPlaceID (String place)
    {
        Resources res = getResources();
        int resourceId = res.getIdentifier(place, "drawable", getPackageName());
        return resourceId;
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                    onMapReady(mMap);
                }
            }
        }
    }

    public String getSearchObjectsType()
    {
        return searchObjectsType;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
