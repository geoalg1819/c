package com.example.strahinja.ga;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class GetNearbyPlaces extends AsyncTask<Object, String, String>
{
    private GoogleMap mMap;
    private ArrayList<Point> seats = new ArrayList<>();
    private List<KdPoint<Double>> points = new ArrayList<>();
    private ArrayList<String> placesNames = new ArrayList<>();
    private Voronoi voronoi;
    private ArrayList<Polyline> lines = new ArrayList<>();
    private MapsActivity mapsActivity;
    private String objectType;
    private Context context;
    private HashMap<Pair<Double, Double>, Marker> map = new HashMap<>();
    private double left, right, up, down;
    private double move;
    private double diameter;
    private boolean first;
    private String searchObject;
    private NNSolver<Double> solver;
    KdTree<Double> tree;

    @Override
    protected String doInBackground(Object... objects) {
        String url;
        InputStream inputStream;
        BufferedReader bufferedReader;
        StringBuilder stringBuilder;
        String data = null;

        mMap = (GoogleMap)objects[0];
        url = (String)objects[1];
        mapsActivity = (MapsActivity)objects[2];
        objectType = (String)objects[3];
        context = (Context)objects[2];
        first = (boolean)objects[4];

//        if (first)
            searchObject = objectType;
//        else
//            searchObject = mapsActivity.getSearchObjectsType();

        try {
            URL myURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection)myURL.openConnection();
            connection.connect();
            inputStream = connection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            data = stringBuilder.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    protected void onPostExecute(String s)
    {
        if ((searchObject.equals("restaurant") || searchObject.equals("supermarket") || searchObject.equals("hotel")) && !first) {
            Log.e("POKUPI", "IDEMO");
            try {
                String searched = searchObject + "sVoronoi";
                FileInputStream fis = context.openFileInput(searched);
                ObjectInputStream is = new ObjectInputStream(fis);
                voronoi = (Voronoi) is.readObject();
                if (voronoi != null)
                    Log.e("VORONOI", "nije null " + searched);
                mapsActivity.setVoronoi(voronoi);
                is.close();
                fis.close();
                addMarkers();
                drawPath();
                return ;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        JSONObject parentObject;

        try {
            parentObject = new JSONObject(s);
            JSONArray array = parentObject.getJSONArray("results");

            int length = array.length();
            for (int i = 0; i < length; i++) {
                JSONObject object = array.getJSONObject(i);
                JSONObject locationObj = object.getJSONObject("geometry").getJSONObject("location");

                String latitude = locationObj.getString("lat");
                String longitude = locationObj.getString("lng");

                String name = object.getString("name");
                String vicinity = object.getString("vicinity");

                double longit = Double.parseDouble(longitude);
                double latit = Double.parseDouble(latitude);

                LatLng latLng = new LatLng(latit, longit);

                double lat = latLng.latitude;
                double lon = latLng.longitude;
                Pair pair = new Pair(lat, lon);

                seats.add(new Point(longit, latit));
                points.add(new KdPoint<>(longit, latit));
                placesNames.add(new String(name + ", " + vicinity));

//                if (!first) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.title(name + ", " + vicinity);
                    markerOptions.position(latLng);
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    Marker marker = mMap.addMarker(markerOptions);
                    map.put(pair, marker);
                    if (first)
                        marker.remove();
//                }
            }

            if (placesNames.size() == 0) {
                Toast.makeText(mapsActivity, "No " + mapsActivity.getSearchObjectsType() + " close to you", Toast.LENGTH_LONG).show();
                return ;
            }

            String string = "";

            for (Point seat : seats)
                string += seat.x + " " + seat.y + ";";

            Log.e("Sedista", string);

//            for (int i=0;i<seats.size();i++) {
//                for (int j=i;j<seats.size();j++) {
//
//                    if (seats.get(i).equals(seats.get(j))) {
////                        marker = map.get(new Pair(seats.get(0).y, seats.get(0).x));
////                        marker.remove();
//                        seats.remove(j);
//                    }
//                }
//            }

            //seats = voronoi.getSites();

//            for (Point seat : seats) {
////                Log.e("USAO", "USAO");
//                LatLng latLng = new LatLng(seat.y, seat.x);
//
//                double lat = latLng.latitude;
//                double lon = latLng.longitude;
//                Pair pair = new Pair(lat, lon);
//
//                MarkerOptions markerOptions = new MarkerOptions();
////                markerOptions.title(name + ", " + vicinity);
//                markerOptions.position(latLng);
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//                Marker marker = mMap.addMarker(markerOptions);
//
//                map.put(pair, marker);
//            }

//            Log.e("Seats", seats.size() + " ");
            left = seats.get(0).x;
            right = seats.get(0).x;
            up = seats.get(0).y;
            down = seats.get(0).y;

            for (int i = 0; i < seats.size(); i++) {
                if (seats.get(i).x < left)
                    left = seats.get(i).x;
                if (seats.get(i).x > right)
                    right = seats.get(i).x;
                if (seats.get(i).y < down)
                    down = seats.get(i).y;
                if (seats.get(i).y > up)
                    up = seats.get(i).y;
            }

            ArrayList<Point> notScaled = new ArrayList<>();
            length = seats.size();
            for (int i=0;i<length;i++) {
                notScaled.add(new Point(seats.get(i).x, seats.get(i).y));
            }

            scaleX(seats, left, right, 0.00000000000000001, 0.99999999999999999);
            scaleY(seats, down, up, 0.00000000000000001, 0.99999999999999999);

            double POMOCNA = 0.99999999999999999 - 0.00000000000000001;

            voronoi = new Voronoi(seats, placesNames, notScaled);

            for (VoronoiEdge e: voronoi.getEdges()) {
                if (e.p1.x > 1.5) {
                    e.p1.y = intersectY(e.p1, e.p2, 1.5);
                    e.p1.x = 1.5;
                }
                else if (e.p1.x < -0.5) {
                    e.p1.y = intersectY(e.p1, e.p2, -0.5);
                    e.p1.x = -0.5;
                }
                if (e.p1.y > 1.5) {
                    e.p1.x = intersectX(e.p1, e.p2, 1.5);
                    e.p1.y = 1.5;
                }
                else if (e.p1.y < -0.5) {
                    e.p1.x = intersectX(e.p1, e.p2, -0.5);
                    e.p1.y = -0.5;
                }

                if (e.p2.x > 1.5) {
                    e.p2.y = intersectY(e.p1, e.p2, 1.5);
                    e.p2.x = 1.5;
                }
                else if (e.p2.x < -0.5) {
                    e.p2.y = intersectY(e.p1, e.p2, -0.5);
                    e.p2.x = -0.5;
                }
                if (e.p2.y > 1.5) {
                    e.p2.x = intersectX(e.p1, e.p2, 1.5);
                    e.p2.y = 1.5;
                }
                else if (e.p2.y < -0.5) {
                    e.p2.x = intersectX(e.p1, e.p2, -0.5);
                    e.p2.y = -0.5;
                }
            }

            double leftPom = -0.5;//v.getEdges().get(0).p1.x;
            double rightPom = 1.5;//v.getEdges().get(0).p1.x;
            double upPom = 1.5;//v.getEdges().get(0).p1.x;
            double downPom = -0.5;//v.getEdges().get(0).p1.x;

            double diffRightLeft = right - left;
            double diffUpDown = up - down;

            double scaleFactorLeftRight = (diffRightLeft * 0.51) / POMOCNA;
            double scaleFactorUpDown = (diffUpDown * 0.51) / POMOCNA;

            scaleXEdge(voronoi.getEdges(), leftPom, rightPom, left - scaleFactorLeftRight, right + scaleFactorLeftRight);
            scaleYEdge(voronoi.getEdges(), downPom, upPom, down - scaleFactorUpDown, up + scaleFactorUpDown);

            mapsActivity.setVoronoi(voronoi);

            tree = new KdTree<>(points);
            solver = new NNSolver<>(tree);

            if (first) {
                FileOutputStream fos = null;
                ObjectOutputStream os = null;
                String searched = searchObject + "sVoronoi";
                try {
                    fos = context.openFileOutput(searched, Context.MODE_PRIVATE);
                    os = new ObjectOutputStream(fos);
                    os.writeObject(voronoi);
                    os.close();
                    fos.close();
                    searched = searchObject + "sSolver";
                    Log.e("MakeTree", searched);
                    fos = context.openFileOutput(searched, Context.MODE_PRIVATE);
                    os = new ObjectOutputStream(fos);
                    os.writeObject(solver);
                    os.close();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            drawPath();

//            Point p1 = new Point(left, down);
//            Point p2 = new Point(right, up);
//            diameter = p1.distanceTo(p2);
//
//            double dis1 = right - left;
//            double dis2 = up - down;
//
//            dis1 = dis1 < dis2 ? dis2 : dis1;

//            move = (diameter - dis1) / 2;

//            for (Point p : seats) {
//                p.x = (p.x * (-10)) / (left - move);
//                p.y = (p.y * 10) / (up - move);
//
//                MarkerOptions options = new MarkerOptions();
//                options.position(new LatLng(p.y, p.x));
//                mMap.addMarker(options);
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addMarkers() {
        int length = voronoi.getSitesNotScaled().size();
        ArrayList<Point> s = voronoi.getSitesNotScaled();
        ArrayList<String> places = voronoi.getPlacesNames();

        for (int i=0;i<length;i++) {
            double lat = s.get(i).y;
            double lon = s.get(i).x;
            LatLng latLng = new LatLng(lat, lon);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.title(places.get(i));
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            Marker marker = mMap.addMarker(markerOptions);
            Pair pair = new Pair(lat, lon);
            map.put(pair, marker);
        }
    }

    private void scaleX (ArrayList<Point> sit, double min, double max)
    {
        for (Point site : sit) {
//            site.x = ((0.99 - 0.01) * (site.x - min)) / (max - min) + 0.01;
            site.x = ((site.x - min)) / (max - min);
        }
    }
    private void scaleY (ArrayList<Point> sit, double min, double max)
    {
        for (Point site : sit) {
//            site.y = ((0.99 - 0.01) * (site.y - min)) / (max - min) + 0.01;
            site.y = (site.y - min) / (max - min);
        }
    }

    private void addLines(double left, double right, double up, double down) {
        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(up, left), new LatLng(up, right))
                .width(5)
                .color(Color.BLUE));

        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(up, left), new LatLng(down, left))
                .width(5)
                .color(Color.BLUE));

        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(down, left), new LatLng(down, right))
                .width(5)
                .color(Color.BLUE));

        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(up, right), new LatLng(down, right))
                .width(5)
                .color(Color.BLUE));
    }

    public void drawPath()
    {

        if (searchObject.equals("restaurant") || searchObject.equals("supermarket") || searchObject.equals("hotel") && !first) {
            try {
                String searched = searchObject + "sSolver";
                Log.e("TakeTREE", searched);
                FileInputStream fis = context.openFileInput(searched);
                ObjectInputStream is = new ObjectInputStream(fis);
                solver = (NNSolver<Double>) is.readObject();
                is.close();
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
//        else {
//            KdTree<Double> tree = new KdTree<>(points);
//
//            solver = new NNSolver<>(tree);
//        }

        if (!first) {
            LatLng current = mapsActivity.getCurrentLocation();

            KdPoint<Double> searchPoint = new KdPoint<>(current.longitude, current.latitude);
            KdPoint<Double> nearestPoint = solver.findNearestPoint(searchPoint);

            //int closest = closestObject();

            mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(mapsActivity.getCurrentLocation().latitude, mapsActivity.getCurrentLocation().longitude), new LatLng(nearestPoint.getAxisValue(1), nearestPoint.getAxisValue(0)))
                    .width(5)
                    .color(Color.BLUE));

            LatLng latLng = new LatLng(Math.abs(mapsActivity.getCurrentLocation().latitude + nearestPoint.getAxisValue(1)) / 2, Math.abs(mapsActivity.getCurrentLocation().longitude + nearestPoint.getAxisValue(0)) / 2);
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(update);

            for (Pair<Double, Double> key : map.keySet())
                Log.e("Kljuc", key.first + " " + key.second + " " + map.get(key).toString());
            Log.e("LookinFor", nearestPoint.getAxisValue(1) + " " + nearestPoint.getAxisValue(0));

            final Marker marker = map.get(new Pair(nearestPoint.getAxisValue(1), nearestPoint.getAxisValue(0)));
            marker.showInfoWindow();
        }
//        mMap.getUiSettings().setMapToolbarEnabled(false);

        //Toast.makeText(context, "The closest " + objectType + " to you is " + placesNames.get(closest), Toast.LENGTH_LONG).show();
    }


    public void drawVoronoi(Voronoi voronoi)
    {
        Log.e("USAO U DRawVoronoi", "");
        List<VoronoiEdge> edges = voronoi.getEdges();
        Polyline line;

//        addLines(left - move, right + move, up + move, down - move);

//        for (VoronoiEdge e : edges) {
//            if (e.p1.x < left)
//                e.p1.x = left - move;
//            if (e.p2.x < left)
//                e.p2.x = left - move;
//
//            if (e.p1.x > right)
//                e.p1.x = right + move;
//            if (e.p2.x > right)
//                e.p2.x = right + move;
//
//            if (e.p1.y < down)
//                e.p1.y = down - move;
//            if (e.p2.y < down)
//                e.p2.y = down - move;
//
//            if (e.p1.y > up)
//                e.p1.y = up + move;
//            if (e.p2.y > up)
//                e.p2.y = up + move;
//        }

        for (VoronoiEdge e : edges) {
           line = mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(e.p1.y, e.p1.x), new LatLng(e.p2.y, e.p2.x))
                    .width(5)
                    .color(Color.RED));
           lines.add(line);
        }
    }

    public void removeLines()
    {
        for (Polyline line : lines)
            line.remove();
    }

    public int closestObject()
    {
        LatLng current = mapsActivity.getCurrentLocation();
        double minDistance = distance(current, seats.get(0));
        double currentDistance;
        int minIndex = 0;

        int length = seats.size();
        for (int i = 1; i  < length; i++) {
            currentDistance = distance(current, seats.get(i));
            if (currentDistance < minDistance) {
                minDistance = currentDistance;
                minIndex = i;
            }
        }

        return minIndex;
    }

    public double distance(LatLng latLng1, Point point)
    {
        return Math.sqrt((latLng1.longitude - point.x) * (latLng1.longitude - point.x) + (latLng1.latitude - point.y) * (latLng1.latitude - point.y));
    }

    private static void scaleX (ArrayList<Point> sit, double min, double max, double a, double b)
    {
        for (Point site : sit) {
            site.x = ((b - a) * (site.x - min)) / (max - min) + a;
        }
    }

    private static void scaleY (ArrayList<Point> sit, double min, double max, double a, double b)
    {
        for (Point site : sit) {
            site.y = ((b - a) * (site.y - min)) / (max - min) + a;
        }
    }

    private static void scaleXEdge (ArrayList<VoronoiEdge> edges, double min, double max, double a, double b)
    {
        for (VoronoiEdge e : edges) {
            if (e.p1.x > -1 && e.p1.x < 2)
                e.p1.x = ((b - a) * (e.p1.x - min)) / (max - min) + a;
            if (e.p2.x > -1 && e.p2.x < 2)
                e.p2.x = ((b - a) * (e.p2.x - min)) / (max - min) + a;
        }
    }

    private static void scaleYEdge (ArrayList<VoronoiEdge> edges, double min, double max, double a, double b)
    {
        for (VoronoiEdge e : edges) {
            if (e.p1.y > -1 && e.p1.y < 2)
                e.p1.y = ((b - a) * (e.p1.y - min)) / (max - min) + a;
            if (e.p2.y > -1 && e.p2.y < 2)
                e.p2.y = ((b - a) * (e.p2.y - min)) / (max - min) + a;
        }
    }

    public static double intersectY(Point x, Point y, double xValue)
    {
        return ((y.y - x.y) / (y.x - x.x)) * (xValue - x.x) + x.y;
    }

    public static double intersectX(Point x, Point y, double yValue)
    {
        return (yValue - x.y) * ((y.x - x.x) / (y.y - x.y)) + x.x;
    }
}