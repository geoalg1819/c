package com.example.strahinja.ga;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;

/**
 *
 * @author strahinja
 */
public class NNSolverInterruptedException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = -612945504696111587L;

	public NNSolverInterruptedException(final Throwable cause) {
		super(cause);
	}
}