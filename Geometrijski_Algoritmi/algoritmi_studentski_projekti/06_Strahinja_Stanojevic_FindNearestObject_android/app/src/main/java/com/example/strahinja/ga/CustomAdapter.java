package com.example.strahinja.ga;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter
{

    Context context;
    ArrayList<SingleRow> arrayList;

    public CustomAdapter(Context c)
    {
        this.context = c;
        arrayList = new ArrayList<>();
        Resources res = context.getResources();
        String places[] = res.getStringArray(R.array.places);
        int images[] = {R.drawable.atm, R.drawable.bank, R.drawable.hospital, R.drawable.hotel};

        for (int i = 0; i < places.length; i++)
            arrayList.add(new SingleRow(places[i], images[i]));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.custom_listview, viewGroup, false);
        TextView textView = row.findViewById(R.id.textView);
        ImageView imageView = row.findViewById(R.id.imageView);

        int imageSize = MapsActivity.screenWidth / 8;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(imageSize, imageSize);
        layoutParams.setMargins(0, 10 ,0, 10);
        imageView.setLayoutParams(layoutParams);

        SingleRow tmp = arrayList.get(i);
        textView.setText(tmp.name);
        imageView.setImageResource(tmp.image);

        return imageView;
    }
}
