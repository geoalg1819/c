#include "ga04_kdtree.h"
#include <QPainter>

KdTreeNode::KdTreeNode(QPoint d, QPoint g): dole(d),gore(g){}

KdTreeNode::~KdTreeNode(){};

KdTree::KdTree(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke,int broj_tacaka, int n, QPoint point)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka), _n(n),trazena(point)
{
    if (imeDatoteke == "")
        _tacke = generisiNasumicneTacke(broj_tacaka);
    else
        _tacke = ucitajPodatkeIzDatoteke(imeDatoteke);
}

QPoint KdTree::median(int axis, std::vector<QPoint> &plist, std::vector<QPoint> &left, std::vector<QPoint> &right)
{
    QPoint median;
    int size = plist.size();
    int med=ceil(float(size)/float(2));
    int count = 0;
    if(size== 1)
        return plist.front();

    std::sort(plist.begin(),plist.end(),[axis](QPoint& a, QPoint& b){
        if(axis%2==0)
            return a.x() < b.x();
        else {
            return a.y() < b.y();
        }
    });

    for(auto& x : plist)
    {
        if(count < med)
            left.push_back(x);
        else
            right.push_back(x);
        ++count;
    }

    median = left.back();
    left.pop_back();
    return median;
}

void KdTree::makeTree(std::vector<QPoint> &plist)
{
    KdTreeNode* head=new KdTreeNode();
    KdTree::makeTreeHelper(head,plist,QPoint(10,10),QPoint(1533,905),0);
    KdTree::_root = head;
}

void KdTree::makeTreeHelper(KdTreeNode *head, std::vector<QPoint> &plist, QPoint d, QPoint g,  int depth)
{
    if(!plist.empty())
    {
        int k=2;
        int axis=depth%k;

        std::vector<QPoint> left_list;
        std::vector<QPoint> right_list;

        QPoint median=KdTree::median(axis,plist,left_list,right_list);
        head->p=median;
        head->dole=d;
        head->gore=g;

        KdTreeNode* left_node= new KdTreeNode();
        KdTreeNode* right_node= new KdTreeNode();

        if(axis%2==0)
            KdTree::makeTreeHelper(left_node,left_list,head->dole,QPoint(head->p.x(),head->gore.y()),depth+1);
        else
            KdTree::makeTreeHelper(left_node,left_list,head->dole,QPoint(head->gore.x(),head->p.y()),depth+1);

        if(!left_list.empty())
            head->left=left_node;
        if(axis%2==0)
            KdTree::makeTreeHelper(right_node,right_list,QPoint(head->p.x(),head->dole.y()),head->gore,depth+1);
        else
            KdTree::makeTreeHelper(right_node,right_list,QPoint(head->dole.x(),head->p.y()),head->gore,depth+1);

        if(!right_list.empty())
            head->right=right_node;
    }
}

void KdTree::printTree(KdTreeNode *head)
{
    if(head==NULL)
        return;

    printTree(head->left);
    std::cout <<"x: " << head->p.x() << " y: " <<head->p.y() << std::endl;
    printTree(head->right);
}

void KdTree::pokreniAlgoritamHelper(KdTreeNode *head, int axis)
{
    if(head==nullptr)
        return;
    if(axis%2==0)
    {
        if(trazena.x() <=head->p.x()){
            pokreniAlgoritamHelper(head->left,axis+1);
        }
        else {
            pokreniAlgoritamHelper(head->right,axis+1);
        }
    }
    else {
        if(trazena.y() <=head->p.y()){
            pokreniAlgoritamHelper(head->left,axis+1);
        }
        else {
            pokreniAlgoritamHelper(head->right,axis+1);
        }
    }

    if(trazeneTacke.size()<_n){
        trazeneTacke.push_back(head->p);
        AlgoritamBaza_updateCanvasAndBlock();
    }
    else {
        double max=sqrt(pow(trazeneTacke[0].x()-trazena.x(),2)+pow(trazeneTacke[0].y()-trazena.y(),2));
        int j=0;
        for (int i=1;i<trazeneTacke.size();i++) {
            double rastojanje=sqrt(pow(trazeneTacke[i].x()-trazena.x(),2)+pow(trazeneTacke[i].y()-trazena.y(),2));

            if(max < rastojanje)
            {
                max=rastojanje;
                j=i;
            }
        }
        double curRas=sqrt(pow(head->p.x()-trazena.x(),2)+pow(head->p.y()-trazena.y(),2));
        if(curRas < max){
            trazeneTacke[j]=head->p;
            AlgoritamBaza_updateCanvasAndBlock();
        }

    }

    if(axis%2==0)
    {
        if(trazena.x() <=head->p.x()){
            pokreniAlgoritamHelper(head->right,axis+1);
        }
        else {
            pokreniAlgoritamHelper(head->left,axis+1);
        }
    }
    else {
        if(trazena.y() <=head->p.y()){
            pokreniAlgoritamHelper(head->right,axis+1);
        }
        else {
            pokreniAlgoritamHelper(head->left,axis+1);
        }
    }

}

void KdTree::pokreniAlgoritam()
{
    makeTree(_tacke);
    trazeneTacke.clear();
    pokreniAlgoritamHelper(_root,0);
    emit animacijaZavrsila();

}

void KdTree::pokreniNaivniAlgoritam()
{

    trazeneTacke.clear();
    QPoint tr=trazena;

    std::sort(_tacke.begin(),_tacke.end(),[tr](QPoint a,QPoint b){
       double dist1= sqrt(pow(a.x()-tr.x(),2)+pow(a.y()-tr.y(),2));
       double dist2= sqrt(pow(b.x()-tr.x(),2)+pow(b.y()-tr.y(),2));

       return dist1 > dist2;
    });

    for (int i=0;i<_n;i++) {
        trazeneTacke.push_back(_tacke.back());
        _tacke.pop_back();

    }

    emit animacijaZavrsila();
}

void KdTree::crtajAlgoritam(QPainter &painter) const
{
    painter.setBrush(QBrush(Qt::red));
    painter.drawEllipse(trazena,10,10);

    crtajStablo(painter,_root,0);
    painter.setBrush(QBrush(Qt::green));
    for (int i =0; i < _tacke.size(); i++) {
        painter.drawEllipse(_tacke[i],5,5);
    }
    painter.setBrush(QBrush(Qt::red));
    for (int i =0; i < trazeneTacke.size(); i++) {
        painter.drawEllipse(trazeneTacke[i],7,7);
    }
    return;
}

void KdTree::crtajStablo(QPainter &painter, KdTreeNode* root,int axis) const
{
    if(root==nullptr)
        return;
    if(axis%2==0)
        painter.drawLine(QPoint(root->p.x(),root->dole.y()),QPoint(root->p.x(),root->gore.y()));
    else
        painter.drawLine(QPoint(root->dole.x(),root->p.y()),QPoint(root->gore.x(),root->p.y()));

    crtajStablo(painter,root->left,axis+1);
    crtajStablo(painter,root->right,axis+1);
}

void KdTree::crtajAlgoritam3D() const
{
    return;
}

bool KdTree::is_3D() const
{
    return false;
}

void KdTree::pokreniAlgoritamBenchmarkHelper(KdTreeNode *head, int axis)
{
    if(head==nullptr)
        return;
    if(axis%2==0)
    {
        if(trazena.x() <=head->p.x()){
            pokreniAlgoritamBenchmarkHelper(head->left,axis+1);
        }
        else {
            pokreniAlgoritamBenchmarkHelper(head->right,axis+1);
        }
    }
    else {
        if(trazena.y() <=head->p.y()){
            pokreniAlgoritamBenchmarkHelper(head->left,axis+1);
        }
        else {
            pokreniAlgoritamBenchmarkHelper(head->right,axis+1);
        }
    }

    if(trazeneTacke.size()<_n){
        trazeneTacke.push_back(head->p);
    }
    else {
        double max=sqrt(pow(trazeneTacke[0].x()-trazena.x(),2)+pow(trazeneTacke[0].y()-trazena.y(),2));
        int j=0;
        for (int i=1;i<trazeneTacke.size();i++) {
            double rastojanje=sqrt(pow(trazeneTacke[i].x()-trazena.x(),2)+pow(trazeneTacke[i].y()-trazena.y(),2));

            if(max < rastojanje)
            {
                max=rastojanje;
                j=i;
            }
        }
        double curRas=sqrt(pow(head->p.x()-trazena.x(),2)+pow(head->p.y()-trazena.y(),2));
        if(curRas < max){
            trazeneTacke[j]=head->p;
        }

    }


    if(axis%2==0)
    {
        if(trazena.x() <=head->p.x()){
            pokreniAlgoritamBenchmarkHelper(head->right,axis+1);
        }
        else {
            pokreniAlgoritamBenchmarkHelper(head->left,axis+1);
        }
    }
    else {
        if(trazena.y() <=head->p.y()){
            pokreniAlgoritamBenchmarkHelper(head->right,axis+1);
        }
        else {
            pokreniAlgoritamBenchmarkHelper(head->left,axis+1);
        }
    }

}

std::vector<QPoint> KdTree::pokreniAlgoritamBenchmark()
{
    makeTree(_tacke);
    pokreniAlgoritamBenchmarkHelper(_root,0);
    QPoint tr = trazena;
    std::sort(trazeneTacke.begin(),trazeneTacke.end(),[tr](QPoint a,QPoint b){
       double dist1= sqrt(pow(a.x()-tr.x(),2)+pow(a.y()-tr.y(),2));
       double dist2= sqrt(pow(b.x()-tr.x(),2)+pow(b.y()-tr.y(),2));

       return dist1 < dist2;
    });
    std::cout << "trazeneTacke" << std::endl;
    for (int i=0;i<trazeneTacke.size();i++) {
        std::cout << "x: " << trazeneTacke[i].x() << " y: " << trazeneTacke[i].y() << std::endl;
    }
    std::cout << "kraj trazeneTacke" << std::endl;
    return trazeneTacke;
}

std::vector<QPoint> KdTree::pokreniNaivniAlgoritamBenchmark()
{
    QPoint tr=trazena;
    std::sort(_tacke.begin(),_tacke.end(),[tr](QPoint a,QPoint b){
       double dist1= sqrt(pow(a.x()-tr.x(),2)+pow(a.y()-tr.y(),2));
       double dist2= sqrt(pow(b.x()-tr.x(),2)+pow(b.y()-tr.y(),2));

       return dist1 > dist2;
    });

    for (int i=0;i<_n;i++) {
        trazeneTackeNaivni.push_back(_tacke.back());
        _tacke.pop_back();
    }
    std::cout << "trazeneTackeNaivni" << std::endl;
    for (int i=0;i<trazeneTackeNaivni.size();i++) {
        std::cout << "x: " << trazeneTackeNaivni[i].x() << " y: " << trazeneTackeNaivni[i].y() << std::endl;
    }
    std::cout << "kraj trazeneTackeNaivni" << std::endl;
    return trazeneTackeNaivni;
}


