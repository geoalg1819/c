#include "ga10_trilateracija.h"
#include <iostream>
#include <QtCore/QDebug>
#include <iostream>
#include <fstream>
#include <cstdlib>

const double PI = 3.1415926535;
const double EARTHR = 6371;
const double EPSILON = 0.01;

using namespace std;

Trilateracija::Trilateracija(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int brojTacaka)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{

    double lat, lon, r;
    if (imeDatoteke == "") {
        randomPoints();
    } else{
        fromFile(imeDatoteke);
    }



}

void Trilateracija::randomPoints() {
     double lat, lon, r;


    double lat1, lon1, lat2, lon2, lat3, lon3, r1, r2, r3;

    int range_max = 60000000;
    int range_min = 30000000;

    lat = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    cout.precision(8);
    lat = lat / 1000000;

    lat1 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lat1 = lat1 / 1000000;


    lat2 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lat2 = lat2 / 1000000;

    lat3 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lat3 = lat3 / 1000000;



    range_max = 60000000;
    range_min = -2000000;
    lon = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lon = lon / 1000000;

    lon1 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lon1 = lon1 / 1000000;
    _as.set(lat1, lon1, calcDistance(lat, lon, lat1, lon1));

    cout << lat1 << " " << lon1 << " " <<  calcDistance(lat, lon, lat1, lon1) << endl;

    lon2 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lon2 = lon2 / 1000000;
    _bs.set(lat2, lon2, calcDistance(lat, lon, lat2, lon2));

    cout << lat2 << " " << lon2 << " " <<  calcDistance(lat, lon, lat2, lon2) << endl;


    lon3 = (double)rand() / (RAND_MAX) * (range_max - range_min) + range_min;
    lon3 = lon3 / 1000000;
    _cs.set(lat3, lon3, calcDistance(lat, lon, lat3, lon3));

    cout << lat3 << " " << lon3 << " " <<  calcDistance(lat, lon, lat3, lon3) << endl;

}

void Trilateracija::fromFile(string imeDatoteke) {
    ifstream myFile;
    double lat, lon, r;
    myFile.open(imeDatoteke);
    if (myFile.is_open()) {
        myFile >> lat >> lon >> r;
        _as.set(lat, lon, r);
        myFile >> lat >> lon >> r;
        _bs.set(lat, lon, r);
        myFile >> lat >> lon >> r;
        _cs.set(lat, lon, r);

    }

    myFile.close();
}

void Trilateracija::pokreniAlgoritam()
{

    if (_as.lat == _bs.lat && _as.lon == _bs.lon) {
        cout << "podaci nisu ispravni" << endl;
        return;
    }

    if (_as.lat == _cs.lat && _as.lon == _cs.lon) {
        cout << "podaci nisu ispravni" << endl;
        return;
    }

    if (_bs.lat == _cs.lat && _bs.lon == _cs.lon) {
        cout << "podaci nisu ispravni" << endl;
        return;
    }
    trilateration(_as, _bs, _cs);

    AlgoritamBaza_updateCanvasAndBlock();

    emit this->animacijaZavrsila();
}

double Trilateracija::calcDistance(double lat1, double lon1, double lat2, double lon2) {
    double f1 = deg2Rads(lat1);
    double f2 = deg2Rads(lat2);
    double df = deg2Rads(lat2- lat1);
    double dl = deg2Rads(lon2 - lon1);

    double a = sin(df/2) * sin(df/2) + cos(f1) * cos(f2) * sin(dl/2) * sin(dl/2);
    double c = 2* asin(min(1.0, sqrt(a)));

    double r = 6371;
    return c * r;
}

vector<double> Trilateracija::pokreniAlgoritamTest()
{
    return trilateration(_as, _bs, _cs);
}

void Trilateracija::crtajAlgoritam(QPainter &painter) const
{

}

void Trilateracija::crtajAlgoritam3D() const
{

    glColor3d(1.0,0.0,0.0); // red x
    glBegin(GL_LINES);
    // x aix

    glVertex3d(-4.0, 0.0, 0.0);
    glVertex3d(4.0, 0.0, 0.0);
    glEnd();

    // y
    glColor3d(0.0,1.0,0.0); // green y
    glBegin(GL_LINES);
    glVertex3d(0.0, -4.0, 0.0);
    glVertex3d(0.0, 4.0, 0.0);
    glEnd();

    // z
    glColor3d(0.0,0.0,1.0); // blue z
    glBegin(GL_LINES);
    glVertex3d(0.0, 0.0 ,-4.0 );
    glVertex3d(0.0, 0.0 ,4.0 );
    glEnd();

    glColor3d(1, 0, 0.4);

    glBegin(GL_POINTS);
    glVertex3d(_a.x, _a.y, _a.z);
    glVertex3d(_b.x, _b.y, _b.z);
    glVertex3d(_c.x, _c.y, _c.z);
    glColor3d(1, 0.4, 0.4);
    glVertex3d(_rez.x, _rez.y, _rez.z);
    glEnd();


    double r = 0.3;
    double u, v;

    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //earth
    r = EARTHR/1000;
    glColor4d(0.6, 0.6, 0.3, 0.3);
    // Sphere shape data
//    glTranslated(_c.x, _c.y, _c.z);
    for (u = 0; u < PI; u += PI / 20) {
        glBegin(GL_TRIANGLE_STRIP);
        for (v = 0; v <= PI*2 + EPSILON; v += PI / 20) {
            set_normal_and_vertex(u, v, r);
            set_normal_and_vertex(u + PI / 20, v, r);
        }

        glEnd();
    }
//    glTranslated(-_c.x, -_c.y, -_c.z);

    r = _c.r;
    glColor4d(0, 1, 0, 0.3);
    // Sphere shape data
    glTranslated(_c.x, _c.y, _c.z);
    for (u = 0; u < PI; u += PI / 20) {
        glBegin(GL_TRIANGLE_STRIP);
        for (v = 0; v <= PI*2 + EPSILON; v += PI / 20) {
            set_normal_and_vertex(u, v, r);
            set_normal_and_vertex(u + PI / 20, v, r);
        }

        glEnd();
    }
    glTranslated(-_c.x, -_c.y, -_c.z);

    glColor4d(0, 0, 1, 0.3);

    // Sphere shape data
    r = _a.r;
    glTranslated(_a.x, _a.y, _a.z);
    for (u = 0; u < PI; u += PI / 20) {
        glBegin(GL_TRIANGLE_STRIP);
        for (v = 0; v <= PI*2 + EPSILON; v += PI / 20) {
            set_normal_and_vertex(u, v, r);
            set_normal_and_vertex(u + PI / 20, v, r);
        }

        glEnd();
    }
    glTranslated(-_a.x, -_a.y, -_a.z);


    glColor4d(1, 0, 0, 0.3);
    // Sphere shape data

    r =_b.r;
    glTranslated(_b.x, _b.y, _b.z);
    for (u = 0; u < PI; u += PI / 20) {
        glBegin(GL_TRIANGLE_STRIP);
        for (v = 0; v <= PI*2 + EPSILON; v += PI / 20) {
            set_normal_and_vertex(u, v, r);
            set_normal_and_vertex(u + PI / 20, v, r);
        }

        glEnd();
    }
    glTranslated(-_b.x, -_b.y, -_b.z);

    // Sphere shape data

    r = 0.1;
    glTranslated(_rez.x, _rez.y, _rez.z);
    for (u = 0; u < PI; u += PI / 20) {
        glBegin(GL_TRIANGLE_STRIP);
        for (v = 0; v <= PI*2 + EPSILON; v += PI / 20) {
            set_normal_and_vertex(u, v, r);
            set_normal_and_vertex(u + PI / 20, v, r);
        }

        glEnd();
    }
    glTranslated(-_rez.x, -_rez.y, -_rez.z);
}

void Trilateracija::set_normal_and_vertex(double u, double v, double r) const
{
    glNormal3d(
                r *sin(u) * sin(v),
                r * cos(u),
                r * sin(u) * cos(v)
                );
    glVertex3d(
                r * sin(u) * sin(v),
                r * cos(u),
                r *sin(u) * cos(v)
                );
}

void Trilateracija::pokreniNaivniAlgoritam()
{

}

bool Trilateracija::is_3D() const
{
    return true;
}


vector<double> Trilateracija::trilateration(LatLong a, LatLong b, LatLong c)
{
    double yA = EARTHR * (cos(deg2Rads(a.lat)) * sin(deg2Rads(a.lon)));
    double xA = EARTHR * (cos(deg2Rads(a.lat)) * cos(deg2Rads(a.lon)));
    double zA = EARTHR * (sin(deg2Rads(a.lat)));

    double xB = EARTHR * (cos(deg2Rads(b.lat)) * cos(deg2Rads(b.lon)));
    double yB = EARTHR * (cos(deg2Rads(b.lat)) * sin(deg2Rads(b.lon)));
    double zB = EARTHR * (sin(deg2Rads(b.lat)));

    double xC = EARTHR * (cos(deg2Rads(c.lat)) * cos(deg2Rads(c.lon)));
    double yC = EARTHR * (cos(deg2Rads(c.lat)) * sin(deg2Rads(c.lon)));
    double zC = EARTHR * (sin(deg2Rads(c.lat)));


    vector<double> P1 = {xA, yA, zA};
    vector<double> P2 = {xB, yB, zB};
    vector<double> P3 = {xC, yC, zC};

    _a.set(xA / 1000, yA /1000, zA /1000, a.r / 1000);
    _b.set(xB / 1000, yB / 1000, zB / 1000, b.r / 1000);
    _c.set(xC / 1000, yC / 1000, zC / 1000, c.r / 1000);


    vector<double> ex = vectorDiv( vectorDiff(P2, P1) , vectorNorm(vectorDiff(P2, P1)));
    double x3 = vectorDot(ex, vectorDiff(P3, P1));
    vector<double> diff = vectorDiff(vectorDiff(P3, P1), vectorMul(x3, ex));
    vector<double> ey = vectorDiv(diff, vectorNorm(diff));
    vector<double> ez = vectorCross(ex, ey);
    double x2 = vectorNorm( vectorDiff(P2, P1) );
    double y3 = vectorDot(ey, vectorDiff(P3, P1));
    double x = (pow(a.r,2) - pow(b.r,2) + pow(x2,2))/(2*x2);
    double y = ((pow(a.r,2) - pow(c.r,2) + pow(x3,2) + pow(y3,2))/(2*y3)) - ((x3/y3)*x);
    double z = sqrt(abs(pow(a.r,2) - pow(x,2) - pow(y,2)));

    vector<double> triPt = vectorAdd(vectorAdd(P1, vectorMul(x, ex)), vectorAdd(vectorMul(y, ey), vectorMul(z, ez)));

    _rez.set(triPt[0] / 1000, triPt[1] / 1000, triPt[2] /1000, 0.1);

    double lat = rad2Degs(asin(triPt[2] / EARTHR));
    double lon = rad2Degs(atan2(triPt[1],triPt[0]));


    cout << lat << endl;
    cout << lon << endl;

    return {lat, lon};
}


double Trilateracija::deg2Rads(double x)
{
    return (PI / 180.0) * x;
}

double Trilateracija::rad2Degs(double x)
{
    return (180.0 / PI) * x;
}

vector<double> Trilateracija::vectorMul(vector<double> a, vector<double> b)
{
    vector<double> result = {0, 0, 0};
    result[0] = a[0]*b[0];
    result[1] = a[1]*b[1];
    result[2] = a[2]*b[2];

    return result;
}

vector<double> Trilateracija::vectorMul(double a, vector<double> b)
{
    vector<double> result = {0, 0, 0};
    result[0] = a*b[0];
    result[1] = a*b[1];
    result[2] = a*b[2];

    return result;
}

vector<double> Trilateracija::vectorDiv(vector<double> a, vector<double> b)
{
    vector<double> result = {0, 0, 0};
    result[0] = a[0] / b[0];
    result[1] = a[1] / b[1];
    result[2] = a[2] / b[2];

    return result;
}

vector<double> Trilateracija::vectorDiv(vector<double> a, double b)
{
    vector<double> result = {0, 0, 0};
    result[0] = a[0] / b;
    result[1] = a[1] / b;
    result[2] = a[2] / b;

    return result;
}

vector<double> Trilateracija::vectorDiff(vector<double> a,vector<double> b)
{
    vector<double> result = {0,0,0};
    result[0]=a[0]-b[0];
    result[1]=a[1]-b[1];
    result[2]=a[2]-b[2];

    return result;
}

vector<double> Trilateracija::vectorAdd(vector<double> a,vector<double> b)
{
    vector<double> result = {0,0,0};
    result[0]=a[0]+b[0];
    result[1]=a[1]+b[1];
    result[2]=a[2]+b[2];

    return result;
}

double Trilateracija::vectorDot(vector<double> a,vector<double> b)
{
    return (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
}

vector<double> Trilateracija::vectorCross(vector<double> a,vector<double> b)
{
    vector<double> result = {0,0,0};
    result[0] = a[1]*b[2]-a[2]*b[1];
    result[1] = a[2]*b[0]-a[0]*b[2];
    result[2] = a[0]*b[1]-a[1]*b[0];

    return result;
}

double Trilateracija::vectorNorm(vector<double> a)
{
    return sqrt(vectorDot(a,a));
}

vector<double> Trilateracija::vectorNormalize(vector<double> a){
    vector<double> result = {0,0,0};
    double mag = sqrt(vectorDot(a,a));
    result[0] = a[0]/mag;
    result[1] = a[1]/mag;
    result[2] = a[2]/mag;

    return result;
}
