#include "ga07_ortogonalni_upiti.h"

#include "ga07_rangetree.h"
#include "oblastcrtanja.h"

#include <QMainWindow>
#include <QStatusBar>
#include <QMouseEvent>
#include <QRubberBand>
#include <QLineEdit>
#include <QMessageBox>
#include <QFileInfo>
#include <QTextStream>
#include <QLabel>
#include <QRandomGenerator>

bool OrtogonalniUpiti::mousePressed = false;

OrtogonalniUpiti::OrtogonalniUpiti(QWidget *pCrtanje,
                                   QOpenGLWidget *pCrtanjeGL,
                                   int pauzaKoraka,
                                   std::string imeDatoteke,
                                   int brojTacaka,
                                   RazlogPravljenjaAlg razlog)
    : AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{
    QString imeDatotekeQStr = QString::fromStdString(imeDatoteke);

    // Preuzmi informacije o roditeljskim GUI elementima specificnim samo ortogonalnim upitima

    if (pCrtanje != nullptr) {
        constexpr auto imeGlavnogElementa = "centralWidget";
        QObject *roditelj = pCrtanje->parent();
        while (roditelj->objectName() != imeGlavnogElementa)
            roditelj = roditelj->parent();

        auto polje = [roditelj](const QString ime) {
            return roditelj->findChild<QLineEdit *>("lineEdit" + ime);
        };
        const QStringList sufiksi = {"X1", "Y1", "X2", "Y2"};
        for (int i = 0; i < _brojTacaka; ++i) {
            for (int j = 0; j < KOORD_DIM; ++j) {
                QLineEdit *le = polje(sufiksi[i * 2 + j]);
                m_lineEdits[i][j] = le;
                Q_ASSERT(m_lineEdits[i][j] != nullptr);
            }
        }

        m_statusBar = static_cast<QMainWindow*>(roditelj->parent())->statusBar();
        mousePressed = false;

        if (!m_statusBar->findChild<QLabel*>()) {
            QLabel *koordinate = new QLabel;
            koordinate->setAlignment(Qt::AlignRight);
            m_statusBar->addWidget(koordinate, 1);
        }
    }

    // Generisi podatke neophodne za strukturu stabla opsega

    if (razlog == RAZLOG_TESTIRANJE) {
        // Za potrebe testiranja, uvecati ulaznu dimenziju
        brojTacaka *= 5000;
        razlog = RAZLOG_NASUMICNE;
    }

    if (razlog == RAZLOG_UCITAVANJE) {
        bool ok = ucitajTackeIzDatoteke(imeDatotekeQStr);
        if (ok == false)
            return;
    }
    else if (razlog == RAZLOG_NASUMICNE){
        m_tacke = QVector<QPoint>::fromStdVector(generisiNasumicneTacke(brojTacaka));
        m_prvoTeme = m_tacke.first();
        m_drugoTeme = m_tacke.last();
        // Unesi opseg u GUI elemente
        if (AlgoritamBaza::_pCrtanje != nullptr) {
            m_lineEdits[0][KOORD_X]->setText(QString::number(m_prvoTeme.x()));
            m_lineEdits[0][KOORD_Y]->setText(QString::number(m_prvoTeme.y()));
            m_lineEdits[1][KOORD_X]->setText(QString::number(m_drugoTeme.x()));
            m_lineEdits[1][KOORD_Y]->setText(QString::number(m_drugoTeme.y()));
        }
    }

    if (razlog != RAZLOG_ISPOCETKA) {
        // Napravi novo stablo samo kada se ucita novi skup tacaka (bio on nasumican ili iz fajla)

        m_stablo = new RangeTree(m_tacke);

        if (pCrtanje == nullptr)
            return; // u pitanju je rezim poredjenja efikasnosti

        pCrtanje->installEventFilter(this);
        m_transformacija = dynamic_cast<OblastCrtanja*>(pCrtanje)->get_transformacija();
        napraviQRubberBand();

        AlgoritamBaza_updateCanvasAndBlock();
    }
}

OrtogonalniUpiti::~OrtogonalniUpiti()
{
    m_tacke.clear();
    QVector<QPoint>().swap(m_tacke);
    m_tackeIzOpsega.clear();
    QVector<QPoint>().swap(m_tackeIzOpsega);
    anulirajQRubberBand();

    delete m_stablo;
}

void OrtogonalniUpiti::pokreniAlgoritam()
{
    bool testiranje = AlgoritamBaza::_pCrtanje == nullptr;
    if (!testiranje) // ne narusavati slozenost prilikom testiranja
        m_tackeIzOpsega.clear();

    AlgoritamBaza_updateCanvasAndBlock();

    bool uzmiOpsegIzPolja = false;
    int intervalX[2], intervalY[2];
    if (!testiranje) {
        // Prioritet prilikom odabira opsega daj GUI poljima
        uzmiOpsegIzPolja = true;
        for (int i = 0; (i < 2) && uzmiOpsegIzPolja; ++i) {
            for (int j = 0; (j < KOORD_DIM) && uzmiOpsegIzPolja; ++j) {
                QString br = m_lineEdits[i][j]->text();
                if (!br.isEmpty())
                    br.toInt(&uzmiOpsegIzPolja);
                else
                    uzmiOpsegIzPolja = false;
            }
        }
        azurirajPravougaonik();
    }
    if (uzmiOpsegIzPolja) {
        intervalX[0] = m_lineEdits[0][KOORD_X]->text().toInt();
        intervalY[0] = m_lineEdits[0][KOORD_Y]->text().toInt();
        intervalX[1] = m_lineEdits[1][KOORD_X]->text().toInt();
        intervalY[1] = m_lineEdits[1][KOORD_Y]->text().toInt();
    }
    else {
        intervalX[0] = m_prvoTeme.x();
        intervalY[0] = m_prvoTeme.y();
        intervalX[1] = m_drugoTeme.x();
        intervalY[1] = m_drugoTeme.y();
    }

    m_stablo->sakupiTackeIzOpsega(intervalX, intervalY, m_tackeIzOpsega);

    AlgoritamBaza_updateCanvasAndBlock();
    emit animacijaZavrsila();
}

void OrtogonalniUpiti::pokreniNaivniAlgoritam()
{
    m_tackeIzOpsega.clear();

    AlgoritamBaza_updateCanvasAndBlock();

    int x[2], y[2]; // opseg po kojem se pretrazuje
    x[0] = m_prvoTeme.x();
    y[0] = m_prvoTeme.y();
    x[1] = m_drugoTeme.x();
    y[1] = m_drugoTeme.y();
    std::sort(x, x + 2);
    std::sort(y, y + 2);

    for (int i = 0; i < m_tacke.size(); ++i) {
        QPoint t = m_tacke.at(i);
        bool uIntervaluX = (t.x() >= x[0] && t.x() <= x[1]);

        if (uIntervaluX && (t.y() >= y[0] && t.y() <= y[1])) {
            m_tackeIzOpsega.append(t);
            AlgoritamBaza_updateCanvasAndBlock();
        }
    }
    emit animacijaZavrsila();
}

void OrtogonalniUpiti::crtajAlgoritam(QPainter &painter) const
{
    m_transformacija = painter.matrix();

    QPen p = painter.pen();
    p.setWidth(6);
    p.setColor(Qt::black);

    painter.setPen(p);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawPoints(m_tacke);

    p.setColor(QColor("#287bff"));
    painter.setPen(p);
    painter.drawPoints(m_tackeIzOpsega);
}

const QVector<QPoint> OrtogonalniUpiti::tackeIzOpsega() const
{
    return m_tackeIzOpsega;
}

void OrtogonalniUpiti::azurirajPravougaonik()
{
    if (AlgoritamBaza::_pCrtanje == nullptr)
        return;

    bool ok = true;
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < KOORD_DIM; ++j) {
            m_lineEdits[i][j]->text().toInt(&ok);
            if (!ok || m_lineEdits[i][j]->text().isEmpty()) {
                m_prvoTeme = {};
                m_prvoTemeQt = {};
                m_drugoTeme = {};
                if (m_selektovanje != nullptr) {
                    m_selektovanje->resize(QSize());
                    m_selektovanje->hide();
                }
                return;
            }
        }
    }

    int x = m_lineEdits[0][KOORD_X]->text().toInt();
    int y = m_lineEdits[0][KOORD_Y]->text().toInt();
    m_prvoTeme = QPoint(x, y);

    x = m_lineEdits[1][KOORD_X]->text().toInt();
    y = m_lineEdits[1][KOORD_Y]->text().toInt();
    m_drugoTeme = QPoint(x, y);

    bool invertibilna;
    QMatrix inverzna = m_transformacija.inverted(&invertibilna);
    if (invertibilna == false)
        return;

    m_prvoTemeQt = inverzna.map(m_prvoTeme);
    QPoint drugoTemeQt = inverzna.map(m_drugoTeme);

    napraviQRubberBand();
    m_selektovanje->setGeometry(QRect(m_prvoTemeQt, drugoTemeQt).normalized());
    m_selektovanje->show();
}

void OrtogonalniUpiti::napraviQRubberBand()
{
    if (m_selektovanje != nullptr)
        return; // ne praviti dve instance QRubberBand objekta
    QPalette palette;
    QColor color(Qt::white);
    palette.setBrush(QPalette::Highlight, QBrush(color));
    m_selektovanje = new QRubberBand(QRubberBand::Rectangle, AlgoritamBaza::_pCrtanje);
    m_selektovanje->setPalette(palette);
}

void OrtogonalniUpiti::anulirajQRubberBand()
{
    m_selektovanje = nullptr;
}

bool OrtogonalniUpiti::ucitajTackeIzDatoteke(QString imeDatoteke)
{
    QFileInfo fajlInfo(imeDatoteke);
    bool ok = true;
    if (fajlInfo.exists() && fajlInfo.isFile()) {
        QFile fajl(imeDatoteke);
        fajl.open(QIODevice::ReadOnly);
        QTextStream in(&fajl);

        int brojTacaka = in.readLine().toInt(&ok);
        const QString naziv = "Fajl nije korektan";
        QString poruka =
            "Proverite da li je fajl u dobrom formatu:\n"
            "Pozitivan broj tacaka N (u zasebnom redu), a"
            "zatim N redova celobrojnih parova koordinata.\n"
            "Na kraju je moguce navesti i opseg [x',x''] puta [y', y''] "
            "- u sledecem formatu:\n\"opseg\"\n\"x' y'\"\"x'' y''\"";

        if (!ok || brojTacaka < 1)
            upozorenje(naziv, poruka);

        int x, y;
        while (brojTacaka-- > 0) {
            auto const red = in.readLine();
            auto const brojevi = red.split(' ');
            x = brojevi[0].toInt(&ok);
            if (!ok) {
                upozorenje(naziv, poruka);
                break;
            }
            y = brojevi[1].toInt(&ok);
            if (!ok) {
                upozorenje(naziv, poruka);
                break;
            }
            m_tacke.append(QPoint(x, y));
        }

        // Da li je takodje unet i opseg?
        bool sadrziOpseg = false;
        if (in.readLine() == "opseg") {
            sadrziOpseg = true;
            poruka =
                "Opseg zadat u fajlu nije korektnog formata.\n"
                "Za opseg ce biti odabrane 2 nasumicne tacke iz skupa.";
            for (int i = 0; i < 2; ++i) {
                auto const red = in.readLine();
                auto const brojevi = red.split(' ');
                if (red.size() != 2 && red != "opseg") {
                    if (!ok) {
                        upozorenje(naziv, poruka);
                        break;
                    }
                }
                x = brojevi[0].toInt(&ok);
                if (!ok) {
                    upozorenje(naziv, poruka);
                    break;
                }
                y = brojevi[1].toInt(&ok);
                if (!ok) {
                    upozorenje(naziv, poruka);
                    break;
                }
                if (i > 0)
                    m_drugoTeme = QPoint(x, y);
                else
                    m_prvoTeme = QPoint(x, y);
            }
        }
        if (!sadrziOpseg || !ok) {
            QRandomGenerator rg;
            ok = true;  // prevazidji greske formata opsega u fajlu odabirom nasumicnog
            m_prvoTeme = m_tacke.at(rg.bounded(0, m_tacke.size() - 1));
            m_drugoTeme = m_tacke.at(rg.bounded(0, m_tacke.size() - 1));
        }

        fajl.close();
    }

    if (!ok) {
        const QString naslov = "Navedeni fajl nije korektan";
        const QString poruka = "Ponistite sve i pokusajte ponovo da odaberete fajl.";
        upozorenje(naslov, poruka);
        return false;
    }

    // Unesi opseg u GUI elemente
    if (AlgoritamBaza::_pCrtanje != nullptr) {
        m_lineEdits[0][KOORD_X]->setText(QString::number(m_prvoTeme.x()));
        m_lineEdits[0][KOORD_Y]->setText(QString::number(m_prvoTeme.y()));
        m_lineEdits[1][KOORD_X]->setText(QString::number(m_drugoTeme.x()));
        m_lineEdits[1][KOORD_Y]->setText(QString::number(m_drugoTeme.y()));
    }
    return true;
}

void OrtogonalniUpiti::upozorenje(QString naslov, QString poruka)
{
    QMessageBox::warning(AlgoritamBaza::_pCrtanje, naslov, poruka, QMessageBox::Close);
}

bool OrtogonalniUpiti::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == AlgoritamBaza::_pCrtanje) {
        QMouseEvent *mEvent = nullptr;
        switch (event->type()) {
            case QEvent::MouseButtonPress:
            case QEvent::MouseMove:
            case QEvent::MouseButtonRelease:
                mEvent = dynamic_cast<QMouseEvent *>(event);
                break;
            case QEvent::Leave: {
                m_statusBar->findChild<QLabel*>()->clear();
                break;
            }
            default:
                break;
        }
        if (mEvent == nullptr)
            return false;
        QPoint painterKoord = m_transformacija.map(mEvent->pos());

        if (event->type() == QEvent::MouseButtonPress && !mousePressed) {

            m_prvoTeme = painterKoord;
            m_prvoTemeQt = mEvent->pos();
            m_lineEdits[0][KOORD_X]->setText(QString::number(m_prvoTeme.x()));
            m_lineEdits[0][KOORD_Y]->setText(QString::number(m_prvoTeme.y()));

            m_drugoTeme = m_prvoTeme;
            m_lineEdits[1][KOORD_X]->setText(QString::number(m_drugoTeme.x()));
            m_lineEdits[1][KOORD_Y]->setText(QString::number(m_drugoTeme.y()));

            m_selektovanje->setGeometry(QRect(m_prvoTemeQt, QSize()).normalized());
            m_selektovanje->show();

            mousePressed = true;
        }
        else if (event->type() == QEvent::MouseMove && mousePressed) {
            m_drugoTeme = painterKoord;

            m_lineEdits[1][KOORD_X]->setText(QString::number(m_drugoTeme.x()));
            m_lineEdits[1][KOORD_Y]->setText(QString::number(m_drugoTeme.y()));

            m_selektovanje->setGeometry(QRect(m_prvoTemeQt, mEvent->pos()).normalized());
        }
        else if (event->type() == QEvent::MouseButtonRelease) {
            mousePressed = false;
        }

        if (event->type() == QEvent::MouseMove) {
            if (m_statusBar) {
                auto poruka = QString("(%1, %2)").arg(painterKoord.x()).arg(painterKoord.y());
                m_statusBar->findChild<QLabel*>()->setText(poruka);
            }
        }

        return true;
    }
    return QObject::eventFilter(obj, event);
}
