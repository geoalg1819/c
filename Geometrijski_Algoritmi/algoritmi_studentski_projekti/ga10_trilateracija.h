#ifndef GA10_TRILATERACIJA_H
#define GA10_TRILATERACIJA_H

#include "algoritambaza.h"
#include <cmath>

using namespace std;
struct LatLong {
    double lat;
    double lon;
    double r;

    void set (double lat, double lon, double r) {
        this->lat = lat;
        this->lon = lon;
        this->r = r;
    }
};

struct Coordinate {
    double x;
    double y;
    double z;
    double r;

    void set(double x, double y, double z, double r) {
        this->x = x;
        this->y = y;
        this->z = z;
        this->r = r;
    }
};

class Trilateracija : public AlgoritamBaza
{
public:
    Trilateracija(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int brojTacaka = 0);

    void pokreniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    virtual void crtajAlgoritam3D() const;
    bool is_3D() const;
    void pokreniNaivniAlgoritam();

    vector<double> pokreniAlgoritamTest();
    double calcDistance(double lat1, double lon1, double lat2, double lon2);

private:
    Coordinate _a, _b, _c, _rez;
    LatLong _as, _bs, _cs, _rezs;

    void set_normal_and_vertex(double u, double v, double r) const;
    vector<double> trilateration(LatLong a, LatLong b, LatLong c);

    void randomPoints();
    void fromFile(string imeDatoteke);
    double deg2Rads(double x);
    double rad2Degs(double x);
    vector<double> vectorMul(vector<double> a,vector<double> b);
    vector<double> vectorMul(double a,vector<double> b);
    vector<double> vectorDiv(vector<double> a,vector<double> b);
    vector<double> vectorDiv(vector<double> a,double b);
    vector<double> vectorDiff(vector<double> a,vector<double> b);
    vector<double> vectorAdd(vector<double> a,vector<double> b);
    double vectorDot(vector<double> a,vector<double> b);
    vector<double> vectorCross(vector<double> a,vector<double> b);
    double vectorNorm(vector<double> a);
    vector<double> vectorNormalize(vector<double> a);
};

#endif // GA10_TRILATERACIJA_H
