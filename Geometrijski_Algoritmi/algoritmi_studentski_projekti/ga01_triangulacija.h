#ifndef GA01_TRIANGULACIJA_H
#define GA01_TRIANGULACIJA_H

#include "algoritambaza.h"
#include <vector>
#include <set>
#include "algoritmi_sa_vezbi/ga05_dcel.h"

// Klasa za implementaciju algoritma triangulacije, Jasmina Vasilijevic 1067/2017.
//
class Triangulacija2 : public AlgoritamBaza
{
public:

    static constexpr int MINDIM = 10;
    static constexpr int MAXDIM = 1000;
    static constexpr int ALGSTEP = 10;
    static QString tipAlgoritma;

    Triangulacija2(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke,int nasumicnih, bool koristiNaivni);

    // Ucitavanje podataka
    static void kreirajNasumicneTacke(int nasumicnih, std::vector<QPoint> &outTacke);

    // Osnovni metodi.
    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();

    // Naivni algoritam
    void podesiOsnovniDcell();
    HalfEdge *uzmiUnutrasnjuIvicu(HalfEdge* prethodna);
    bool losTrougao(Vertex* v, Vertex *u, Vertex *w);
    bool imaTemenaUUnutrasnjosti(Vertex *v, Vertex *u, Vertex *w);
    bool pozitivnoOrijentisan(Vertex *v, Vertex *u, Vertex* w);
    HalfEdge *odseciTrougao(HalfEdge *e, Vertex *v, Vertex *u, Vertex *w);

    // Regularan algoritam

    // Metodi za crtanje.
    void crtajAlgoritam(QPainter& painter) const;
    void crtajDcell(QPainter &painter) const;
    void ispisiGresku(QPainter &painter) const;

    // Neimplementirani metodi.
    virtual void crtajAlgoritam3D() const
    {
        throw "Nepodrzan tip iscrtavanja.";
    }

    bool is_3D() const
    {
        return false;
    }

private:

    // Ulazni podaci
    bool _koristiNaivni;

    // Strukture za cuvanje i obradu podataka
    DCEL _polygon;

    // Pomocni clanovi za iscrtavanje
    bool _iscrtajDcellInstant;
    size_t _maxTemeZaIscrtavanje;
    HalfEdge *_poslednjaDodataIvica;
    QString _greska;
    Field *_poljeZaObradu;
    Vertex * _trougaoU = nullptr;
    Vertex *_trougaoV = nullptr;
    Vertex *_trougaoW = nullptr;
    std::vector<HalfEdge*> _iviceInicijalnogPoligona;
};

// Staticka pomocna klasa crtac, za laksu organizaciju iscrtavanja objekata
// i manipulaciju sa QPainter klasom.
//
class Crtac
{
public:
    static void Crtaj(const QString &text, QPainter &painter);
    static void Crtaj(const Vertex *teme, QPainter &painter);
    static void Crtaj(const HalfEdge *ivica, QPainter &painter, Qt::GlobalColor boja, int debljina, Qt::PenStyle stil = Qt::SolidLine);

private:
    // Ne treba nam konstruktor, posto zelimo da se ova klasa ponasa kao staticka
    // pa ga deklarisemo kao privatni i brisemo ga.
    //
    Crtac() = delete;
};

#endif // GA01_TRIANGULACIJA_H
