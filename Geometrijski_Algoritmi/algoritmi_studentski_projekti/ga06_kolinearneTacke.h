#ifndef GA06_KOLINEARNETACKE_H
#define GA06_KOLINEARNETACKE_H

#include "algoritambaza.h"
#include "ga02_unija_pravougaonika.h"

struct Tacka
{
public:
    QPoint tacka;
    double ugao;
};

class KolinearneTacke : public AlgoritamBaza
{
public:
    KolinearneTacke(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;
    void pokreniNaivniAlgoritam();
    double izracunajUgao(QPoint o, QPoint b);
    bool poredi(const Tacka &a, const Tacka &b);
    void reset();
    void generisiNasumicneTacke(int broj_tacaka);
    bool poredi3(const Tacka &a, const Tacka &b);

private:
    std::vector<Tacka> _tacke;
    QPoint _trenutnaTacka;
    std::vector<QPoint> pocetneTacke;
    std::vector<std::pair<QPoint, QPoint>> kolinearne;
    unsigned _index;
};


#endif // GA06_KOLINEARNETACKE_H
