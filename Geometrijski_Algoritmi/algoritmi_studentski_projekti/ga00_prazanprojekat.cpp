#include "ga00_prazanprojekat.h"

PrazanProjekat::PrazanProjekat(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{

}

void PrazanProjekat::pokreniAlgoritam()
{

}

void PrazanProjekat::crtajAlgoritam(QPainter &painter) const
{

}

void PrazanProjekat::crtajAlgoritam3D() const
{

}

bool PrazanProjekat::is_3D() const
{
    return false;
}
