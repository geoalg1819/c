#include "ga04_preseciduzi.h"
#include <fstream>
#include <iostream>

PreseciDuzi::PreseciDuzi(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_duzi)
        :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka),
         _eventQueue(), _preseci(),
         _statusQueue(StatusQueueComp(&_brisucaPravaY))
{
    _brisucaPravaY = _pCrtanje->height();

    if (imeDatoteke == "")
        _duzi = generisiNasumicneDuzi(broj_duzi);
    else
        _duzi = ucitajDuziIzDatoteke(imeDatoteke);

    for(int i=0; i<_duzi.size(); i++)
    {
        _eventQueue.insert({_duzi[i].p1(), &_duzi[i], nullptr, UPPER_POINT});
        _eventQueue.insert({_duzi[i].p2(), &_duzi[i], nullptr, LOWER_POINT});
    }

    /* Provera unosa podataka

    for(int i=0; i<_duzi.size(); i++)
    {
        std::cout << _duzi[i].p1().x() << " " << _duzi[i].p1().y() << "\t";
        std::cout << _duzi[i].p2().x() << " " <<_duzi[i].p2().y() << std::endl;
    }

    std::cout << std::endl << std::endl;

    std::set<EventQueueNode, EventQueueComp>::iterator it;
    for (it = _eventQueue.begin(); it != _eventQueue.end(); ++it)
    {
         std::cout << it->event.x() << " " << it->event.y() << std::endl;
    }
    */

}

void PreseciDuzi::pokreniAlgoritam()
{
    //Dok god ima neobradjenih dogadjaja
    while(!_eventQueue.empty())
    {
        //Preuzimamo sledeci dogadjaj i brisemo ga iz reda
        EventQueueNode eventNode = *_eventQueue.begin();
        _eventQueue.erase(_eventQueue.begin());

        QPointF presek;

        //Ako je u pitanju gornje teme duzi
        if (eventNode.type == UPPER_POINT)
        {
            //Pomeramo brisucu pravu
            _brisucaPravaY = eventNode.event.y();
            AlgoritamBaza_updateCanvasAndBlock();

            //Ubacujemo duz u status (i pamtimo je zbog sledecih koraka)
            auto trenutna_duz = _statusQueue.insert(eventNode.line1).first;

            //Proveravamo presek sa prethodnom (ako je ima)
            if (_statusQueue.begin() != trenutna_duz)
            {
                auto prethodna = std::prev(trenutna_duz);
                if (pomocnefunkcije::presekDuzi(**trenutna_duz, **prethodna, presek))
                {
                    if (eventNode.event != presek && presek.y() <= _brisucaPravaY)
                        _eventQueue.insert({presek, *trenutna_duz, *prethodna, INTERSECTION});
                }
            }

            //Proveravamo presek sa sledecom u statusu (ako je ima)
            auto sledeca = std::next(trenutna_duz);
            if (_statusQueue.end() != sledeca)
            {
                if (pomocnefunkcije::presekDuzi(**trenutna_duz, **sledeca, presek))
                {
                    if (eventNode.event != presek && presek.y() <= _brisucaPravaY)
                        _eventQueue.insert({presek, *trenutna_duz, *sledeca, INTERSECTION});
                }
            }
        }
        //Ako je u pitanju donje teme
        else if (eventNode.type == LOWER_POINT)
        {
            //Pomeramo brisucu pravu
            _brisucaPravaY = eventNode.event.y();
            AlgoritamBaza_updateCanvasAndBlock();

            //Pronalazimo duz koju treba obrisati iz statusa
            auto trenutna_duz = _statusQueue.find(eventNode.line1);

            //Proveravamo presek njene prethodne i sledece u statusu (jer one postaju novi susedi)
            if (trenutna_duz != _statusQueue.begin())
            {
                auto prethodna = std::prev(trenutna_duz);
                auto sledeca = std::next(trenutna_duz);

                if (sledeca != _statusQueue.end())
                {
                    if (pomocnefunkcije::presekDuzi(**prethodna, **sledeca, presek))
                    {
                        if (eventNode.event != presek && presek.y() <= _brisucaPravaY)
                            _eventQueue.insert({presek, *prethodna, *sledeca, INTERSECTION});


                    }
                }
            }

            //Brisemo duz iz statusa
            brisiIzStatusa(eventNode.line1);
        }
        //Ako je u pitanju presecna tacka
        else
        {
            //Dodajemo tacku u niz presecnih tacaka
            _preseci.push_back(eventNode.event);
            AlgoritamBaza_updateCanvasAndBlock();

            //Razmenjujemo duzi koje se u toj tacki seku
            //Tako sto ih brisemo, pa pomeramo brisucu pravu pa ih opet ubacimo u status
            _statusQueue.erase(eventNode.line1);
            _statusQueue.erase(eventNode.line2);

            //Pomeramo brisucu pravu
            _brisucaPravaY = eventNode.event.y();
            AlgoritamBaza_updateCanvasAndBlock();

            //Vracamo duzi u status
            auto duz1 = _statusQueue.insert(eventNode.line1).first;
            auto duz2 = _statusQueue.insert(eventNode.line2).first;

            //Proveravamo preseke sa novim susedima
            if (duz1 != _statusQueue.begin())
            {
                auto prethodna = std::prev(duz1);
                if (pomocnefunkcije::presekDuzi(**duz1, **prethodna, presek))
                {
                    if (presek != eventNode.event && presek.y() < _brisucaPravaY)
                        _eventQueue.insert({presek, *duz1, *prethodna, INTERSECTION});
                }
            }

            auto sledeca = std::next(duz1);
            if (sledeca != _statusQueue.end())
            {
                if (pomocnefunkcije::presekDuzi(**duz1, **sledeca, presek))
                {
                    if (presek != eventNode.event && presek.y() < _brisucaPravaY)
                        _eventQueue.insert({presek, *duz1, *sledeca, INTERSECTION});
                }
            }


            if (duz2 != _statusQueue.begin())
            {
                auto prethodna = std::prev(duz2);
                if (pomocnefunkcije::presekDuzi(**duz2, **prethodna, presek))
                {
                    if (presek != eventNode.event && presek.y() < _brisucaPravaY)
                        _eventQueue.insert({presek, *duz2, *prethodna, INTERSECTION});
                }
            }

            sledeca = std::next(duz2);
            if (sledeca != _statusQueue.end())
            {
                if (pomocnefunkcije::presekDuzi(**duz2, **sledeca, presek))
                {
                    if (presek != eventNode.event && presek.y() < _brisucaPravaY)
                        _eventQueue.insert({presek, *duz2, *sledeca, INTERSECTION});
                }
            }

        }
    }

    _statusQueue.clear();
    AlgoritamBaza_updateCanvasAndBlock();

    emit animacijaZavrsila();
}

void PreseciDuzi::pokreniNaivniAlgoritam()
{

}

void PreseciDuzi::crtajAlgoritam(QPainter &painter) const
{
    QPen magneta = painter.pen();
    magneta.setColor(Qt::magenta);
    magneta.setWidth(3);

    QPen blue = painter.pen();
    blue.setColor(Qt::blue);
    magneta.setWidth(3);

    QPen yellow = painter.pen();
    yellow.setColor(Qt::yellow);
    yellow.setWidth(10);

    //Iscrtavamo sve duzi
    for(const QLineF& d : _duzi)
        painter.drawLine(d);

    //Iscrtavamo trenutnu poziciju brisuce prave
    painter.setPen(blue);
    painter.drawLine(0, _brisucaPravaY, _pCrtanje->width(), _brisucaPravaY);

    //Iscrtavamo trenutno stanje statusa
    painter.setPen(magneta);
    int i = 1; //brojac za ispis redosleda duzi u statusu
    for(const QLineF* l : _statusQueue)
    {
        painter.drawLine(*l);
        //painter.drawText(l->center(), QString::number(i++));
    }

    //Isrtavamo sve presecne tacke koje smo nasli
    painter.setPen(yellow);
    for(const QPointF& t : _preseci)
        painter.drawPoint(t);
}

void PreseciDuzi::crtajAlgoritam3D() const
{

}

bool PreseciDuzi::is_3D() const
{
    return false;
}

void PreseciDuzi::brisiIzStatusa(QLineF *l)
{
    for(auto it = _statusQueue.begin(); it != _statusQueue.end(); ++it)
    {
        if(*it == l)
        {
            _statusQueue.erase(it);
            break;
        }
    }
}

std::vector<QLineF> PreseciDuzi::ucitajDuziIzDatoteke(std::string imeDatoteke)
{
    std::ifstream inputFile(imeDatoteke);
    std::vector<QLineF> duzi;

    double x, y, x1, y1;
    while(inputFile >> x >> y >> x1 >> y1)
    {
        /* Redosled je bitan zbog UPPER POINT i LOWER POINT */
        if (y < y1 || (y == y1 && x > x1))
        {
         double pom_x = x;
         double pom_y = y;
         x = x1;
         y = y1;
         x1 = pom_x;
         y1 = pom_y;
        }
        duzi.push_back(QLineF(x, y, x1, y1));
    }

    return duzi;
}

std::vector<QLineF> PreseciDuzi::generisiNasumicneDuzi(int broj_duzi)
{
    srand(static_cast<unsigned>(time(0)));

    int xMax = _pCrtanje->width()- DRAWING_BORDER;
    int yMax = _pCrtanje->height() - DRAWING_BORDER;

    int xMin = DRAWING_BORDER;
    int yMin = DRAWING_BORDER;

    std::vector<QLineF> randomDuzi;

    int xDiff = xMax-xMin;
    int yDiff = yMax-yMin;
    for(int i=0; i < broj_duzi; i++)
    {
        double x = xMin + rand()%xDiff;
        double y = yMin + rand()%yDiff;
        double x1 = yMin + rand()%xDiff;
        double y1 = xMin + rand()%yDiff;

        if (y < y1 || (y == y1 && x > x1))
        {
         double pom_x = x;
         double pom_y = y;
         x = x1;
         y = y1;
         x1 = pom_x;
         y1 = pom_y;
        }

        randomDuzi.push_back(QLineF(x, y, x1, y1));
    }

    return randomDuzi;
}



