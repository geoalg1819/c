#include "ga02_konveksniomotac.h"

#include "pomocnefunkcije.h"
#include <QPainter>

#include <iostream>

KonveksniOmotac::KonveksniOmotac(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_tacaka)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{
    if (imeDatoteke != "")
        _tacke = ucitajPodatkeIzDatoteke(imeDatoteke);
    else
        _tacke = generisiNasumicneTacke(broj_tacaka);
}

void KonveksniOmotac::pokreniAlgoritam()
{
    _konveksniOmotac.clear();
    // Prvo se trazi tacka skroz desno (ona je sigurno u omotacu).
    _prvaTacka = _tacke[0];
    for(QPoint p: _tacke)
        if ((p.x() > _prvaTacka.x()) ||
            (p.x() == _prvaTacka.x() && p.y() < _prvaTacka.y()))
                _prvaTacka = p;


    // ovaj red se moze izbaciti, sluzi samo za lepsu vizuelizaciju
    _tackaKojaSeTrenutnoIspituje = _prvaTacka;

    AlgoritamBaza_updateCanvasAndBlock();


    qSort(_tacke.begin(), _tacke.end(), [&](const QPoint& tacka1, const QPoint& tacka2){return poredi(tacka1, tacka2);});

    _konveksniOmotac.push_back(_prvaTacka);
    AlgoritamBaza_updateCanvasAndBlock();

    _konveksniOmotac.push_back(_tacke[1]);
    AlgoritamBaza_updateCanvasAndBlock();

    int k = 2;
    int m = 1; // trenutni indeks tacke u konveksnom omotacu
    while( k < _tacke.size() )
    {
        //ovaj red je samo zbog lepse vizuelizacije, moze se izbaciti
        _tackaKojaSeTrenutnoIspituje = _tacke[k];

        if (pomocnefunkcije::povrsinaTrougla2(_konveksniOmotac[m-1], _konveksniOmotac[m], _tacke[k]) < 0)
        {
            _konveksniOmotac.push_back(_tacke[k]);
            AlgoritamBaza_updateCanvasAndBlock();

            k++;
            m++;
        }
        else
        {
            _konveksniOmotac.pop_back();
            AlgoritamBaza_updateCanvasAndBlock();

            m--;
        }
    }

    //naredna tri reda su samo zbog lepse vizuelizacije, mogu se izbaciti
    _tackaKojaSeTrenutnoIspituje = _prvaTacka;
    _konveksniOmotac.push_back(_prvaTacka);
    AlgoritamBaza_updateCanvasAndBlock();


    emit animacijaZavrsila();

}

void KonveksniOmotac::pokreniNaivniAlgoritam()
{
    _konveksniOmotacNaivni.clear();

    int i, j, k;
    int pointsNum = _tacke.size();
    bool valid;

    for(i=0; i<pointsNum; i++)
    {
        for(j=0; j<pointsNum; j++)
        {
            if(i!=j)
            {
                valid = true;
                for(k=0; k<pointsNum; k++)
                {
                    if(k == i || k == j)
                        continue;
                    if(pomocnefunkcije::povrsinaTrougla2(_tacke[i], _tacke[j], _tacke[k]) <= 0)
                    {
                        valid = false;
                        break;
                    }
                }
                if(valid)
                {
                    _konveksniOmotacNaivni.push_back(_tacke[i]);
                    _konveksniOmotacNaivni.push_back(_tacke[j]);
                }
            }
        }
    }
    //Sortiranje po uglu
    std::sort(_konveksniOmotacNaivni.begin(), _konveksniOmotacNaivni.end(), [&](const QPoint& lhs, const QPoint& rhs){return poredi(lhs, rhs);});

    std::cout<< "stigli dovde\n";

    //Ukloni duplikate
    auto it = std::unique(_konveksniOmotacNaivni.begin(), _konveksniOmotacNaivni.end());
    _konveksniOmotacNaivni.resize(std::distance(_konveksniOmotacNaivni.begin(), it));

    std::cout<< "stigli dovde\n";
}

void KonveksniOmotac::crtajAlgoritam(QPainter &painter) const
{
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen p = painter.pen();
    p.setColor(Qt::black);
    p.setWidth(5);
    p.setCapStyle(Qt::RoundCap);

    painter.setPen(p);

    for(QPoint tacka : _tacke)
        painter.drawPoint(tacka);

    p.setColor(Qt::blue);
    painter.setPen(p);
    painter.drawPoint(_prvaTacka);


    // iscrtava se trenutni omotac
    p.setColor(Qt::magenta);
    painter.setPen(p);
    for(int i=1; i<_konveksniOmotac.size(); i++)
       painter.drawLine(_konveksniOmotac[i-1], _konveksniOmotac[i]);
    if (_konveksniOmotac.size() > 0)
        painter.drawLine(_konveksniOmotac[_konveksniOmotac.size()-1], _tackaKojaSeTrenutnoIspituje);
}

void KonveksniOmotac::crtajAlgoritam3D() const
{

}

bool KonveksniOmotac::is_3D() const
{
    return false;
}

bool KonveksniOmotac::poredi(const QPoint &a, const QPoint &b)
{
    double param = pomocnefunkcije::povrsinaTrougla2(_prvaTacka, a, b);

    if (param > 0)
       return false;

    if (param < 0)
        return true;

    /* U suprotnom su tacke kolinearne. Ako su kolinearne, onda ona koja nije ekstremna nije u konveksnom omotacu. */
    int x = abs(a.x() - _prvaTacka.x()) - abs(b.x() - _prvaTacka.x());
    int y = abs(a.y() - _prvaTacka.y()) - abs(b.y() - _prvaTacka.y());

    return (x < 0 || y < 0);

}

std::vector<QPoint> KonveksniOmotac::konveksniOmotac()
{
    return _konveksniOmotac;
}

std::vector<QPoint> KonveksniOmotac::konveksniOmotacNaivni()
{
    return _konveksniOmotacNaivni;
}
