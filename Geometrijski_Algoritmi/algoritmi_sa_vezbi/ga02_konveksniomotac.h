#ifndef GA02_KONVEKSNIOMOTAC_H
#define GA02_KONVEKSNIOMOTAC_H

#include "../algoritambaza.h"

#include <QPoint>
#include <set>
#include <functional>

class KonveksniOmotac : public AlgoritamBaza
{
public:
    KonveksniOmotac(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;

    bool poredi(const QPoint& a, const QPoint& b);

    std::vector<QPoint> konveksniOmotac();
    std::vector<QPoint> konveksniOmotacNaivni();

private:
    std::vector<QPoint> _tacke;

    std::vector<QPoint> _konveksniOmotac;

    QPoint _prvaTacka;

    /* radi lepse vizuelizacije, dodat ovaj parametar */
    QPoint _tackaKojaSeTrenutnoIspituje;

    //konveksni omotac za Naivni algoritam
    std::vector<QPoint> _konveksniOmotacNaivni;
};


#endif // GA02_KONVEKSNIOMOTAC_H
