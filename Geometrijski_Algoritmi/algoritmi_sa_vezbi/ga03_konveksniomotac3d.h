#ifndef GA03_KONVEKSNIOMOTAC3D_H
#define GA03_KONVEKSNIOMOTAC3D_H

#include "algoritambaza.h"
#include <QVector3D>
#include <vector>
#include <QList>
#include <iostream>
#include "ga03_datastructures.h"
#include "pomocnefunkcije.h"

class KonveksniOmotac3D : public AlgoritamBaza
{
public:
    KonveksniOmotac3D(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;

    /* Funkcije potrebne za algoritam */
    bool Tetraedar();
    void DodajTeme(Teme* t);
    void ObrisiVisak();

    double zapremina6(Stranica* s, Teme* t);
    bool kolinearne(Teme*a, Teme*b, Teme*c);
    Stranica* napraviDruguStranicu(Ivica*i, Teme*t);
    Stranica* napraviPrvuStranicu(Ivica*iv, Teme*t);


    /* Ucitavanje podataka. */
    std::vector<Teme*> generisiNasumicneTacke(int broj_tacaka);
    std::vector<Teme*> ucitajPodatkeIzDatoteke(std::string imeDatoteke);

private:
    std::vector<Teme*> _tacke;
    QList<Ivica*> _ivice;
    QList<Stranica*> _stranice;

    int _znakZapremine;
};

#endif // GA03_KONVEKSNIOMOTAC3D_H
