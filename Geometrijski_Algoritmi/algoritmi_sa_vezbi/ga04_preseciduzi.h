#ifndef GA04_PRESECIDUZI_H
#define GA04_PRESECIDUZI_H

#include <set>

#include "algoritambaza.h"
#include "pomocnefunkcije.h"
#include "ga04_datastructures.h"


class PreseciDuzi : public AlgoritamBaza
{
public:
    PreseciDuzi(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_duzi = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;

private:
    std::vector<QLineF> _duzi;
    std::set<EventQueueNode, EventQueueComp> _eventQueue;
    std::set<QLineF*, StatusQueueComp> _statusQueue;

    double _brisucaPravaY;
    std::vector<QPointF> _preseci;

    void brisiIzStatusa(QLineF* l);

    std::vector<QLineF> ucitajDuziIzDatoteke(std::string imeDatoteke);
    std::vector<QLineF> generisiNasumicneDuzi(int broj_duzi);
};

#endif // GA04_PRESECIDUZI_H

