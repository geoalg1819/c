#ifndef GA00_DEMOISCRTAVANJA_H
#define GA00_DEMOISCRTAVANJA_H

#include "algoritambaza.h"

class DemoIscrtavanja : public AlgoritamBaza
{
public:
    DemoIscrtavanja(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void crtajAlgoritam(QPainter &painter) const;
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam3D() const;
    bool is_3D() const;

private:
    std::vector<QPoint> _tacke;
    int _n;
};

#endif // GA00_DEMOISCRTAVANJA_H
