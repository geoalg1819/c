#include "ga03_konveksniomotac3d.h"
#include <fstream>

KonveksniOmotac3D::KonveksniOmotac3D(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_tacaka)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka)
{
    _znakZapremine = 1;

    if (imeDatoteke != "")
        _tacke = ucitajPodatkeIzDatoteke(imeDatoteke);
    else
        _tacke = generisiNasumicneTacke(broj_tacaka);
}

void KonveksniOmotac3D::pokreniAlgoritam()
{
    AlgoritamBaza_updateCanvasAndBlock();

    if (Tetraedar() == false)
    {
         std::cout << "Sve tacke komplanarne" << std::endl;
         emit animacijaZavrsila();
         return;
    }
    AlgoritamBaza_updateCanvasAndBlock();

    for(int i=0; i<_tacke.size(); i++)
    {
        if (!_tacke[i]->getObradjeno())
        {
            DodajTeme(_tacke[i]);

            /*U prethodnom koraku neke stranice i ivice su oznacene kao visak i moraju se obrisati.*/
            ObrisiVisak();
            _tacke[i]->setObradjeno(true);

            AlgoritamBaza_updateCanvasAndBlock();
         }
    }

    emit animacijaZavrsila();
}

void KonveksniOmotac3D::pokreniNaivniAlgoritam()
{
}

void KonveksniOmotac3D::crtajAlgoritam(QPainter &painter) const
{

}

void KonveksniOmotac3D::crtajAlgoritam3D() const
{
    glColor3d(1, 0, 0.4);

    glBegin(GL_POINTS);
        for(int i=0; i< _tacke.size(); i++)
        {
             glVertex3d(_tacke[i]->x(), _tacke[i]->y(), _tacke[i]->z());
        }
    glEnd();

    for(int i=0; i<_stranice.size(); i++)
    {
        float red = (float)rand()/RAND_MAX;
        float green = (float)rand()/RAND_MAX;
        float blue = (float)rand()/RAND_MAX;

        glColor3d(red, green, blue);

        glBegin(GL_POLYGON);
            glVertex3d(_stranice[i]->teme1()->x(), _stranice[i]->teme1()->y(), _stranice[i]->teme1()->z());
            glVertex3d(_stranice[i]->teme2()->x(), _stranice[i]->teme2()->y(), _stranice[i]->teme2()->z());
            glVertex3d(_stranice[i]->teme3()->x(), _stranice[i]->teme3()->y(), _stranice[i]->teme3()->z());
        glEnd();
    }
}

bool KonveksniOmotac3D::is_3D() const
{
    return true;
}

bool KonveksniOmotac3D::Tetraedar()
{
    int i;
    double z;

    for(i=0; i<_tacke.size()-2; i++)
    {
        if (!kolinearne(_tacke[i], _tacke[i+1], _tacke[i+2]))
            break;
    }

    if (i == _tacke.size()-2)
        return false;

    _tacke[i]->setObradjeno(true);
    _tacke[i+1]->setObradjeno(true);
    _tacke[i+2]->setObradjeno(true);


    Ivica* i1 = new Ivica(_tacke[i], _tacke[i+1]);
    Ivica* i2 = new Ivica(_tacke[i+1], _tacke[i+2]);
    Ivica* i3 = new Ivica(_tacke[i+2], _tacke[i]);

    _ivice.push_back(i1);
    _ivice.push_back(i2);
    _ivice.push_back(i3);

    Stranica* s = new Stranica(_tacke[i], _tacke[i+1], _tacke[i+2],
                               i1, i2, i3);

    _stranice.push_back(s);

    i1->postavi_stranicu(s);
    i2->postavi_stranicu(s);
    i3->postavi_stranicu(s);

/* sve tacke pre tacke i+3 su kolinearne, samim tim su i komplanarne, pa se ne moraju ispitivati ponovo. */
    for(i=i+3; i<_tacke.size(); i++)
    {
        if ((z = zapremina6(s, _tacke[i])) != 0)
            break;
    }

    if (i == _tacke.size())
        return false;

    _tacke[i]->setObradjeno(true);

    if (z < 0)
    {
       s->izmeniRedosledTemena();
       i1->izmeniRedosledTemena();
       i2->izmeniRedosledTemena();
       i3->izmeniRedosledTemena();
    }

    i1->postavi_stranicu(napraviDruguStranicu(i1, _tacke[i]));
    i2->postavi_stranicu(napraviDruguStranicu(i2, _tacke[i]));
    i3->postavi_stranicu(napraviDruguStranicu(i3, _tacke[i]));

    return true;
}

void KonveksniOmotac3D::DodajTeme(Teme *t)
{
    bool postoji_vidljiva = false;

    for(int i=0; i<_stranice.size(); i++)
    {
        double vol = zapremina6(_stranice[i], t);

        if (vol < 0)
        {
            _stranice[i]->setVidljiva(true);
            postoji_vidljiva = true;
        }
    }

    /* Ako nema vidljivu stranicu onda je unutar omotaca.
     * U tom slucaju ni ivice, ni stranice se ne menjaju.
    */
    if (!postoji_vidljiva)
        return;

    /* Inace, menjaju se Ivice, neke se brisu, a one na ivici regiona
     * menjaju susednu stranicu.
     */
    int n = _ivice.size();
    for(int i=0; i<n; i++)
    {
        if (_ivice[i]->stranica1()->getVidljiva() && _ivice[i]->stranica2()->getVidljiva())
            _ivice[i]->setObrisati(true);

        else if (_ivice[i]->stranica1()->getVidljiva())
            _ivice[i]->zameniVidljivuStranicu(napraviPrvuStranicu(_ivice[i], t), 0);
        else
            _ivice[i]->zameniVidljivuStranicu(napraviDruguStranicu(_ivice[i], t), 1);

    }
}

void KonveksniOmotac3D::ObrisiVisak()
{
    int i=0;

    while(i < _stranice.size())
    {
        if (_stranice[i]->getVidljiva())
        {
            _stranice.removeAt(i);
        }
        else
            i++;
    }


    i = 0;

    while (i < _ivice.size()) {
        if (_ivice[i]->getObrisati())
        {
            _ivice.removeAt(i);
        }
        else
            i++;
    }
}

double KonveksniOmotac3D::zapremina6(Stranica *s, Teme *t)
{
    return pomocnefunkcije::zapremina(s->teme1()->koordinate(),
                s->teme2()->koordinate(), s->teme3()->koordinate(),
                t->koordinate());
}

bool KonveksniOmotac3D::kolinearne(Teme *a, Teme *b, Teme *c)
{
    return pomocnefunkcije::kolinearne3D(a->koordinate(), b->koordinate(), c->koordinate());
}

Stranica *KonveksniOmotac3D::napraviDruguStranicu(Ivica *iv, Teme *t)
{
    /* Mozda ta ivica vec postoji, pa je ne treba praviti kao novu! */

    bool postoji1 = false, postoji2 = false;
    Ivica* i1;
    Ivica* i2;

    for(int i= 0; i<_ivice.size(); i++)
    {
        if ((_ivice[i]->teme1() == iv->teme1() && _ivice[i]->teme2() == t) ||
            (_ivice[i]->teme1() == t && _ivice[i]->teme2() == iv->teme1()))
        {
            postoji1 = true;
            i1 = _ivice[i];
        }

        if ((_ivice[i]->teme1() == iv->teme2() && _ivice[i]->teme2() == t) ||
            (_ivice[i]->teme1() == t && _ivice[i]->teme2() == iv->teme2()))
        {
            postoji2 = true;
            i2 = _ivice[i];
        }
    }

    if (!postoji1)
    {
        i1 = new Ivica(iv->teme1(), t);
        _ivice.push_back(i1);
    }

    if (!postoji2)
    {
        i2 = new Ivica(t, iv->teme2());
       _ivice.push_back(i2);
    }

    Stranica* s = new Stranica(iv->teme2(), iv->teme1(), t,
                               iv, i1, i2);

    _stranice.push_back(s);

    i1->postavi_stranicu(s);
    i2->postavi_stranicu(s);

    return s;
}

Stranica *KonveksniOmotac3D::napraviPrvuStranicu(Ivica *iv, Teme *t)
{
    /* Mozda ta ivica vec postoji, pa je ne treba praviti kao novu! */

    bool postoji1 = false, postoji2 = false;
    Ivica* i1;
    Ivica* i2;

    for(int i= 0; i<_ivice.size(); i++)
    {
        if ((_ivice[i]->teme1() == iv->teme1() && _ivice[i]->teme2() == t) ||
            (_ivice[i]->teme1() == t && _ivice[i]->teme2() == iv->teme1()))
        {
            postoji1 = true;
            i1 = _ivice[i];
        }

        if ((_ivice[i]->teme1() == iv->teme2() && _ivice[i]->teme2() == t) ||
            (_ivice[i]->teme1() == t && _ivice[i]->teme2() == iv->teme2()))
        {
            postoji2 = true;
            i2 = _ivice[i];
        }
    }

    if (!postoji1)
    {
        i1 = new Ivica(t, iv->teme1());
        _ivice.push_back(i1);
    }

    if (!postoji2)
    {
        i2 = new Ivica(iv->teme2(), t);
       _ivice.push_back(i2);
    }

    Stranica* s = new Stranica(iv->teme1(), iv->teme2(), t,
                               iv, i1, i2);

    _stranice.push_back(s);

    i1->postavi_stranicu(s);
    i2->postavi_stranicu(s);

    return s;
}

std::vector<Teme *> KonveksniOmotac3D::generisiNasumicneTacke(int broj_tacaka)
{
    srand(static_cast<unsigned>(time(0)));

    std::vector<Teme*> randomPoints;

    for(int i=0; i < broj_tacaka; i++)
        randomPoints.push_back(new Teme(QVector3D(
           (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX)));

    return randomPoints;
}

std::vector<Teme *> KonveksniOmotac3D::ucitajPodatkeIzDatoteke(std::string imeDatoteke)
{
    std::ifstream inputFile(imeDatoteke);
    std::vector<Teme*> points;
    float x, y, z;
    while(inputFile >> x >> y >> z)
    {
        points.push_back(new Teme(QVector3D(x, y, z)));
    }
    return points;
}


