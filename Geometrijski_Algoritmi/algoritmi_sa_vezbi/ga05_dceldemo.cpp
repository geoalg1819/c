#include "ga05_dceldemo.h"

DCELDemo::DCELDemo(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_duzi)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka), _polygon(imeDatoteke, pCrtanje->height(), pCrtanje->width())
{}

void DCELDemo::pokreniAlgoritam()
{
    AlgoritamBaza_updateCanvasAndBlock();
    emit animacijaZavrsila();
}

void DCELDemo::pokreniNaivniAlgoritam()
{

}

void DCELDemo::crtajAlgoritam(QPainter &painter) const
{
    QPen pen = painter.pen();

    for(int i=0; i<_polygon.edges().size(); i+=2)
    {
        /* Crta se poligon. */
        pen.setColor(Qt::yellow);
        painter.setPen(pen);
        painter.drawLine(_polygon.edges()[i]->origin()->coordinates(),
                         _polygon.edges()[i]->next()->origin()->coordinates());


        /* Crta se poligon "u suprotnom smeru", koriscenjem twin.
         * Da bi se video efekat na crtezu, koordinate su za malo pomerene u odnosu na
         * originalnu tacku.
         */
        pen.setColor(Qt::red);
        painter.setPen(pen);
        painter.drawLine(_polygon.edges()[i]->twin()->origin()->coordinates().x() + 5,
                         _polygon.edges()[i]->twin()->origin()->coordinates().y() + 5,
                         _polygon.edges()[i]->twin()->next()->origin()->coordinates().x() - 5,
                         _polygon.edges()[i]->twin()->next()->origin()->coordinates().y() + 5);
   }

    int curr_num = 1;
    painter.setBrush(Qt::red);
    painter.setPen(Qt::white);
    /* Crtaju se temena, ali kao elipsa, radi lepote. */
    for(Vertex* v: _polygon.vertices())
    {
       painter.drawEllipse(v->coordinates(), 10, 10);
       painter.drawText(v->coordinates().x() - 4, v->coordinates().y() + 4, QString::fromStdString(std::to_string(curr_num)));
       curr_num++;
    }
}

void DCELDemo::crtajAlgoritam3D() const
{

}

bool DCELDemo::is_3D() const
{
    return false;
}


