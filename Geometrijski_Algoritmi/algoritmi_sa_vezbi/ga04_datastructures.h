#ifndef GA04_DATASTRUCTURES_H
#define GA04_DATASTRUCTURES_H

#include <QPointF>
#include <QLineF>

#include "pomocnefunkcije.h"

/*******************************EVENT QUEUE*****************************/

/* Enumeracija tipova tacaka */
enum EventType {UPPER_POINT, LOWER_POINT, INTERSECTION};

/* Struktura koja opisuje jedan dogadjaj:
    event - koordinate tacke
    type - tip dogadaja
    U slucaju da je u pitanu gornja ili donja tacka,
    line1 sadrzi pokazivac na duz kojoj ta tacka pripada,
    a line2 je nullptr
    U slucaju da je u pitanu tacka preseka,
    line1 i line2 su duzi koje se u njoj seku
*/
struct EventQueueNode
{
  QPointF event;
  QLineF* line1;
  QLineF* line2;
  EventType type;
};

struct EventQueueComp
{
    bool operator()(const EventQueueNode levi, const EventQueueNode desni) const
    {
        return (levi.event.y() > desni.event.y()) ||
               (levi.event.y() == desni.event.y() && levi.event.x() < desni.event.x());
    }
};

/*******************************STATUS QUEUE*****************************/

/*
Funktor za poredjenje dve duzi.
Duzi poredimo po mestu preseka sa brisucom pravom.
(Preciznije: x kooridinati preseka sa brisucom pravom)
Iz tog razloga nam treba podatak o trenutnoj poziciji brisuce prave.
Taj podatak cemo dobiti tako sto cemo na pocetku u _ypos staviti
adresu y koordinate nase brisuce prave.
*/

struct StatusQueueComp
{
private:
    double *_ypoz;

public:
    StatusQueueComp(double *ypoz)
        :_ypoz(ypoz)
    {}

    bool operator()(const QLineF* line1, const QLineF* line2) const
    {
        //Oduzimamo neki mali broj jer ako se gleda pozicija u tacki preseka
        //onda ce presek1 i presek2 da budu jedna te ista tacka, pa ove duzi
        //nije moguce pravilno sortirati
        QLineF brisucaPrava = QLineF(0, (*_ypoz) - 0.5, 10, (*_ypoz) - 0.5);

        QPointF presek1;
        QPointF presek2;

        pomocnefunkcije::presekDuzi(brisucaPrava, *line1, presek1);
        pomocnefunkcije::presekDuzi(brisucaPrava, *line2, presek2);

        return presek1.x() < presek2.x();
    }
};

#endif // GA04_DATASTRUCTURES_H
