#ifndef GA06_TRIANGULACIJA_H
#define GA06_TRIANGULACIJA_H

#include <set>

#include "algoritambaza.h"
#include "algoritmi_sa_vezbi/ga05_dcel.h"
#include "algoritmi_sa_vezbi/ga06_datastructures.h"

class Triangulacija : public AlgoritamBaza
{
public:
    Triangulacija(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_tacaka = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;

private:

    ///
    /// \brief MonotonePartition
    /// Od datog poligona kreira vise monotonih poligona
    void monotonePartition();

    /* Obrada temena za pravljenje monotonog poligona
     * Ove metode su opsane pseudo kodom u Computational Geometry
     * na stranama 53 i 54
     * Brojevi koji stoje u komentarima implementacije ovih metoda
     * odgovaraju brojevima u pseudo kodu u knjizi
     */
    void handleStartVertex(Vertex *v);
    void handleEndVertex(Vertex *v);
    void handleSplitVertex(Vertex *v);
    void handleMergeVertex(Vertex *v);
    void handleRegularVertex(Vertex *v);

    /*
     * Ova metoda je opsana pseudo kodom u Computational Geometry
     * na strani 57
     * Brojevi koji stoje u komentarima implementacije ove metode
     * odgovara brojevima u pseudo kodu u knjizi
     */
    void triangulacija(Field *f);
    bool istiLanac(HalfEdge *e1, HalfEdge *e2);
    bool leviLanac(HalfEdge *e1, HalfEdge *e2);
    bool desniLanac(HalfEdge *e1, HalfEdge *e2);
    ///
    /// \brief TriangulacijaArray
    /// Za svako polje poligona poziva se Triangulacija
    ///
    void  TriangulacijaArray();

    ///
    /// \brief makeEdge
    /// Pravi dijagonalu
    ///
    void makeEdge(Vertex* v1, Vertex* v2);

    /// TODO: popraviti
    /// \brief connectNextAndPrevEdges
    /// \param numberEdges
    /// Funkcija koja na kraju particionisanja za novonapravljenje
    /// dijagonale vrsi potrebno menjanje next i prev pokazivaca
    /// ne radi dobro kad vise dijagonala ide iz jedne tacke
    ///
    void connectNextAndPrevEdges(int numberEdges);

    int _brisucaPravaY;

    /* Vertex u Triangulaciji ima tip
     * A HalfEdge ima polje helper
     * Ova polja su dodata u DCEL
     * Zato, pogledati implementaciju DCEL
     * I dodata izmene
     */
    // MOTONE PARTITION
    std::set<Vertex*, EventQueueCompTriangulation> _eventQueue;
    std::set<HalfEdge*, StatusQueueCompTriangulation> _statusQueue;

    //TRIANGULATION
    std::vector<HalfEdge*> _stekTriangulacije;
    std::set<HalfEdge*, EventQueueCompTriangulation2> _eventQueueTriangulation;

    DCEL _polygon;
};

#endif // GA06_TRIANGULACIJA_H
