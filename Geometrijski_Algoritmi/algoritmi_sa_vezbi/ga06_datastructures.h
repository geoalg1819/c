#ifndef GA06_DATASTRUCTURES_H
#define GA06_DATASTRUCTURES_H

#include "algoritmi_sa_vezbi/ga05_dcel.h"
#include "pomocnefunkcije.h"

///
/// \brief The EventQueueComp struct
/// Brisuca prava se krece od jedne ka drugoj EventTacki
///
struct EventQueueCompTriangulation
{
    bool operator()(const Vertex* levi, const Vertex* desni) const
    {
        return (levi->coordinates().y() > desni->coordinates().y()) ||
               (levi->coordinates().y() == desni->coordinates().y() &&
                levi->coordinates().x() < desni->coordinates().x());
    }
};


// Poredjenje je na osnovu preseka sa imaginarnom brisucom pravom
// ipak, nije potrebno zaista odrediti presek
// vec se moze koristi metod konveksan
// Ipak, ko zeli, moze zaista da odredi presek
struct StatusQueueCompTriangulation
{
private:
    int *_ypoz;

public:
    StatusQueueCompTriangulation(int *ypoz)
        :_ypoz(ypoz)
    {}

    bool operator()(const HalfEdge* line1, const HalfEdge* line2) const
    {
        QLineF brisucaPrava = QLineF(0, (*_ypoz), 10, (*_ypoz));

        QPointF presek1;
        QPointF presek2;

        QLineF l1 = QLineF(line1->x1(), line1->y1(),
                           line1->x2(), line1->y2());

        QLineF l2 = QLineF(line2->x1(), line2->y1(),
                           line2->x2(), line2->y2());


        pomocnefunkcije::presekDuzi(brisucaPrava, l1, presek1);
        pomocnefunkcije::presekDuzi(brisucaPrava, l2, presek2);

        return presek1.x() < presek2.x();
    }
};

///
/// \brief The EventQueueCompTriangulation2 struct
/// U Event za Triangulaciju pamtimo HalfEdge, a ne vertex
/// (kao sto je dato u knjizi)
/// zbog problema u implementaciji koji postoje ako se koristi vertex
///
struct EventQueueCompTriangulation2
{
    bool operator()(const HalfEdge* lhs, const HalfEdge* rhs) const
    {
        return (lhs->origin()->coordinates().y() > rhs->origin()->coordinates().y() ||
                (lhs->origin()->coordinates().y() == rhs->origin()->coordinates().y() &&
                 lhs->origin()->coordinates().x() < rhs->origin()->coordinates().x()));
    }
};

#endif // GA06_DATASTRUCTURES_H
