#include "ga05_dcel.h"

#include <iostream>
#include <fstream>

/***********************************************************************/
/*                               DCEL                                  */
/***********************************************************************/

/* Ovaj konstruktor nije za ispit/test.
 * Predstavlja opsti slucaj konstruisanja poligona sa rupama
 */
DCEL::DCEL(std::string imeDatoteke, int h, int w)
    :_vertices{}, _edges{}, _fields{}
{
    std::ifstream in(imeDatoteke);
    std::string tmp;

    in >> tmp;
    if (tmp.compare("OFF") != 0)
    {
        std::cout << "Wrong file format: " << imeDatoteke << std::endl;
        exit(EXIT_FAILURE);
    }

    int vertexNum, edgeNum, fieldNum;
    in >> vertexNum >> edgeNum >> fieldNum;

    /* Kreiranje temena. */
    /* Za ozbiljnije testranje:
     * Proveriti dokumentaciju za off format.
     * Ovde je prilagodjeno za 2d. I za velicinu naseg kanvasa.
     * Odnosno, podrazumeva se ispravan ulaz.
     * TODO: SREDITI OVO
     */
    float tmpx, tmpy, tmpz;
    int x, y;
    Vertex* v;
    for(int i=0; i<vertexNum; i++)
    {
        /* Koordinate u off foramatu su realne, i najcesce [-1, 1]
         * (pp da je uvek tako, da ne ulazimo u detalje)
         * Ovde ih namestam da slika bude na sredini kanvasa.
         */
        in >> tmpx >> tmpy >> tmpz;
        x = static_cast<int>((tmpx + 1)/2.0f*w);
        y = static_cast<int>((tmpy + 1)/2.0f*h);
        v = new Vertex({{x, y}, NULL});
        _vertices.push_back(v);
    }

    /* Pravljenje polja i poluivica. */
    int firstIdx;
    for(int i=0; i<fieldNum; i++)
    {
        Field* f = new Field();
        _fields.push_back(f);

        int fvNum, prevIdx, currIdx;

        /* ucitava se broj temena trenutnog polja. */
        in >> fvNum;

        //Ucitava se indeks prvog temena
        in >> firstIdx;
        prevIdx = firstIdx;

        /* Koristimo kao temp za ivice koje se prave. */
        HalfEdge *ei1, *ei2;
        /* Pomocni skup ivica. Problem je sto prev i next pokazivaci
         * ne mogu da se postave tokom kreiranja ivica, vec
         * tek kad sve ivice se naprave. Zato se koristi ova pomocna lista.
         */
        std::vector<HalfEdge*> tmpHalfEdgeList;

        for(int j=1; j<fvNum; j++)
        {
            //Ucitava se indeks narednog temena polja
            in >> currIdx;
            //Proverava se da li stranice vec postoje
            HalfEdge* e = findEdge(_vertices[prevIdx], _vertices[currIdx]);
            if (e != nullptr)
            {
                prevIdx = currIdx;
                tmpHalfEdgeList.push_back(e);
                tmpHalfEdgeList.push_back(e->twin());
                continue;
            }

            ei1 = new HalfEdge(_vertices[prevIdx], nullptr, nullptr, nullptr, nullptr);
            ei2 = new HalfEdge(_vertices[currIdx], ei1, nullptr, nullptr, f);
            tmpHalfEdgeList.push_back(ei1);
            tmpHalfEdgeList.push_back(ei2);
            ei1->setTwin(ei2);

            _vertices[prevIdx]->setIncidentEdge(ei1);
            _edges.push_back(ei1);
            _edges.push_back(ei2);

            prevIdx = currIdx;
        }

        /* Povezuju se prva i poslednja tacka. */
        HalfEdge* e = findEdge(_vertices[prevIdx], _vertices[firstIdx]);
        if (e != NULL)
        {
            tmpHalfEdgeList.push_back(e);
            tmpHalfEdgeList.push_back(e->twin());
        }
        else
        {
            ei1 = new HalfEdge(_vertices[prevIdx], nullptr, nullptr, nullptr, nullptr);
            ei2 = new HalfEdge(_vertices[firstIdx], ei1, nullptr, nullptr, f);
            ei1->setTwin(ei2);

            tmpHalfEdgeList.push_back(ei1);
            tmpHalfEdgeList.push_back(ei2);

            _vertices[prevIdx]->setIncidentEdge(ei1);
            _edges.push_back(ei1);
            _edges.push_back(ei2);
            f->setOuterComponent(ei2);
        }

        /* Naknadno se postavljaju prev i next pokazivaci. */
        for(int k=0; k<2*fvNum; k+=2)
        {
            HalfEdge* ek1 = tmpHalfEdgeList[k];
            HalfEdge* ek2 = tmpHalfEdgeList[k+1];

            if(ek1->next() == nullptr)
            {
                ek1->setNext(tmpHalfEdgeList[(k+2)%(2*fvNum)]);
                ek2->setPrev(tmpHalfEdgeList[(k+3)%(2*fvNum)]);

                if(ek1->prev() == nullptr)
                    ek1->setPrev(tmpHalfEdgeList[(k-2+2*fvNum)%(2*fvNum)]);
                ek2->setNext(tmpHalfEdgeList[(k-1+2*fvNum)%(2*fvNum)]);
            }
            else
            {
               //Ivica je postojala pa treba prevezati postojece
                ek2->setNext(tmpHalfEdgeList[(k-1+2*fvNum)%(2*fvNum)]);
                ek2->setPrev(tmpHalfEdgeList[(k+3)%(2*fvNum)]);

                ek1->next()->twin()->setNext(tmpHalfEdgeList[(k+2)%(2*fvNum)]);
                tmpHalfEdgeList[(k+2)%(2*fvNum)]->setPrev(ek1->next()->twin());

                if(ek1->prev())
                {
                    ek1->prev()->twin()->setPrev(tmpHalfEdgeList[(k-2+2*fvNum)%(2*fvNum)]);
                    tmpHalfEdgeList[(k-2+2*fvNum)%(2*fvNum)]->setNext(ek1->prev()->twin());
                }
            }
        }
    }

}

DCEL::DCEL(const std::vector<QPoint> &tacke)
    :_vertices{}, _edges{}, _fields{}
{
    loadData(tacke);
}

DCEL::DCEL()
    :_vertices{}, _edges{}, _fields{}
{}

std::vector<Vertex *> DCEL::vertices() const
{
    return _vertices;
}

std::vector<Vertex*> &DCEL::v()
{
    return _vertices;
}

const std::vector<Vertex*> *DCEL::cv() const
{
    return &_vertices;
}

std::vector<HalfEdge *> DCEL::edges() const
{
    return _edges;
}

std::vector<HalfEdge *> &DCEL::e()
{
    return _edges;
}

const std::vector<HalfEdge *> *DCEL::ce() const
{
    return &_edges;
}

std::vector<Field *> DCEL::fields() const
{
    return _fields;
}

std::vector<Field*> &DCEL::f()
{
    return _fields;
}

const std::vector<Field*> *DCEL::cf() const
{
    return &_fields;
}

void DCEL::setFields(std::vector<Field *> &fileds)
{
    _fields = fileds;
}

void DCEL::loadData(const std::vector<QPoint> &tacke)
{
    Field* fUnutrasnje = new Field();
    _fields.push_back(fUnutrasnje);

    HalfEdge *previousEdge = nullptr;
    HalfEdge *firstEdge = nullptr; //pamtimo radi spajanja na kraju
    Vertex *firstVertex = nullptr;

    for(auto itTacke = tacke.cbegin(); itTacke != tacke.cend(); ++itTacke)
    {
        QPoint t = *itTacke;
        Vertex *v = new Vertex();
        _vertices.push_back(v);
        HalfEdge *e = new HalfEdge();
        _edges.push_back(e);
        HalfEdge *ep = new HalfEdge();
        _edges.push_back(ep);

        if(nullptr == firstEdge)
        {
            firstEdge = e;
            firstVertex = v;
        }

        e->setTwin(ep);
        e->setOrigin(v);
        e->setIncidentFace(fUnutrasnje);

        ep->setTwin(e);

        v->setCoordinates(t);
        v->setIncidentEdge(e);

        if (nullptr != previousEdge)
        {
            ep->setNext(previousEdge->twin());
            e->setPrev(previousEdge);
            previousEdge->setNext(e);
            previousEdge->twin()->setPrev(ep);
            previousEdge->twin()->setOrigin(v);
        }

        previousEdge = e;
    }

    //Povezivanje prve i poslednje tacke
    previousEdge->setNext(firstEdge);
    firstEdge->setPrev(previousEdge);
    previousEdge->twin()->setPrev(firstEdge->twin());
    firstEdge->twin()->setNext(previousEdge->twin());
    previousEdge->twin()->setOrigin(firstVertex);

    fUnutrasnje->setInnerComponent(firstEdge);
    fUnutrasnje->setOuterComponent(firstEdge->twin());
}

void DCEL::insertEdge(HalfEdge *e)
{
    _edges.push_back(e);
}

void DCEL::insertFiled(Field *f)
{
    _fields.push_back(f);
}

HalfEdge *DCEL::findEdge(Vertex *start, Vertex *end)
{
    for(HalfEdge* e : _edges)
    {
        if (e->origin() == start && e->twin()->origin() == end)
            return e;
    }

    return nullptr;
}

/***********************************************************************/
/*                             VERTEX                                  */
/***********************************************************************/

Vertex::Vertex()
    :_coordinates{}, _incidentEdge{nullptr}, _obradjen{false}
{}

Vertex::Vertex(QPoint coordinates, HalfEdge *incidentEdge)
    :_coordinates{coordinates}, _incidentEdge{incidentEdge}
{}

QPoint Vertex::coordinates() const
{
    return _coordinates;
}

void Vertex::setCoordinates(const QPoint &coordinates)
{
    _coordinates = coordinates;
}

HalfEdge *Vertex::incidentEdge() const
{
    return _incidentEdge;
}

void Vertex::setIncidentEdge(HalfEdge *incidentEdge)
{
    _incidentEdge = incidentEdge;
}

VertexType Vertex::type() const
{
    return _type;
}

void Vertex::setType(VertexType t)
{
    _type = t;
}

bool Vertex::obradjen() const
{
    return _obradjen;
}

void Vertex::obradi()
{
    _obradjen = true;
}


/***********************************************************************/
/*                           HALFEDGE                                  */
/***********************************************************************/

HalfEdge::HalfEdge()
    :_origin{nullptr}, _twin{nullptr}, _next{nullptr}, _prev{nullptr}, _incidentFace{nullptr}, _helper{nullptr}
{}

HalfEdge::HalfEdge(Vertex *origin, HalfEdge *twin, HalfEdge *next, HalfEdge *prev, Field *incidentFace)
    :_origin(origin), _twin(twin), _next(next), _prev(prev), _incidentFace(incidentFace)
{}

Vertex *HalfEdge::origin() const
{
    return _origin;
}

void HalfEdge::setOrigin(Vertex *origin)
{
    _origin = origin;
}

HalfEdge *HalfEdge::twin() const
{
    return _twin;
}

void HalfEdge::setTwin(HalfEdge *twin)
{
    _twin = twin;
}

HalfEdge *HalfEdge::next() const
{
    return _next;
}

void HalfEdge::setNext(HalfEdge *next)
{
    _next = next;
}

HalfEdge *HalfEdge::prev() const
{
    return _prev;
}

void HalfEdge::setPrev(HalfEdge *prev)
{
    _prev = prev;
}

Field *HalfEdge::incidentFace() const
{
    return _incidentFace;
}

void HalfEdge::setIncidentFace(Field *incidentFace)
{
    _incidentFace = incidentFace;
}

int HalfEdge::x1() const
{
    return _origin->coordinates().x();
}

int HalfEdge::y1() const
{
    return _origin->coordinates().y();
}

QPoint HalfEdge::t1() const
{
    return _origin->coordinates();
}

QPoint HalfEdge::t2() const
{
    return _twin->origin()->coordinates();
}

int HalfEdge::x2() const
{
    return _twin->origin()->coordinates().x();
}

int HalfEdge::y2() const
{
    return _twin->origin()->coordinates().y();
}

void HalfEdge::setHelper(Vertex *h)
{
    _helper = h;
}

Vertex *HalfEdge::helper()
{
    return _helper;
}

/***********************************************************************/
/*                             FIELD                                   */
/***********************************************************************/

Field::Field()
    :_outerComponent{nullptr}, _inerComponents{}
{}

Field::Field(HalfEdge *outerComponent, std::vector<HalfEdge *> inerComponent)
    :_outerComponent{outerComponent}, _inerComponents{inerComponent}
{}

HalfEdge *Field::outerComponent() const
{
    return _outerComponent;
}

void Field::setOuterComponent(HalfEdge *outerComponent)
{
    _outerComponent = outerComponent;
}

std::vector<HalfEdge *> Field::inerComponents() const
{
    return _inerComponents;
}

HalfEdge *Field::innerComponent() const
{
    if(inerComponents().empty())
    {
        return nullptr;
    }

    return _inerComponents[0];
}

void Field::setInerComponents(std::vector<HalfEdge *> inerComponents)
{
    _inerComponents = inerComponents;
}

void Field::setInnerComponent(HalfEdge* innerComponent)
{
    _inerComponents.push_back(innerComponent);
}




