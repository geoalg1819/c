#include "ga06_triangulacija.h"

#include <iostream>
#include "pomocnefunkcije.h"

Triangulacija::Triangulacija(QWidget *pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke, int broj_tacka)
    :AlgoritamBaza(pCrtanje, pCrtanjeGL, pauzaKoraka),
    _eventQueue(), _statusQueue(StatusQueueCompTriangulation(&_brisucaPravaY))
{
    _brisucaPravaY = _pCrtanje->height();

    std::vector<QPoint> tacke;

    //ucitavanje tacka
    if (imeDatoteke != "")
        tacke = ucitajPodatkeIzDatoteke(imeDatoteke);
    /* poligonalne duzi ne smeju da se medjusobno presecaju
     * kod nasumicnih tacka to nije moguce garatovati
     * pa ova opcija nije podrzana.
    else
        tacke = generisiNasumicneTacke(broj_duzi);
    */

    _polygon.loadData(tacke);
}

void Triangulacija::pokreniAlgoritam()
{
    _eventQueue.clear();
    _eventQueueTriangulation.clear();
    _statusQueue.clear();
    _stekTriangulacije.clear();

    AlgoritamBaza_updateCanvasAndBlock();

    monotonePartition();
    AlgoritamBaza_updateCanvasAndBlock();

    std::vector<Field*> fields = _polygon.fields();
    int n = fields.size();
    /* Imati u vidu da broj polja nije konstantan jer
     * ce triangulacijom biti dodato jos novih polja
     * ali ta nova pola ne treba ponovo obradjivati.
     * Zato dole ne moze u uslovu da stoji _polygon.fields().size()
     * ili (for Field* f : _polygon.fields())
     */

    for(int i=0; i<n; i++)
        triangulacija(fields[i]);
    AlgoritamBaza_updateCanvasAndBlock();

    emit animacijaZavrsila();
}

void Triangulacija::pokreniNaivniAlgoritam()
{

}

void Triangulacija::crtajAlgoritam(QPainter &painter) const
{
    //postavljanje boja
    QPen pen = painter.pen();

    QPen magneta = painter.pen();
    magneta.setColor(Qt::magenta);
    magneta.setWidth(10);

    QPen blue = painter.pen();
    blue.setColor(Qt::blue);
    blue.setWidth(10);

    QPen yellow = painter.pen();
    yellow.setColor(Qt::yellow);
    yellow.setWidth(10);

    QPen red = painter.pen();
    red.setColor(Qt::red);
    red.setWidth(10);

    QPen green = painter.pen();
    green.setColor(Qt::green);
    green.setWidth(10);

    //crtanje poligona
    //preskacu se twin stranice
    for(int i=0; i<_polygon.edges().size(); i+=2)
    {
        /* Crta se poligon. */
        painter.drawLine(_polygon.edges()[i]->origin()->coordinates(),
                         _polygon.edges()[i]->next()->origin()->coordinates());
    }

    //crtanje temena prema tipu
    for(const Vertex* p : _eventQueue)
    {
        if (p->type() == START_VERTEX)
        {
            painter.setPen(magneta);
            painter.drawPoint(p->coordinates());
        }
        else if (p->type() == END_VERTEX)
        {
            painter.setPen(blue);
            painter.drawPoint(p->coordinates());
        }
        else if (p->type() == REGULAR_VERTEX)
        {
            painter.setPen(yellow);
            painter.drawPoint(p->coordinates());
        }
        else if (p->type() == SPLIT_VERTEX)
        {
            painter.setPen(red);
            painter.drawPoint(p->coordinates());
        }
        else if (p->type() == MERGE_VERTEX)
        {
            painter.setPen(green);
            painter.drawPoint(p->coordinates());
        }
    }

    //Iscrtavamo trenutnu poziciju brisuce prave
    pen.setWidth(1);
    painter.setPen(pen);
    painter.drawLine(0, _brisucaPravaY, _pCrtanje->width(), _brisucaPravaY);
}

void Triangulacija::crtajAlgoritam3D() const
{

}

bool Triangulacija::is_3D() const
{
    return false;
}

/***********************************************************************/
/*                    MONOTONE PARTITION                               */
/***********************************************************************/


void Triangulacija::monotonePartition()
{
    _eventQueue.clear();
    _statusQueue.clear();

    std::vector<Vertex*> vertices = _polygon.vertices();
    int n = vertices.size();

    int numberEdges = _polygon.edges().size();

    /* Ispitivanje tipa i dodavanje u red dogadjaja. */
    for(int i=0; i<n; i++)
    {
        Vertex* v = vertices[i];
        Vertex* v_next = vertices[i]->incidentEdge()->twin()->origin();
        Vertex* v_prev = vertices[i]->incidentEdge()->prev()->origin();

        if (pomocnefunkcije::ispod(v_next->coordinates(), v->coordinates()) &&
            pomocnefunkcije::ispod(v_prev->coordinates(), v->coordinates()))
        {
            if (pomocnefunkcije::konveksan(v->coordinates(), v_next->coordinates(), v_prev->coordinates()))
            {
                v->setType(START_VERTEX);
            }
            else
            {
                v->setType(SPLIT_VERTEX);
            }
        }
        else if (pomocnefunkcije::ispod(v->coordinates(), v_next->coordinates()) &&
                 pomocnefunkcije::ispod(v->coordinates(), v_prev->coordinates()))
        {
            if (pomocnefunkcije::konveksan(v->coordinates(), v_next->coordinates(), v_prev->coordinates()))
            {
                 v->setType(END_VERTEX);
            }
            else
                 v->setType(MERGE_VERTEX);
        }
        else
            v->setType(REGULAR_VERTEX);

        _eventQueue.insert(v);
    }

    AlgoritamBaza_updateCanvasAndBlock();

    //DODAVANJE DIJAGONALA
    //Dok god ima neobradjenih dogadjaja
    while(!_eventQueue.empty())
    {
        //Preuzimamo sledeci dogadjaj i brisemo ga iz reda
        Vertex* eventNode = *_eventQueue.begin();
        _eventQueue.erase(_eventQueue.begin());

        //Pomeramo brisucu pravu
        _brisucaPravaY = eventNode->coordinates().y();
        AlgoritamBaza_updateCanvasAndBlock();

        //Obradjujemo dogadaj
        switch(eventNode->type())
        {
           case START_VERTEX: handleStartVertex(eventNode); AlgoritamBaza_updateCanvasAndBlock(); break;
           case END_VERTEX: handleEndVertex(eventNode); AlgoritamBaza_updateCanvasAndBlock(); break;
           case MERGE_VERTEX: handleMergeVertex(eventNode); AlgoritamBaza_updateCanvasAndBlock(); break;
           case REGULAR_VERTEX: handleRegularVertex(eventNode); AlgoritamBaza_updateCanvasAndBlock(); break;
           case SPLIT_VERTEX: handleSplitVertex(eventNode); AlgoritamBaza_updateCanvasAndBlock(); break;
        }
   }

   connectNextAndPrevEdges(numberEdges);

   _eventQueue.clear();
}

void Triangulacija::handleStartVertex(Vertex *v)
{
    v->incidentEdge()->setHelper(v);
    _statusQueue.insert(v->incidentEdge());
}

void Triangulacija::handleEndVertex(Vertex *v)
{
    if (v->incidentEdge()->prev()->helper()->type() == MERGE_VERTEX)
    {
        makeEdge(v, v->incidentEdge()->prev()->helper());
    }

    auto e = _statusQueue.find(v->incidentEdge()->prev());
    _statusQueue.erase(e);
}

void Triangulacija::handleSplitVertex(Vertex *v)
{
    /* pravimo vestacku ivicu kome je i pocetno i krajnje teme v i
     * u odnosu na tu ivicu trazimo kandidata za direktno levo od v
     * indeks ivice je nebitan.
     */
    //1.
    HalfEdge* ei1 = new HalfEdge(v, nullptr, nullptr, nullptr, nullptr);
    HalfEdge* ei2 = new HalfEdge(v, ei1, nullptr, nullptr, nullptr);
    ei1->setTwin(ei2);

    auto ej = _statusQueue.lower_bound(ei2);

    //2.
    makeEdge((*ej)->helper(), v);

    //3.
    (*ej)->setHelper(v);

    //4.
    _statusQueue.insert(v->incidentEdge());
    v->incidentEdge()->setHelper(v);
}

void Triangulacija::handleMergeVertex(Vertex *v)
{
    //1.
    if (v->incidentEdge()->prev()->helper()->type() == MERGE_VERTEX)
        makeEdge(v, v->incidentEdge()->prev()->helper());

    //3.
    auto e = _statusQueue.find(v->incidentEdge()->prev());
    _statusQueue.erase(e);

    //4.
    HalfEdge* ei1 = new HalfEdge(v, nullptr, nullptr, nullptr, nullptr);
    HalfEdge* ei2 = new HalfEdge(v, ei1, nullptr, nullptr, nullptr);
    ei1->setTwin(ei2);

    auto ej = _statusQueue.lower_bound(ei2);

    //5.
    if ((*ej)->helper()->type() == MERGE_VERTEX)
        makeEdge(v, (*ej)->helper());

    //7.
    (*ej)->setHelper(v);
}

void Triangulacija::handleRegularVertex(Vertex *v)
{
    //1.
    Vertex* v_next = v->incidentEdge()->twin()->origin();
    if (pomocnefunkcije::ispod(v_next->coordinates(), v->coordinates()))
    {
        //2.
        if (v->incidentEdge()->prev()->helper()->type() == MERGE_VERTEX)
            makeEdge(v, v->incidentEdge()->prev()->helper());

        //4.
        auto e = _statusQueue.find(v->incidentEdge()->prev());
        _statusQueue.erase(e);

        //5.
        _statusQueue.insert(v->incidentEdge());
        v->incidentEdge()->setHelper(v);

    }
    else
    {
        //6.
        HalfEdge* ei1 = new HalfEdge(v, nullptr, nullptr, nullptr, nullptr);
        HalfEdge* ei2 = new HalfEdge(v, ei1, nullptr, nullptr, nullptr);
        ei1->setTwin(ei2);

        auto ej = _statusQueue.lower_bound(ei2);

        //7.
        if ((*ej)->helper()->type() == MERGE_VERTEX)
            makeEdge(v, (*ej)->helper());

        //9.
        (*ej)->setHelper(v);
    }
}

/***********************************************************************/
/*                         TRIANGULATION                               */
/***********************************************************************/

void Triangulacija::triangulacija(Field* f)
{
    //radi vizuelizacije
    _brisucaPravaY = _pCrtanje->height();
    AlgoritamBaza_updateCanvasAndBlock();

    _stekTriangulacije.clear();
    _eventQueueTriangulation.clear();

    //1.
    HalfEdge* e = f->outerComponent();
    HalfEdge* firstEdge = e;
    HalfEdge*  eventNode;
    HalfEdge*  eventNode_prev;

    do
    {
        _eventQueueTriangulation.insert(e);
        e = e->next();
     }while(e != firstEdge);


    //2.
    if(!_eventQueueTriangulation.empty())
    {
        //Preuzimamo sledeci dogadjaj i brisemo ga iz reda
         eventNode = *_eventQueueTriangulation.begin();
         _eventQueueTriangulation.erase(_eventQueueTriangulation.begin());
        _stekTriangulacije.push_back(eventNode);
        _brisucaPravaY = eventNode->origin()->coordinates().y();
        AlgoritamBaza_updateCanvasAndBlock();
    }
    if(!_eventQueueTriangulation.empty())
    {
        //Preuzimamo sledeci dogadjaj i brisemo ga iz reda
        eventNode = *_eventQueueTriangulation.begin();
        _eventQueueTriangulation.erase(_eventQueueTriangulation.begin());

        eventNode_prev = eventNode;
        _stekTriangulacije.push_back(eventNode);
        _brisucaPravaY = eventNode->origin()->coordinates().y();
        AlgoritamBaza_updateCanvasAndBlock();
    }


    while(!_eventQueueTriangulation.empty())
    {
        eventNode = *_eventQueueTriangulation.begin();
        _eventQueueTriangulation.erase(_eventQueueTriangulation.begin());
        _brisucaPravaY = eventNode->origin()->coordinates().y();
        AlgoritamBaza_updateCanvasAndBlock();

        HalfEdge* stackNode;
        HalfEdge* stackNode_first;

        if (istiLanac(eventNode, _stekTriangulacije[_stekTriangulacije.size()-1]))
        {
            //8.
            stackNode_first = _stekTriangulacije[_stekTriangulacije.size()-1];
            _stekTriangulacije.pop_back();

            bool leviL = leviLanac(eventNode, stackNode_first);
            bool desniL = desniLanac(eventNode, stackNode_first);

            //9.
            while(_stekTriangulacije.size() != 0)
            {
                stackNode= _stekTriangulacije[_stekTriangulacije.size()-1];
                _stekTriangulacije.pop_back();

                //if (konveksan(v, u, v_prethodnik))
                //ne moze samo ovaj jedan if, mora da se vidi na kom je lancu, levom ili desnom
                //pa onda stackNodezavisnosti od toga i ispitivanje konveksnosto je drugacije
                if ((leviL &&
                     pomocnefunkcije::konveksan(
                         eventNode->origin()->coordinates(),
                         stackNode->origin()->coordinates(),
                         stackNode_first->origin()->coordinates())) ||
                    (desniL &&
                     pomocnefunkcije::konveksan(
                         eventNode->origin()->coordinates(),
                         stackNode_first->origin()->coordinates(),
                         stackNode->origin()->coordinates())))
                {
                    makeEdge(eventNode->origin(), stackNode->origin());
                    AlgoritamBaza_updateCanvasAndBlock();
                }
                else
                    break;
            }

            //push last one back to S
            _stekTriangulacije.push_back(stackNode);
            //10.
            _stekTriangulacije.push_back(eventNode);
        }
        else
        {
            //5.
            while(_stekTriangulacije.size() != 0)
            {
                stackNode= _stekTriangulacije[_stekTriangulacije.size()-1];
                //6.
                if (_stekTriangulacije.size() != 1) //ne dodaji za poslednju
                {
                    makeEdge(eventNode->origin(), stackNode->origin());
                    AlgoritamBaza_updateCanvasAndBlock();
                }

                _stekTriangulacije.pop_back();
            }

            //7.
            _stekTriangulacije.push_back(eventNode_prev);
            _stekTriangulacije.push_back(eventNode);
        }

        eventNode_prev = eventNode;
    }


    //11.
    eventNode = eventNode_prev;
    while(_stekTriangulacije.size() != 0)
    {
        HalfEdge* stackNode= _stekTriangulacije[_stekTriangulacije.size()-1];

        makeEdge(eventNode->origin(), stackNode->origin());
        AlgoritamBaza_updateCanvasAndBlock();

        _stekTriangulacije.pop_back();
    }

    _eventQueueTriangulation.clear();
}

bool Triangulacija::istiLanac(HalfEdge* e1, HalfEdge* e2)
{
    QPoint a = e1->origin()->coordinates();
    QPoint a_next = e1->twin()->origin()->coordinates();

    QPoint b = e2->origin()->coordinates();
    QPoint b_next = e2->twin()->origin()->coordinates();

    if (pomocnefunkcije::ispod(a, a_next) &&
        pomocnefunkcije::ispod(b, b_next))
        return true;
    else if (pomocnefunkcije::ispod(a_next, a) &&
             pomocnefunkcije::ispod(b_next, b))
        return true;
    else
        return false;
}

bool Triangulacija::leviLanac(HalfEdge *e1, HalfEdge *e2)
{
    QPoint a = e1->origin()->coordinates();
    QPoint a_next = e1->twin()->origin()->coordinates();

    QPoint b = e2->origin()->coordinates();
    QPoint b_next = e2->twin()->origin()->coordinates();

    if (pomocnefunkcije::ispod(a_next, a) &&
        pomocnefunkcije::ispod(b_next, b))
        return true;
    else
        return false;
}

bool Triangulacija::desniLanac(HalfEdge *e1, HalfEdge *e2)
{
    QPoint a = e1->origin()->coordinates();
    QPoint a_next = e1->twin()->origin()->coordinates();

    QPoint b = e2->origin()->coordinates();
    QPoint b_next = e2->twin()->origin()->coordinates();

    if (pomocnefunkcije::ispod(a, a_next) &&
        pomocnefunkcije::ispod(b, b_next))
        return true;
    else
        return false;
}

/***********************************************************************/
/*                      POMOCNE FUNKCIJE                               */
/***********************************************************************/

void Triangulacija::makeEdge(Vertex *v1, Vertex *v2)
{
    /* kreira se novo polje */
    Field* f = new Field();
    _polygon.insertFiled(f);

    /* Kreiraju se nove ivice. Novo polje odgovara ivici ei1,
     * a staro ivici ei2. */
    HalfEdge* ei1 = new HalfEdge(v1, nullptr, nullptr, nullptr, v1->incidentEdge()->incidentFace());
    HalfEdge* ei2 = new HalfEdge(v2, ei1, nullptr, nullptr, f);
    ei1->setTwin(ei2);

    _polygon.insertEdge(ei1);
    _polygon.insertEdge(ei2);

    /* Za napravljene ivice postavljaju se prethodni i sledeci. */
    ei1->setNext(v2->incidentEdge());
    ei1->setPrev(v1->incidentEdge()->prev());

    ei2->setNext(v1->incidentEdge());
    ei2->setPrev(v2->incidentEdge()->prev());

    f->setOuterComponent(ei2);

    /* Potrebno je promeniti pokazivace i za ostale ivice, tj. da
     * sve ivice koje su "sa druge strane" dijagonale
     * imaju novo polje. Za sada to ne radimo. */

    /* Postavljaju se pokazivaci next i prev za postojece ivice. */
    /* Pokazivace next i prev ne smeju da se menjaju dok
     * se ne zavrsi ceo monoton partition.
     * Inace moton partition nece raditi ispravno.
     * Kada se promene pokazivaci next i prev promeni se
     * i poligon koji posmatramo (tj. moze samo da se posmatra onaj poligon
     * do kog moze da se stigne preko next i prev pokazivaca).
     */

}

void Triangulacija::connectNextAndPrevEdges(int numberEdges)
{
    std::vector<HalfEdge*> edges = _polygon.edges();
    int n = edges.size();

    /* bitno:
     * Next i prev stores pointers to the next and pervious edge on the boundary
     * of Incident_face
     */

    //menjaju se next i prev kod ivica koje su pre i
    //posle novih ivica
    for(int i=numberEdges; i<n; i+=2)
    {
        edges[i]->prev()->setNext(edges[i]);
        edges[i]->next()->setPrev(edges[i]);

        edges[i+1]->next()->twin()->setNext(edges[i]);
        edges[i+1]->prev()->twin()->setPrev(edges[i]);

        //podesavanja za twin
        edges[i+1]->prev()->setNext(edges[i+1]);
        edges[i+1]->next()->setPrev(edges[i+1]);

        edges[i]->next()->twin()->setNext(edges[i+1]);
        edges[i]->prev()->twin()->setPrev(edges[i+1]);
    }
}


