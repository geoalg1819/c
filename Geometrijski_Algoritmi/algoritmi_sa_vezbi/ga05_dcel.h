#ifndef DCEL_H
#define DCEL_H

#include <QPoint>
#include <QWidget>

class Vertex;
class HalfEdge;
class Field;

/* Enumeracija tipova temena *
 * Potrebno za TRIANGULACIJU */
enum VertexType {START_VERTEX, SPLIT_VERTEX, END_VERTEX, MERGE_VERTEX, REGULAR_VERTEX};

///
/// \brief The DCEL class
/// Dvostruko povezna lista (Doubly-Connected Edge List)
/// Vise procitati u knjizi Computational Geometry
///
class DCEL
{
public:
    DCEL(std::string imeDatoteke, int h, int w);

    ///
    /// \brief DCEL
    /// Poligon zadatak tackama u pozitivnom matematickom smeru
    /// bez rupa
    ///
    DCEL(const std::vector<QPoint> &tacke);

    DCEL();

    std::vector<Vertex*> vertices() const;
    std::vector<Vertex *> &v();
    const std::vector<Vertex*> *cv()const;
    std::vector<HalfEdge*> edges() const;
    std::vector<HalfEdge*> &e();
    const std::vector<HalfEdge *> *ce() const;
    std::vector<Field*> fields() const;
    std::vector<Field *> &f();
    const std::vector<Field*> *cf() const;

    void setFields(std::vector<Field*>& fileds);

    void loadData(const std::vector<QPoint> &tacke);

    void insertEdge(HalfEdge* e);
    void insertFiled(Field* f);

private:
    HalfEdge* findEdge(Vertex* start, Vertex* end);


    std::vector<Vertex*> _vertices;
    std::vector<HalfEdge*> _edges;
    std::vector<Field*> _fields;
};



///
/// \brief The Vertex class
/// Teme poligona
///
class Vertex{
public:
    Vertex();
    Vertex(QPoint coordinates, HalfEdge* incidentEdge);

    QPoint coordinates() const;
    void setCoordinates(const QPoint& coordinates);

    HalfEdge* incidentEdge() const;
    void setIncidentEdge(HalfEdge* incidentEdge);

    /* potrebno za TRIANGULACIJU */
    VertexType type() const;
    void setType(VertexType t);

    bool obradjen() const;
    void obradi();

private:
    QPoint _coordinates;
    HalfEdge* _incidentEdge;

    /* potrebno za TRIANGULACIJU */
    VertexType _type;
    bool _obradjen = false;
};

///
/// \brief The HalfEdge class
/// Ovo je zapravo jedna ivica poligona, ali ivica moze imati dva smera, pa je
/// sa HalfEdge oznacen jedan smer, a ona na koju pokazuje (twin) je ista ivica, ali drugi smer
/// Vise o ovoj strukturi procitati u Computational Geoemtry
///
class HalfEdge{
public:
    HalfEdge();
    HalfEdge(Vertex* origin, HalfEdge* twin, HalfEdge* next, HalfEdge* prev, Field* incidentFace);

    Vertex* origin() const;
    void setOrigin(Vertex* origin);

    HalfEdge* twin() const;
    void setTwin(HalfEdge* twin);

    HalfEdge* next() const;
    void setNext(HalfEdge* next);

    HalfEdge* prev() const;
    void setPrev(HalfEdge* prev);

    Field* incidentFace() const;
    void setIncidentFace(Field* incidentFace);

    // vraca koordinate
    // moze i bez naredna 4 metoda, ali se ovim znacajno skracuje pisanje
    // raznih poziva
    // a samim tim i mogucnost greske
    int x1() const;
    int y1() const;
    QPoint t1() const;
    QPoint t2() const;

    int x2() const;
    int y2() const;

    /* potrebno za TRIANGULACIJU */
    void setHelper(Vertex* h);
    Vertex* helper();

private:
    Vertex* _origin;
    HalfEdge* _twin;
    HalfEdge* _next;
    HalfEdge* _prev;
    Field* _incidentFace;

    /* potrebno za TRIANGULACIJU */
    Vertex* _helper;
};


class Field{
public:
    Field();
    Field(HalfEdge* outerComponent, std::vector<HalfEdge *> inerComponents);

    HalfEdge* outerComponent() const;
    void setOuterComponent(HalfEdge* outerComponent);

    std::vector<HalfEdge*> inerComponents() const;
    HalfEdge* innerComponent() const;
    void setInerComponents(std::vector<HalfEdge*> inerComponents);
    void setInnerComponent(HalfEdge *innerComponent);

private:
    HalfEdge* _outerComponent;
    std::vector<HalfEdge*> _inerComponents;
};

#endif // DCEL_H
