#ifndef GA03_DATASTRUCTURES_H
#define GA03_DATASTRUCTURES_H

#include <QVector3D>
#include <math.h>

/* Flegovi */
#define NAOMOTACU  	true
#define UKLONI  	true
#define VIDLJIVA  	true
#define OBRADJENA	true

class Ivica;
class Stranica;

class Teme{
public:
    Teme(QVector3D tacka)
        :_obradjeno(false), _ivica(nullptr), _koordinate(tacka)
    {}

    float x()
    {
        return _koordinate.x();
    }

    float y()
    {
        return _koordinate.y();
    }

    float z()
    {
        return _koordinate.z();
    }

    QVector3D koordinate()
    {
        return _koordinate;
    }

    bool getObradjeno()
    {
        return _obradjeno;
    }

    void setObradjeno(bool param)
    {
        _obradjeno = param;
    }

private:
    QVector3D _koordinate;
    Ivica* _ivica;
    bool _obradjeno;
    // bool _naOmotu; ukoliko je za ispis potrebno ispisati tacke na omotu,
    //                mada se u 3d iscrtavaju ivice ili stranice
};


class Ivica{
public:
    Ivica()
        :_obrisati(false)
    {
        _stranice[0] = nullptr;
        _stranice[1] = nullptr;

        _temena[0] = nullptr;
        _temena[1] = nullptr;
    }

    Ivica(Teme* t1, Teme* t2)
        :_obrisati(false)
    {
        _stranice[0] = nullptr;
        _stranice[1] = nullptr;

        _temena[0] = t1;
        _temena[1] = t2;
    }

    void postavi_stranicu(Stranica* s)
    {
        if (_stranice[0] == nullptr)
            _stranice[0] = s;
        else if (_stranice[1] == nullptr)
            _stranice[1] = s;
    }

    Teme* teme1()
    {
        return _temena[0];
    }

    Teme* teme2()
    {
        return _temena[1];
    }

    Stranica* stranica1()
    {
        return _stranice[0];
    }

    Stranica* stranica2()
    {
        return _stranice[1];
    }

    void setObrisati(bool param)
    {
        _obrisati = param;
    }

    bool getObrisati()
    {
        return _obrisati;
    }

    void zameniVidljivuStranicu(Stranica* s, int i)
    {
        _stranice[i] = s;
    }

    void izmeniRedosledTemena()
    {
        Teme* pom = _temena[0];
        _temena[0] = _temena[1];
        _temena[1] = pom;
    }

private:
    Stranica* _stranice[2];
    bool _obrisati;
    Teme* _temena[2];
};

class Stranica{
public:
    Stranica()
        :_vidljiva(false)
    {
        for(int i=0; i<3; i++)
        {
            _ivice[i] = nullptr;
            _temena[i] = nullptr;
        }
    }

    Stranica(Teme* t1, Teme* t2, Teme* t3, Ivica* i1, Ivica* i2, Ivica* i3)
        :_vidljiva(false)
    {
        _temena[0] = t1;
        _temena[1] = t2;
        _temena[2] = t3;

        _ivice[0] = i1;
        _ivice[1] = i2;
        _ivice[2] = i3;
    }

    Teme* teme1()
    {
        return _temena[0];
    }

    Teme* teme2()
    {
        return _temena[1];
    }

    Teme* teme3()
    {
        return _temena[2];
    }

    void izmeniRedosledTemena()
    {
        Teme* pom = _temena[0];
        _temena[0] = _temena[1];
        _temena[1] = pom;
    }

    void setVidljiva(bool param)
    {
        _vidljiva = param;
    }

    bool getVidljiva()
    {
        return _vidljiva;
    }

private:
    Ivica* _ivice[3];
    Teme* _temena[3];
    bool _vidljiva;
};



#endif // GA03_DATASTRUCTURES_H
