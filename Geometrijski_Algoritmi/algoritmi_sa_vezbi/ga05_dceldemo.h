#ifndef DCELDEMO_H
#define DCELDEMO_H

#include "algoritambaza.h"
#include "pomocnefunkcije.h"
#include "ga05_dcel.h"


class DCELDemo : public AlgoritamBaza
{
public:
    DCELDemo(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka, std::string imeDatoteke = "", int broj_duzi = BROJ_NASUMICNIH_TACAKA);

    void pokreniAlgoritam();
    void pokreniNaivniAlgoritam();
    void crtajAlgoritam(QPainter& painter) const;
    void crtajAlgoritam3D() const;
    bool is_3D() const;

private:
    DCEL _polygon;
};

#endif // DCELDEMO_H
