#ifndef POMOCNEFUNKCIJE_H
#define POMOCNEFUNKCIJE_H

#include <QPoint>
#include <QPointF>
#include <QLineF>
#include <QVector3D>

namespace pomocnefunkcije {
///
/// \brief povrsinaTrougla2 -- izracunava "dvostruku povrsinu trougla" odredjenu sa tackama
/// a, b i c. Pri tome, ne racuna se apsolutna vrednost, pa ako je tacka c sa leve strane
/// prave ab  onda je ta povrsina pozitivna, a inace je negativna.
///
///
    double povrsinaTrougla2(QPoint a, QPoint b, QPoint c);

    double rastojanje (QPoint a, QPoint b);

    ///
    /// \brief saLeveStrane -- koriscenjem funkcije povrsinaTrougla2 odredjuje da li
    /// je tacka c sa leve ili desne strane prave ab
    ///
    bool konveksan(QPoint a, QPoint b, QPoint c);

    bool kolinearne3D(QVector3D a, QVector3D b, QVector3D c);

    double zapremina(QVector3D a, QVector3D b, QVector3D c, QVector3D d);

    bool duzSadrziTacku(QLineF l, QPointF p);

    bool presekDuzi(QLineF l1, QLineF l2, QPointF& presek);

    bool ispod(QPoint a, QPoint b);
}

#endif // POMOCNEFUNKCIJE_H
