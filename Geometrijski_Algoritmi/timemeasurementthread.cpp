#include "timemeasurementthread.h"

#include "config.h"
#include "mainwindow.h"
#include "algoritambaza.h"
#include "algoritmi_sa_vezbi/ga02_konveksniomotac.h"
#include "algoritmi_sa_vezbi/ga07_voronojidijagram.h"
#include "algoritmi_studentski_projekti/ga01_triangulacija.h"
#include "algoritmi_studentski_projekti/ga02_unija_pravougaonika.h"
#include "algoritmi_studentski_projekti/ga04_kdtree.h"
#include "algoritmi_studentski_projekti/ga07_ortogonalni_upiti.h"
#include "algoritmi_studentski_projekti/ga08_hertelmehlhorn.h"

TimeMeasurementThread::TimeMeasurementThread(QString tipAlgoritma, int minValue, int step, int maxValue)
    :QThread (), _algorithmType(tipAlgoritma), _minValue(minValue), _step(step), _maxValue(maxValue)
{
}

void TimeMeasurementThread::run()
{
    clock_t begin, end;
    double optimalTime, naiveTime;

    AlgoritamBaza* pAlgorithm = nullptr;

    for(int i= _minValue; i <= _maxValue; i += _step)
    {
        std::cout << "i: " << i << std::endl;

        if (_algorithmType == "Unija pravougaonika")
            pAlgorithm = new UnijaPravougaonika(nullptr, nullptr, 0, "", i);
        if (_algorithmType == "KdTree")
            pAlgorithm = new KdTree(nullptr, nullptr, 0, "", i);
        if (_algorithmType == "Ortogonalni upiti")
            pAlgorithm = new OrtogonalniUpiti(nullptr, nullptr, 0, "", i, RAZLOG_TESTIRANJE);
        if (_algorithmType == "Hertel-Mehlhorn")
            pAlgorithm = new HertelMehlhorn(nullptr, nullptr, 0, "", i);
        if(_algorithmType == "Triangulacija")
            pAlgorithm = new Triangulacija2(nullptr, nullptr, 0, "", i, false /*naivni algoritam ce biti pozvan direktno*/);

        /*
         * nije podrzano
        else if (tipAlgoritma == "Demonstracija iscrtavanja" ||
            tipAlgoritma == "Brisuca prava" ||
            tipAlgoritma == "Konveksni omotac 3D" ||
            tipAlgoritma == "Preseci duzi" ||
            tipAlgoritma == "DCEL Demo" ||
            tipAlgoritma == "Triangulacija")
                break;
       */



        if(pAlgorithm)
        {
#ifndef SKIP_OPTIMAL
            begin = clock();
            pAlgorithm->pokreniAlgoritam();
            end = clock();
            optimalTime = double(end - begin) / CLOCKS_PER_SEC;
#else
            optimalTime = 0;
#endif

#ifndef SKIP_NAIVE
            begin = clock();
            pAlgorithm->pokreniNaivniAlgoritam();
            end = clock();
            naiveTime = double(end - begin) / CLOCKS_PER_SEC;
#else
            naiveTime = 0;
#endif
            std::cout << "Zavrsio oba poziva, stigao do crtanja" <<std::endl;
            emit updateChart(i, optimalTime, naiveTime);
            delete pAlgorithm;
            pAlgorithm = nullptr;
        }
    }
}
