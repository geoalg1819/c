///
/// Svaki algoritam koji cemo implementirati treba da:
/// 1. Nasledi AlgorithmBase (koji predstavlja apstrakciju koja vodi racuna o animaciji i iscrtavanju)
/// 2. Da implementira metod "pokreniAlgoritam()" u kom ce biti implementacija algoritma.
/// 3. Da implementira metod "crtajAlgoritam()" u kom ce biti implementirano iscrtavanje
///
/// Svaki put kada se promeni stanje algoritma (kada je potrebno promeniti crtez),
/// potrebno je pozvati metod AlgoritamBaza_updateCanvasAndBlock();
/// Ovo je zapravo makro, koji pozove metod updateCanvasAndBlock() i u slucaju da
/// detektuje da algoritam treba da se zaustavi (jedna bool promenljiva koja se
/// postavlja na true kada kosirnik klikne "Zaustavi"), samo se zamenjuje komandom
/// return;
///
/// Razlog za ovaj mehanizam je to sto se algoritam izvrsava u okviru svoje niti i nije bezbedno
/// (sa aspekta resursa) ubiti tu nit dok se ne zavrsi. Na ovaj nacin, svaki put
/// kada se vrsi azuriranje crteza, proverava se i stanje pomenute logicke promenljive
/// i ukoliko ona signalizira da treba da se zaustavimo, na mestu azuriranja crteza ce
/// se naci komanda return; i na taj nacin smo postigli efekat koji smo zeleli
/// (izaslo se iz funkcije koja se izvrsava u okviru niti za animaciju)

#ifndef ALGORITAMBAZA_H
#define ALGORITAMBAZA_H

#include <QObject>
#include <QSemaphore>
#include <QWidget>
#include <QOpenGLWidget>
#include <QPainter>

#define AlgoritamBaza_updateCanvasAndBlock() \
    if(!_pNit) \
    { \
       ; \
    } \
    else if (updateCanvasAndBlock()) \
    { \
        return; \
    }

enum RazlogPravljenjaAlg {
    RAZLOG_UCITAVANJE = 0,
    RAZLOG_NASUMICNE = 1,
    RAZLOG_ISPOCETKA = 2,
    RAZLOG_TESTIRANJE = 3,
};

class AnimacijaNit;

/* Nasledjuje se QObject koji omogucava rad sa signalima i slotovima.
 */
class AlgoritamBaza : public QObject
{
    Q_OBJECT
protected:
    static int constexpr DRAWING_BORDER = 10;
    static int constexpr BROJ_NASUMICNIH_TACAKA = 20;

private:
    static int constexpr INVALID_TIMER_ID = -1;

    /* Parametri za animaciju */
    int _pauzaKoraka;
    int _timerId;
    QSemaphore _semafor;
    bool _unistiAnimaciju;

    ///
    /// \brief timerEvent - funkcija koja se poziva na svakih _delayMs ms.
    ///     U njoj samo oslobadjamo semafor i na taj nacin omogucavamo da se predje na sledeci
    ///     korak algoritma
    /// \param event
    /// The QTimer class provides a high-level programming interface for timers. To use it, create a QTimer, connect its timeout() signal to the appropriate slots, and call start(). From then on, it will emit the timeout() signal at constant intervals.
    ///
    void timerEvent(QTimerEvent *event);

public:
    AlgoritamBaza(QWidget* pCrtanje, QOpenGLWidget *pCrtanjeGL, int pauzaKoraka);

    ///
    /// \brief pokreniAlgoritam - funkcija koju ce svaka podklasa implementirati,
    ///         a koja predstavlja izvrsavanje konkretnog algoritma
    ///
    virtual void pokreniAlgoritam() = 0;
    virtual void crtajAlgoritam(QPainter &painter) const = 0;
    virtual void crtajAlgoritam3D() const = 0;
    virtual void pokreniNaivniAlgoritam() = 0;
    virtual bool is_3D() const = 0;

    ///
    /// \brief pokreniAnimaciju - funkcija za pokretanje animacije
    ///     1. Pravi i pokrece nit za konkretan algoritam
    ///     2. Pokrece tajmer
    ///
    void pokreniAnimaciju();


    ///
    /// \brief pauzirajIliNastaviAnimaciju - pauziranja, odnosno ponovno pokretanje animacije
    ///         Ako je tajmer pokrenut, zaustavlja ga
    ///         Ako tajmer nije pokrenut, pokrece ga
    ///
    void pauzirajIliNastaviAnimaciju();

    ///
    /// \brief sledeciKorakAnimacije - izvrsavanje algoritma korak po korak
    ///     1. Zaustavljanje tajmera (da bi se zaustavilo kontinuirano izvrsavanje algoritma)
    ///     2. Oslobadjanje semafora (da bi se izvrsio jedan korak algoritma)
    ///
    void sledeciKorakAnimacije();

    ///
    /// \brief zaustaviAnimaciju - zaustavljanje animacije
    ///     1. Postavljanje logicke promenljive na true
    ///     2. Pustanje semafora (da bi se animacija "nastavila", tj. da bi se doslo do updateCanvasAndBlock())
    ///         2.1. U updateCanvasAndBlock ce se detektovati da je logicka promenljiva true i algoritam ce se zaustaviti
    ///     3. Oslobadjanje odgovarajucih resursa
    ///
    void zaustaviAnimaciju();

    ///
    /// \brief promeniDuzinuPauze - funkcija za promenu brzine animacije
    /// \param duzinaPauze - pauza izmedju dva koraka, izrazena u ms
    ///
    void promeniDuzinuPauze(int duzinaPauze);

signals:
    void animacijaZavrsila();

protected:
    /* Nit koja izvrsava algoritam */
    AnimacijaNit *_pNit;

    ///
    /// \brief _pCrtanje - oblast crtanja
    ///
    QWidget *_pCrtanje;
    QOpenGLWidget *_pCrtanjeGL;

    ///
    /// \brief updateCanvasAndBlock - azuriranje crteza i blokiranje dok se semafor ne oslobodi
    ///     1. Azuriranje crteza iz renderarea klase
    ///     2. Blokiranje semafora (semafor se oslobadja u tajmeru, tako da se na taj nacin
    ///         simulira animacija)
    /// \return true ako animacija treba da se zaustavi, false u suprotnom
    ///
    bool updateCanvasAndBlock();


    std::vector<QPoint> generisiNasumicneTacke(int broj_tacaka = BROJ_NASUMICNIH_TACAKA);
    std::vector<QPoint> ucitajPodatkeIzDatoteke(std::string imeDatoteke);
};

#endif // ALGORITAMBAZA_H
