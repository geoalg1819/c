#-------------------------------------------------
#
# Project created by QtCreator 2018-09-17T20:31:16
#
#-------------------------------------------------

QT       += core gui opengl charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Geometrijski_Algoritmi
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    algoritambaza.cpp \
    oblastcrtanja.cpp \
    animacijanit.cpp \
    algoritmi_sa_vezbi/ga01_brisucaprava.cpp \
    algoritmi_sa_vezbi/ga00_demoiscrtavanja.cpp \
    algoritmi_sa_vezbi/ga02_konveksniomotac.cpp \
    pomocnefunkcije.cpp \
    oblastcrtanjaopengl.cpp \
    algoritmi_sa_vezbi/ga03_konveksniomotac3d.cpp \
    algoritmi_sa_vezbi/ga04_preseciduzi.cpp \
    algoritmi_sa_vezbi/ga05_dcel.cpp \
    algoritmi_sa_vezbi/ga05_dceldemo.cpp \
    algoritmi_sa_vezbi/ga06_triangulacija.cpp \
    timemeasurementthread.cpp \
    algoritmi_sa_vezbi/ga07_voronojidijagram.cpp\
    algoritmi_studentski_projekti/ga00_prazanprojekat.cpp \
    algoritmi_studentski_projekti/ga01_triangulacija.cpp \
    algoritmi_studentski_projekti/ga02_unija_pravougaonika.cpp \
    algoritmi_studentski_projekti/ga10_trilateracija.cpp \
    algoritmi_studentski_projekti/ga04_kdtree.cpp \
    algoritmi_studentski_projekti/ga07_ortogonalni_upiti.cpp \
    algoritmi_studentski_projekti/ga07_rangetree.cpp \
    algoritmi_studentski_projekti/ga08_hertelmehlhorn.cpp \
    algoritmi_studentski_projekti/ga06_kolinearneTacke.cpp

HEADERS += \
        mainwindow.h \
    algoritambaza.h \
    oblastcrtanja.h \
    animacijanit.h \
    algoritmi_sa_vezbi/ga01_brisucaprava.h \
    algoritmi_sa_vezbi/ga00_demoiscrtavanja.h \
    algoritmi_sa_vezbi/ga02_konveksniomotac.h \
    pomocnefunkcije.h \
    oblastcrtanjaopengl.h \
    algoritmi_sa_vezbi/ga03_konveksniomotac3d.h \
    algoritmi_sa_vezbi/ga03_datastructures.h \
    algoritmi_sa_vezbi/ga04_datastructures.h \
    algoritmi_sa_vezbi/ga04_preseciduzi.h \
    algoritmi_sa_vezbi/ga05_dcel.h \
    algoritmi_sa_vezbi/ga05_dceldemo.h \
    algoritmi_sa_vezbi/ga06_triangulacija.h \
    algoritmi_sa_vezbi/ga06_datastructures.h \
    timemeasurementthread.h \
    config.h \
    algoritmi_sa_vezbi/ga07_voronojidijagram.h\
    algoritmi_studentski_projekti/ga00_prazanprojekat.h \
    algoritmi_studentski_projekti/ga01_triangulacija.h \
    algoritmi_studentski_projekti/ga02_unija_pravougaonika.h \
    algoritmi_studentski_projekti/ga10_trilateracija.h \
    algoritmi_studentski_projekti/ga04_kdtree.h \
    algoritmi_studentski_projekti/ga07_ortogonalni_upiti.h \
    algoritmi_studentski_projekti/ga07_rangetree.h \
    algoritmi_studentski_projekti/ga08_hertelmehlhorn.h \
    algoritmi_studentski_projekti/ga06_kolinearneTacke.h

FORMS += \
        mainwindow.ui

LIBS += -lglut -lGLU

CONFIG += c++14

DISTFILES += \
    algoritmi_sa_vezbi/triangulation_input \
    algoritmi_sa_vezbi/triangulation_input2.txt \
    input_files/ga04_kdtree_input1.txt \
    input_files/ga04_kdtree_input2.txt \
    input_files/ga04_kdtree_input3.txt
