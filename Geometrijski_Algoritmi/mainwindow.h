#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "algoritambaza.h"
#include "oblastcrtanja.h"
#include "oblastcrtanjaopengl.h"

#include "algoritmi_sa_vezbi/ga00_demoiscrtavanja.h"
#include "algoritmi_sa_vezbi/ga01_brisucaprava.h"
#include "algoritmi_sa_vezbi/ga02_konveksniomotac.h"
#include "algoritmi_sa_vezbi/ga03_konveksniomotac3d.h"
#include "algoritmi_sa_vezbi/ga04_preseciduzi.h"
#include "algoritmi_sa_vezbi/ga05_dceldemo.h"
#include "algoritmi_sa_vezbi/ga06_triangulacija.h"
#include "algoritmi_sa_vezbi/ga07_voronojidijagram.h"

#include "timemeasurementthread.h"
#include "config.h"

/*QChart*/
#include <QtCharts/QLineSeries>
QT_CHARTS_USE_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_datoteka_dugme_clicked();

    void on_Nasumicni_dugme_clicked();

    void on_Ponisti_dugme_clicked();

    void on_Zapocni_dugme_clicked();

    void on_Zaustavi_dugme_clicked();

    void on_Sledeci_dugme_clicked();

    void on_Ispocetka_dugme_clicked();

    void on_tipAlgoritma_currentIndexChanged(int index);

    void on_merenjeButton_clicked();
    void on_lineSeriesChange(double dim, double optimal, double naive);

    void on_naivniCheck_stateChanged(int arg1);

public slots:
    void na_krajuAnimacije();


private:
    void animacijaButtonAktivni(bool param_aktivnosti);

    void animacijaParametriButtonAktivni(bool param_aktivnosti);

    void napraviNoviAlgoritam(RazlogPravljenjaAlg razlog);

private:
    Ui::MainWindow *ui;
    AlgoritamBaza *_pAlgoritamBaza;
    OblastCrtanja *_pOblastCrtanja;
    OblastCrtanjaOpenGL *_pOblastCrtanjaOpenGL;
    std::string _imeDatoteke;
    int _duzinaPauze;
    int _broj_nasumicnih_tacaka;
    bool _naivni;
    QLineSeries* _naiveSeries;
    QLineSeries* _optimalSeries;

    TimeMeasurementThread* _mThread;
};

#endif // MAINWINDOW_H
