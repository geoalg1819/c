\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{color}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{
  amsmath, % omoguci blok: \text{}
  amsthm,  % omoguci uklanjanje brojeva kod teorema
  amssymb  % podrska za dodatne simbole poput \square
}

\usepackage{graphicx}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=blue, urlcolor=blue}

\usepackage{color, colortbl}
\definecolor{PaleBlue}{rgb}{0.69,0.77,0.87}
\definecolor{Gray}{gray}{0.9}

\addto\captionsenglish{\renewcommand{\figurename}{Slika}}

\title{Stablo opsega 2D - Ortogonalni upiti\\{\large Geometrijski algoritmi}\vspace{3mm}}

\author{Nikola Katić\vspace{5mm}\\{\small Matematički fakultet}\\{\small Univerzitet u Beogradu}}

\date{februar 2018.}

\begin{document}
\maketitle

\section{Opis problema}
\label{sec:opis-problema}

Problem ortogonalne pretrage (eng.~{\em{orthogonal range queries}}), čije rešenje je implementirano ovim projektom, za dati skup tačaka efikasno pronalazi tačke koje pripadaju pravougaoniku na ulazu. Ovaj problem ima primene i u oblasti baza podataka. Ukoliko su podaci uređeni po određenim kriterijumima, filtriranje samo onih koji potpadaju u određen interval\footnote{Dekartov proizvod svih intervala specifičnih odabranim kriterijumima.} je problem koji se može apstrahovati kao pronalaženje tačaka u $\mathbb{R}^n$.

Formulacija za slučaj jedne dimenzije je: {\em{za dati skup realnih brojeva $S = \{p_1, p_2, ..., p_n\}$ i interval $I = [\mu, \mu']$, pronaći sve elemente koji pripadaju i jednom i drugom skupu}}.

Sortiranjem niza i vršenjem binarne pretrage, ili ekvivalentno, pravljenjem modifikovanog binarnog stabla pretrage moglo bi se dobiti rešenje koje nakon pretprocesiranja u vremenu $\mathcal{O}(n\log{n})$ pruža mogućnost upita u vremenu od $\mathcal{O}(\log{n + k})$, gde je $k$ broj tačaka koje potpadaju u interval.

Formulacija za ravanski slučaj je analogna, s tim što važi $S \subset \mathbb{R}^2$. Da bi se moglo pronaći asimptotski pogodno rešenje, koriste se naprednije strukture podataka. Stablo opsega (eng.~{\em{range tree}}) i {\em{kd-stablo}} su primeri takvih struktura. Obe pomenute strukture omogućavaju rad i u višedimenzionom prostoru, ali je stablo opsega struktura koja efikasnije rešava problem ortogonalnih pretraga.

\section{Naivni algoritam}
\label{sec:naivni-algoritam}

Naivni pristup pronalazi rešenje u linearnom vremenu\footnote{Preciznije u vremenu $\mathcal{O}(n\cdot d)$, gde je $d$ dimenzija.}, jednim prolaskom kroz ceo skup i direktnom proverom da li svaka od tačaka pripada zadatom intervalu. Ovaj algoritam se dobro ponaša, jer je za svaku tačku potrebno izvršiti $2d$ poređenja skalara. Tako mali konstantni faktor predstavlja dobru stranu ovog algoritma, a uz to, postoje i prednosti procesorskog keširanja.

\section{Naprednije tehnike}
\label{sec:naprednije-tehnike}

Prilikom opisa problema, već su date neke od mogućnosti pametnog rešavanja problema. Razmotrimo najpre rešenje jednodimenzionog problema korišćenjem modifikovanog binarnog stabla pretrage. Primer takvog stabla dat je na slici \ref{slika1}.

Ideja je da se u listovima čuvaju vrednosti konkretne tačke, a da se u svakom od unutrašnjih čvorova čuva maksimum levog podstabla. Na takvom stablu je moguće izvršiti efikasnu pretragu.

\subsection{Pojmovi}

Da bi se zašlo u detalje algoritma, uvode se naredni pojmovi:

\begin{itemize}
\item {\bf{Kanonički skup}} čvora $v$ -- skup svih listova podstabla sa korenom u $v$
\item {\bf{Čvor podele}} -- za dati interval, to je koren stabla sa najmanjim kanoničkim skupom takvim da sadrži sve tačke iz intervala
\end{itemize}

\begin{figure}
  \centering
  \includegraphics[scale=0.3]{1dstablo.png}
  \caption{Pretražuju se tačke iz skupa $S = \{$3,10, 19, 23, 30, 37, 59, 62, 70, 80, 100, 105$\}$ koje potpadaju u interval $I = [19, 80]$. Primer preuzet iz \cite{Berg}.}
  \label{slika1}
\end{figure}

\subsection{Postupak}

Najpre se počevši od korena stabla pronalazi čvor podele $v$. To je mesto u kojem se putevi ka krajevima intervala, tj. čvorovima sa vrednostima $\mu$ i $\mu'$, razdvajaju. U nastavku pratimo samo levi put, a analogno tom postupku se vrši postupak i po drugoj strani.

Kretanjem od čvora podele nadalje, proverava se da li je taj čvor list. Ukoliko je list, vrši se provera da li pripada intervalu, i prijavljuje se ukoliko pripada.

Ako trenutni čvor nije list, onda se ispituje da li je maksimum njegovog levog podstabla veći ili jednak od levog kraja intervala. Ukoliko je to istina, pretraga se rekurzivno nastavlja u levom detetu trenutnog čvora. Dodatno, pre skretanja ulevo iz svakog čvora koji nije čvor podele treba prijaviti kanonički skup desnog deteta.

Ukoliko trenutni čvor nije list i maksimum njegovog levog podstabla je manji od vrednost $\mu$, onda se pretraga rekurzivno nastavlja u desnom detetu.

Ovako opisani rekurzivan postupak naznačen je na slici \ref{slika1}. Iz unutrašnjeg čvora $23$ se tokom puta ka levom kraju intervala $\mu$ skreće ulevo, a pre toga se ceo kanonički skup čvora $37$ prijavljuje kao deo rešenja. Sada je trenutni čvor $10$ i pošto je $10 < 19$, pretraga se nastavlja u $19$. Kanonički skup lista $23$ (a to je on sam) se dodaje u rešenje i pretraga dolazi u list $19$ koji pripada intervalu i biva dodat.

\subsection{Dvodimenzioni slučaj}

Prethodno opisani postupak može se uopštiti tako što bi se u svakom unutrašnjem čvoru mogla čuvati zasebna stabla. Prvo stablo bi moglo da ima kriterijum pretraživanja po $x$-koordinati, a unutrašnja stabla po $y$-koordinati. Sekundarno stablo se kreira nad kanoničkim skupom čvora glavnog stabla.

Postupak pretrage je prirodno proširenje jednodimenzionog slučaja. U glavnom stablu se pretraga odvija na uobičajen način, razmatrajući samo $x$-koordinatu. Svaki put kada bi se došlo u situaciju da treba da se prijavi kanonički skup nekog čvora u jednodimenzionom algoritmu, ovde je neophodno umesto toga izvršiti pretragu po sekundarnom stablu i $y$-intervalu.

Sveukupno, memorijska složenost se sada povećava i iznosi $\mathcal{O}(n\log{n})$, a vremenska složenost je $\mathcal{O}(\log^2{n} + k)$

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{2dstablo.png}
  \caption{Dat je skup tačaka $S = \{$(2, 17), (4, 17), (9, 13), (12, 14), (23, 5), (25, 31), (30, 16), (33, 2)$\}$. Slika demonstrira višestrukost pojavljivanja tačke (12, 14). Primer preuzet iz \cite{MasterStabloOpsega}.}
  \label{slika2}
\end{figure}


\subsection{Unakrsno povezivanje}

Zadržavajući istu memorijsku složenost, moguće je poboljšati vremensku složenost za logaritamski faktor. Umesto da se po svakom čvoru glavnog stabla vrši pretraga sekundarnog stabla, moguće je čuvati sekundarni niz sastojan od kanoničkih čvorova sortiranih u neopadajućem poretku (po $y$-koordinati).

Dodatno se čuvaju i veze među elementima sekundarnog niza čvora $v$ sa sekundarnim nizovima dece čvora $v$. Svaki element sekundarnog niza čuva i pokazivač na prvi element iz sekundarnog niza deteta koji nije manji od njega (po $y$-koordinati). Ukoliko takav element ne postoji, čuva se $null$ pokazivač.

Ovakvo stablo se naziva {\em{slojevitim stablom}} opsega. Primer takvog stabla koji je dat na slici \ref{slika3} odgovara običnom 2D stablu opsega sa slike \ref{slika2}.

Veze među elementima sekundarnog niza omogućavaju da se ne vrše dodatne logaritmaske pretrage po sekundarnim strukturama unutrašnjih čvorova. Dovoljno je da se u čvoru podele binarnom pretragom pronađe početni element, a nakon toga pomoću pokazivača se ažurira pozicija u sekundarnom nizu sa $y$-intervalom od interesa.

Ukupna memorijska složenost se nije promenila u odnosu na obično 2D stablo pretrage, a vremenska složenost je smanjena na $\mathcal{O}(\log{n} + k)$.

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{slojevito_2dstablo.png}
  \caption{Pretražuju se tačke iz skupa $S = \{$(2, 17), (4, 17), (9, 13), (12, 14), (23, 5), (25, 31), (30, 16), (33, 2)$\}$ koje potpadaju u interval $I = [3, 28]\times[14, 17]$. Primer preuzet iz \cite{MasterStabloOpsega}.}
  \label{slika3}
\end{figure}

\subsection{Dodatni komentari}

Degenerisani slučaj strukture stabla opsega je postojanje dve tačke sa istom koordinatom. Samim tim je problematičan i slučaj kada postoje dve iste tačke. Ovaj problem se prevazilazi posmatranjem leksikografskog uređenja tačaka i neznatnom modifikacijom pretrage čvora podele i same pretrage. 

Leksikografsko uređenje podrazumeva da se u glavnom stablu ne vrši samo poređenje po $x$ koordinati, već i po $y$-koordinati ukoliko su $x$ koordinate jednake. U sekundarnom stablu se analogno prvo poredi po $y$-koordinati, pa tek u slučaju jednakosti i po $x$-koordinati.

Umesto traženja čvora podele, može se uzeti i njegov roditelj (ukoliko ga ima), čime se usled balansiranosti stabla ne narušava složenost postupka. Takođe, prilikom obilaska glavnog stabla i ispitivanja, u obzir se uzima i jednakost, čime se sprečava neispravnost algoritma u slučajevima kao sa slike \ref{slika4}.

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{duplikati.png}
  \caption{Pretražuju se tačke iz skupa $S = \{$(0, 10), (2, 40), (2, 30), (4, 10)$\}$ koje potpadaju u interval $I = [1, 2]\times[0, 50]$. Primer preuzet iz \cite{MasterStabloOpsega}.}
  \label{slika4}
\end{figure}

\subsection{Implementacioni detalji}

Implementirani kod pruža API u vidu {\em{RangeTree}} klase, a sva interakcija sa korisnikom i grafičkim korisničkim interfejsom je sadržana u klasi {\em{OrtogonalniUpiti}}. Detaljna dokumentacija u vidu {\em{Doxygen}} komentara data je za klasu {\em{RangeTree}} u samom kodu, a po potrebi, moguće je generisati {\em{HTML}} datoteku sa linkovima.

Prilikom izgradnje stabla, čvorovi se pohranjuju u niz i indeksirani su počevši od indeksa 1 -- koren stabla. Levi (tj. desni) sin čvora sa indeksom $k$ je čvor sa indeksom $2\cdot k$ (tj. $2\cdot k$+1). Listova ima najviše $n$ i to u slučaju kada je $n=2^c$ (za neko $c \in \mathbb{N}$). Informacija o konkretnoj tački sadržana je samo u listovima stabla.

Stablo se izgrađuje iterativno i to pristupom odozdo-nagore i sa desna nalevo. Svaka od metoda za rad nad stablom je takođe iterativna, zarad praktične efikasnosti.

\renewcommand{\arraystretch}{2}
\begin{table}[h!]
  \begin{tabular}{|l|l|l|l|}
    
    \hline
    \rowcolor{PaleBlue}
    Naziv testa & Opis testa & Ulaz & Očekivani izlaz\\
    \hline

    %% prazno_stablo_greska
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{prazno\_stablo\_}}\\ {\texttt{greska}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}      
      {@{}l@{}}Ako je zadat niz\\ tačaka prazan,\\dolazi do greške\\ prilikom kreiranja\\ stabla
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Prazan niz\\ tačaka
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Neuspešno\\ kreiranje\\ stabla
    \end{tabular} \\
    \hline

    %% jednoclano_stablo
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{jednoclano\_}}\\{\texttt{stablo}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Kreira se stablo\\ od datog čvora.\\ Zadat je i opseg\\ kome pripada ta\\ tačka
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}}Tačka (5, 5)\\ i opseg\\ $[-10, 10] \times$\\ $[-10, 10]$
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Jedna\\ prikupljena\\ tačka (5, 5)
    \end{tabular} \\
    \hline

    %% tesitraj_iste_koordinate
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{testiraj\_iste\_}}\\{\texttt{koordinate}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Rad sa tačkama\\ koje imaju istu\\ vrednost neke\\ koordinate
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}}Tačke (0, 10),\\ (2, 40), (2, 30),\\ (4, 10) i opseg\\ $[1, 2] \times [0, 50]$
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Tačke (2, 30)\\ i (2, 40)
    \end{tabular} \\
    \hline

    %% testiraj_duplikate
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{testiraj\_}}\\{\texttt{duplikate}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Zadavanje tačaka\\ među kojima ima\\ duplikata.\\U rezultatu će se\\ nalaziti duple\\ vrednosti
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Dve tačke\\ sa istim\\ koordinatama:\\(0, 0) i (0, 0),\\ i opseg\\$[0, 0] \times [0, 0]$
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Tačke (0, 0)\\ i (0, 0)
    \end{tabular} \\
    \hline

    %% cvor_podele
    {\texttt{cvor\_podele}}&
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Provera da li je\\ dobro izabran\\ čvor podele
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Tačke i opseg\\ se učitavaju\\ iz datoteke
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Indeks čvora\\ podele u\\ stablu je 12
    \end{tabular} \\
    \hline

    %% isto_resenje_naivnog_i_optimalnog_FAJL
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{isto\_resenje\_}}\\{\texttt{naivnog\_i\_}}\\{\texttt{optimalnog\_FAJL}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Upoređivanje\\ prikupljenih\\ tačaka od strane\\ optimalnog i\\ naivnog algoritma
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Tačke i opseg\\ se učitavaju iz\\ datoteke\\
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Isti broj\\ prikupljenih\\ tačaka,\\ i same tačke\\ su jednake
    \end{tabular} \\
    \hline

    %% isto_resenje_naivnog_i_optimalnog_RANDOM
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} {\texttt{isto\_resenje\_}}\\{\texttt{naivnog\_i\_}}\\{\texttt{optimalnog\_RANDOM}}
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Upoređivanje\\ prikupljenih\\ tačaka od strane\\ optimalnog i\\ naivnog algoritma
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Tačke i opseg\\ se biraju\\ nasumično
    \end{tabular} &
    \renewcommand{\arraystretch}{1.3}
    \begin{tabular}
      {@{}l@{}} Isti broj\\ prikupljenih\\ tačaka,\\ i same tačke\\ su jednake
    \end{tabular} \\
    \hline
    
  \end{tabular}
  \caption{Pregled testova ispravnosti \label{tab:testovi}.}
\end{table}
\renewcommand{\arraystretch}{1}

\section{Testiranje ispravnosti}
\label{sec:testiranje-ispravnosti}

Testiranje ispravnosti izvršeno je pomoću {\em{Google Test}} biblioteke. Implementirano je 6 jediničnih testova koji ispituju ispravnost upita izvršenih nad ulaznim podacima. Opis testova se može videti na tabeli \ref{tab:testovi}.

\section{Poređenje efikasnosti}
\label{sec:poredjenje-efikasnosti}

Merenje vremena izvršavanja i poređenje za naivni i napredni algoritam dato je na slikama \ref{slika5} i \ref{slika6}. Ulazna dimenzija je skalirana 5000 puta -- kako bi bolje ponašanje naprednog algoritma više došlo do izražaja. To znači da je najveća dimenzija ulaza sa slika \ref{slika5} i \ref{slika6} oko $4.5 \cdot 10^6$

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.35]{poredjenje1.png}
  \caption{Poređenje vremena izvršavanja optimalnog i naivnog algoritma.}
  \label{slika5}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.35]{poredjenje2.png}
  \caption{Poređenje vremena izvršavanja optimalnog i naivnog algoritma.}
  \label{slika6}
\end{figure}

\newpage \phantomsection % dodaj novi "anchor" za ispravno referenciranje

Efikasnost optimalnog algoritma dolazi do izražaja kada je broj tačaka veoma veliki. Na prosečnom računaru bi taj broj bio veći od $10^8$ tačaka. Razlog tome je veliki konstantni faktor koji igra ulogu u složenosti, ali najpre veličina $k$ (broj rešenja) koji može da bude blizu linearnog, a u tom slučaju naivni algoritam ima prednost manjeg konstantnog faktora. Prilikom testiranja efikasnosti, za nasumični opseg su odabrane (nasumično) dve tačke iz skupa koji se razmatra.

\section{Korisnički interfejs}
\label{sec:korisnicki_interfejs}

Korisnički interfejs će biti opisan na primeru rada sa nasumično odabranim tačkama. Potrebno je prvo iz padajuće liste izabrati ,,{\em{Ortogonalni upiti}}'' i u polje za broj nasumičnih tačaka uneti neku vrednost, ili je izostaviti, u tom slučaju će broj nasumično izabranih tačaka biti 20. Nakon pritiska na dugme ,,{\em{Odaberi nasumicne}}'' generiše se skup tačaka, a neke dve među njima se (nasumično) biraju za granice intervala. Klikom na dugme ,,{\em{Zapocni}}'' izvršava se upit za taj interval nad generisanim skupom tačaka. Interval nad kojim je izvršen upit se iscrtava u vidu jednoznačno određenog pravougaonika.

Korisnik može uneti interval na dodatna dva načina. Jedan način zadavanja intervala je obeležavanjem pravougaonika pomoću kursora, prevlačenjem preko površine za iscrtavanje, kao što je prikazano na slici \ref{slika7}. Trenutne koordinate kursora se ispisuju u donjem desnom uglu, a interval koji obeleženi pravougaonik obuhvata može se videti dole levo, pod naslovom ,,{\em{upit nad pravougaonim opsegom}}'' -- u prvom redu se nalazi interval $x$ koordinate, a u drugom interval $y$ koordinate.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.34]{upotreba1}
  \caption{Primer obeležavanja intervala za pretragu u vidu pravougaonika.}
  \label{slika7}
\end{figure}

\noindent Kada korisnik klikne na dugme ,,{\em{Ispocetka}}'' izvršava se upit nad istim tačkama, ali sada za interval koji je predstavljen obeleženim pravougaonikom, što se vidi na slici \ref{slika8}. Plavom bojom su naznačene tačke koje pripadaju zadatom intervalu.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.34]{upotreba2}
  \caption{Prikaz izvršenog upita za obeleženi interval.}
  \label{slika8}
\end{figure}

\noindent Granice intervala se mogu zadati i unosom vrednosti u polja koja se nalaze u donjem levom delu prozora, ispod naslova ,,{\em{upit nad pravougaonim opsegom}}''. Na slici \ref{slika9} je prikazana izmena intervala (630, 207) u interval (6300, 207) promenom vrednosti u tekstualnom polju za granicu $x$ koordinate. Pritiskom na dugme ,,{\em{Ispocetka}}'' ažurira se veličina pravougaonika određenog zadatim tačkama i izvršava upit nad intervalom koji taj pravougaonik predstavlja. Onemogućen je unos nevalidnih vrednosti (npr. slova) u tekstualna polja.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.34]{upotreba3}
  \caption{Menjanje opsega unosom konkretnih vrednosti za granice opsega, tj. koordinata tačaka koje određuju pravougaonik.}
  \label{slika9}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.34]{upotreba4}
  \caption{Ažuriranje pravougaonika na osnovu vrednosti učitanih iz tekstualnih polja i prikaz rešenja upita nad novim intervalom.}
  \label{slika9}
\end{figure}

\subsection{Učitavanje iz datoteke}

Ulazne podatke je moguće učitati i iz datoteke, koja se zadaje pritiskom na dugme ,,Ucitaj iz datoteke''. U prvom redu datoteke se mora nalaziti broj tačaka, a u narednim redovima koordinate tačaka, po jedna tačka u redu. Koordinate se zapisuju u formatu $X ~ Y$. Nakon spiska tačaka, opciono se može zadati opseg koji se ispituje. To se radi tako što se u redu ispod spiska tačaka navede reč ,,opseg'', a u dva naredna reda tačke koje jednoznačno definišu pravougaonik, tj. interval koji se ispituje. Ukoliko opseg nije zadat u datoteci. Primer validne datoteke:

\begin{verbatim}
  3
  1 1
  2 3
  4 4
  opseg
  3 10
  4 10
\end{verbatim}

%% \vspace{30em}

\renewcommand\refname{Literatura}
\begin{thebibliography}{9}
\bibitem{ORourke}
  J. O'Rourke, {\em{Computational Geometry in C}}, Cambridge University Press (1998)

\bibitem{Berg}
  M. de Berg, O. Cheong, M. van Kreveld and M. Overmars,
  {\em{Computational Geometry: Algorithms and Applications}}, 3rd edition, Springer (2008)

\bibitem{MasterStabloOpsega}
  D. Slijepčević, {\em{Stablo opsega i primene}}, master rad, Matematički fakultet (2017)

\bibitem{CGLecture8}
  M. van Kreveld, {\em{Computational Geometry -- Lecture on Range Trees}}, online at \url{https://www.cise.ufl.edu/class/cot5520sp18/CG_RangeTrees.pdf}, University of Florida
\end{thebibliography}
\end{document}
