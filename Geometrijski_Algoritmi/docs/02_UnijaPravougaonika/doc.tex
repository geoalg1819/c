% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[utf8]{inputenc} 
\usepackage{graphicx}
\usepackage[english,serbian]{babel}
\usepackage[unicode]{hyperref}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage[graphicx]{realboxes}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{chngcntr}
\usepackage{algorithm}
\usepackage{algpseudocode}
\counterwithin{figure}{section}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}


\theoremstyle{plain}
\newtheorem{thm}{Teorema}[section] 
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definicija} 
\newtheorem{lem}[thm]{Tvrdjenje}
\newtheorem{exmp}[thm]{Primer}


\begin{document}

\title{Ra\v{c}unanje povr\v{s}ine unije N pravougaonika u ravni sa stranicama paralelnim osama\\ \small{Projekat u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{\href{mailto:mi14042@matf.bg.ac.rs}{Ivan Ristovi\'c}}
\date{decembar 2018.}

\maketitle

\tableofcontents

\newpage


\section{Opis problema}
\label{sec:Uvod}

Dat skup pravougaonika $S$ u ravni sa stranicama paralelnim sa koordinatnim osama. Preciznije, za svaki pravougaonik $p \in S$ sa temenima $A(x1,y1)$, $B(x2,y1)$, $C(x1,y2)$ i $D(x2,y2)$ va\v{z}i:
$$x1 < x2 \wedge y1 < y2 \wedge AB \parallel Ox \wedge CD \parallel Ox \wedge AD \parallel Oy \wedge BC \parallel Oy$$
Cilj je prona\'c{}i povr\v{s}inu njihove unije - $P(S)$, tj: $$P(S) = \bigcup\limits_{p \in S} P(p) = \bigcup\limits_{p \in S} |AB| \cdot |CD|$$

Ovaj problem predstavlja uop\v{s}tenje problema nala\v{z}enja du\v{z}ine unije intervala u $\mathbb{R}$, i sli\v{c}no se mo\v{z}e uop\v{s}titi za vi\v{s}edimenzionalne prostore \footnote{U $\mathbb{R}^{3}$ bi problem bio nala\v{z}enje zapremine unije $n$ kocki.}.


\section{Naivni algoritam}
\label{sec:Naivni}

Naivni algoritam se mo\v{z}e konstruisati svodjenjem na jednodimenzioni problem. Pretpostavimo da ve\'c{} imamo algoritam koji re\v{s}ava problem nala\v{z}enja du\v{z}ine unije intervala. Konstrui\v{s}imo algoritam zasnovan na bri\v{s}u\'c{}oj pravoj. Mesta dogadjaja su stranice pravougaonika paralelne sa bri\v{s}u\'c{}om pravom. Kad naidjemo na pravougaonik ubacujemo njegovu stranicu paralelnu sa bri\v{s}u\'c{}om pravom u status \footnote{Struktura podataka koja podr\v{z}ava efikasno ubacivanje i izbacivanje du\v{z}i.}, a kada izadjemo iz pravougaonika izbacujemo odgovaraju\'c{}u stranicu iz statusa. Pomeramo bri\v{s}ucu pravu bez umanjenja op\v{s}tosti po $y$-osi. Izmedju svakog pomeraja ra\v{c}unamo du\v{z}inu unije stranica u statusu, zatim dobijenu du\v{z}inu mno\v{z}imo sa du\v{z}inom pomeraja bri\v{s}u\'c{}e prave i to dodajemo na ukupnu povr\v{s}inu (videti sliku \ref{fig2}).

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.45]{resources/fig2.PNG}
    \caption{Prikaz implementacije algoritma zasnovanog na bri\v{s}u\'c{}oj pravoj.}
    \label{fig2}
\end{figure}

\begin{algorithm}
    \caption{UnijaPravougaonika}\label{UnijaPravougaonikaPseudo}
    \begin{algorithmic}[1]
    \Procedure{UnijaPravougaonika}{}
        \State $P \gets 0$
        \State $ypos \gets 0$ \Comment{Pozicija bri\v{s}u\'c{}e prave}
        \While{lista dogadjaja nije prazna}
            \State $s \gets $ izbaciti narednu stranicu iz liste dogadjaja 
            \State $P \gets P + (s.y - ypos)$ $\cdot$ \Call{UnijaIntervala}{status}
            \State $P \gets s.y$

            \If {$s$ je donja stranica pravougaonika}
                \State \Call{Dodaj}{status, s}
            \Else
                \State \Call{Izbaci}{status, s}
            \EndIf
        \EndWhile
        \State \textbf{return} $P$
    \EndProcedure
    \end{algorithmic}
\end{algorithm}

Pseudokod algoritma se mo\v{z}e videti na slici \ref{UnijaPravougaonikaPseudo}. Ukoliko je slo\v{z}enost algoritma koji re\v{s}ava jednodimenzionu verziju problema $O(n\log{}n)$, onda je ukupna vremenska slo\v{z}enost $O(n^{2}\log{}n)$ \footnote{Ova slo\v{z}enost se mo\v{z}e pobolj\v{s}ati do $O(n^{2})$ tako \v{s}to se odr\v{z}ava sortiranost statusa pri ubacivanju i brisanju.}. Prostorna slo\v{z}enost je $O(n)$. Ostaje samo da se pojasni algoritam za jednodimenzionu verziju problema -pseudokod  se mo\v{z}e videti na slici \ref{UnijaIntervalaPseudo}.

\begin{algorithm}
    \caption{UnijaIntervala}\label{UnijaIntervalaPseudo}
    \begin{algorithmic}[1]
    \Procedure{UnijaIntervala}{status}
        \State Sortirati du\v{z}i iz statusa po njihovim levim krajevima
        \State $U \gets 0$
        \State $xl \gets $ levi kraj prve du\v{z}i iz statusa
        \State $maxr \gets 0$
        \For{du\v{z} $d \in status$} 
            \State $x \gets$ levi kraj $d$
            \State $l \gets$ du\v{z}ina $d$
            \If {$x < maxr$}
                \State $U \gets U + (x - xl)$
            \Else
                \State $U \gets U + (maxr - xl)$
            \EndIf
            \State $xl \gets x$
            \If {$x + l > maxr$}
                \State $maxr \gets x + l$
            \EndIf
        \EndFor
            
        \State $U \gets U + (maxr - xl)$
            
        \State \textbf{return} $U$
    \EndProcedure
    \end{algorithmic}
\end{algorithm}


\subsection{Detalji implementacije}
\label{subsec:ImplementacijaNaivni}

Status se mo\v{z}e implementirati kao balansirano binarno stablo uredjeno po levim krajevima du\v{z}i (ukoliko su levi krajevi jednaki, onda se sortira po desnim krajevima). Red dogadjaja takodje mo\v{z}e biti balansirano binarno stablo uredjeno po $y$ koordinatama stranica. Vizuelni prikaz algoritma se mo\v{z}e videti na slici \ref{fig2}.


\section{Efikasni algoritam}
\label{sec:Efikasni}

Efikasni algoritam predstavlja unapredjenje naivnog algoritma opisanog u delu \ref{sec:Naivni}. Umesto da se svaki put iznova ra\v{c}una du\v{z}ina unije intervala du\v{z}i, bilo bi lepo ako bismo mogli jeftino da \emph{a\v{z}uriramo} trenutnu du\v{z}inu unije. Zaista, pri svakom pomeraju bri\v{s}u\'c{}e prave mo\v{z}e se desiti jedan od narednih dogadjaja:
\begin{itemize}
    \item Du\v{z} je dodata u status
    \item Du\v{z} je izba\v{c}ena iz statusa
\end{itemize}

Ukoliko bi status u sebi dr\v{z}ao informaciju o du\v{z}ini trenutne unije, ne bismo je morali iznova ra\v{c}unati. Takav unapredjeni status bi morao podr\v{z}avati slede\'c{}e operacije u odgovaraju\'c{}im vremenskim ograni\v{c}enjima:
\begin{itemize}
    \item Ubacivanje nove du\v{z}i u vremenu $O(\log{}n)$
    \item Izbacivanje postoje\'c{}e du\v{z}i u vremenu $O(\log{}n)$
    \item Upit o du\v{z}ini unije svih du\v{z}i u statusu u vremenu $O(1)$
\end{itemize}

Ukoliko pretpostavimo da nam je ovakva struktura podataka na raspolaganju, mo\v{z}emo da trivijalno formiramo efikasni algoritam (videti sliku \ref{EfikasnaUnijaPravougaonikaPseudo}) ukupne vremenske slo\v{z}enosti $O(n\log{}n)$ i prostorne slo\v{z}enosti $O(n)$. Ostaje samo pitanje: \emph{Kako napraviti ovakvu strukturu podataka?}.

\begin{algorithm}
    \caption{EfikasnaUnijaPravougaonika}\label{EfikasnaUnijaPravougaonikaPseudo}
    \begin{algorithmic}[1]
    \Procedure{EfikasnaUnijaPravougaonika}{}
        \State $P \gets 0$
        \State $ypos \gets 0$ \Comment{Pozicija bri\v{s}u\'c{}e prave}
        \While{lista dogadjaja nije prazna}
            \State $s \gets $ izbaciti narednu stranicu iz liste dogadjaja 
            \State $P \gets P + (s.y - ypos)$ $\cdot$ status.DuzinaUnije
            \If {$s$ je donja stranica pravougaonika}
                \State \Call{Dodaj}{status, s}
            \Else
                \State \Call{Izbaci}{status, s}
            \EndIf
        \EndWhile
        \State \textbf{return} $P$
    \EndProcedure
    \end{algorithmic}
\end{algorithm}


\subsection{Implementacija efikasnog statusa}
\label{subsec:ImplementacijaEfikasni}

Status \'c{}emo dobiti tako \v{s}to \'c{}emo malo izmeniti \emph{binarna stabla pretrage} nad krajevima intervala du\v{z}i. Svaki \v{c}vor $u$ u stablu $T$ \'c{}e imati 10 polja (videti sliku \ref{fig3}):
\begin{itemize}
    \item \texttt{value} - klju\v{c} po kom se porede \v{c}vorovi, predstavlja vrednost levog ili desnog kraja du\v{z}i,
    \item \texttt{left} - pokaziva\v{c} na levog sina,
    \item \texttt{right} - pokaziva\v{c} na desnog sina,
    \item \texttt{isLeft} - indikator da li \v{c}vor odgovara levom ili desnom kraju du\v{z}i,
    \item \texttt{other} - suprotni kraj du\v{z}i,
    \item \texttt{min} - minimalni \v{c}vor u podstablu $T(u)$ po vrednosti polja \texttt{value},
    \item \texttt{max} - maksimalni \v{c}vor u podstablu $T(u)$ po vrednosti polja \texttt{value},
    \item \texttt{leftmin} - vrednost najmanjeg levog kraja du\v{z}i u podstablu $T(u)$, ova vrednost ne mora biti u $T(u)$,
    \item \texttt{rightmax} - vrednost najve\'c{}eg desnog kraja du\v{z}i u podstablu $T(u)$, ova vrednost ne mora biti u $T(u)$,
    \item \texttt{measure} - du\v{z}ina unije svih du\v{z}i u podstablu $T(u)$ ali ograni\v{c}ena intervalom $[min, max]$.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{resources/fig3.PNG}
    \caption{Vizuelizacija efikasnog statusa.}
    \label{fig3}
\end{figure}

S obzirom da je dodavanje du\v{z}i zapravo dodavanje dva njena kraja u $T$, a upit o uniji zapravo vrednost polja \texttt{measure} korena stabla $T$, uslovi o slo\v{z}enosti su ispunjeni akko se ubacivanje i brisanje du\v{z}i (\v{s}to uklju\v{c}uje a\v{z}uriranje polja \v{c}vorova pod uticajem) mo\v{z}e obaviti u $O(\log{}n)$.

\begin{lem}
    Neka je $T$ efikasni status opisan iznad. Tada se polja \texttt{min}, \texttt{max}, \texttt{leftmin}, \texttt{rightmax} i \texttt{measure} proizvoljnog \v{c}vora $u \in T$ mogu izra\v{c}unati na osnovu ostalih polja od $u$ i polja sinova od $u$.
\end{lem}

Polja \texttt{min}, \texttt{max}, \texttt{leftmin} i \texttt{rightmax} se mogu trivijalno izra\v{c}unati, naime:
$$
\mathtt{u.min} = \left\{
\begin{array}{ll}
    \mathtt{u.value}, & \mathtt{u.left = nil} \\
    \mathtt{u.left.min}, & \mathtt{u.left \neq nil} \\
\end{array} 
\right. 
$$
$$
\mathtt{u.leftmin} = \left\{
\begin{array}{ll}
      \mathtt{min(u.value, u.left.leftmin, u.right.leftmin)}, & \mathtt{u.isLeft = true} \\
      \mathtt{min(u.other, u.left.leftmin, u.right.leftmin)}, & \mathtt{u.isLeft = false} \\
\end{array} 
\right. 
$$

Ako je $\mathtt{u.left = nil}$ ili $\mathtt{u.right = nil}$, onda se termovi koji ih sadr\v{z}e izostavljaju. Sli\v{c}no se ra\v{c}unaju polja \texttt{max} i \texttt{leftmax}.

Polje \texttt{measure} je komplikovanije, potrebno je razmatrati nekoliko slu\v{c}ajeva. Pretpostavimo da je \texttt{u.left = true}, ostali slu\v{c}ajevi su simetri\v{c}ni. Potrebno je ispitati naredna tri uslova (videti sliku \ref{fig4}):
\begin{enumerate}
    \item $(\mathtt{u.right.leftmin = u.right.min} \vee \mathtt{u.right.leftmin = u.value})$ $\wedge$\\ $\mathtt{(u.left.rightmax = u.left.max)}$ \\
        U ovom slu\v{c}aju doprinos ukupnoj uniji dolazi samo od $u$. Pritom je potrebno videti sa koje strane je \texttt{u.other} i shodno tome dodati odredjeni dodatak na ukupnu uniju.
        $$
        \mathtt{u.measure} = \left\{
        \begin{array}{l}
              \mathtt{u.left.measure + u.right.measure + u.right.min - u.value}\\ u.other \in T(u.right) \\
              \mathtt{u.left.measure + u.right.max - u.value}\\ u.other \notin T(u.right) \\
        \end{array} 
        \right. 
        $$
    \item $\mathtt{(u.left.rightmax \neq u.left.max)}$ \\
        U ovom slu\v{c}aju cela du\v{z}ina izmedju sinova je pokrivena.
        $$
        \mathtt{u.measure} = \left\{
        \begin{array}{l}
              \mathtt{u.left.measure + u.right.measure + u.right.min - u.left.max}\\ u.other \in T(u.right) \\
              \mathtt{u.left.measure + u.right.max - u.left.max}\\ u.other \notin T(u.right) \\
        \end{array} 
        \right. 
        $$
    \item $(\mathtt{u.right.leftmin \neq u.right.min} \wedge \mathtt{u.right.leftmin \neq u.value})$ $\wedge$\\ $\mathtt{(u.left.rightmax = u.left.max)}$ \\
        U ovom slu\v{c}aju cela du\v{z}ina izmedju sinova je pokrivena.
        $$
        \mathtt{u.measure} = \left\{
        \begin{array}{l}
              \mathtt{u.left.measure + u.right.measure + u.right.min - u.value}\\ u.other \in T(u.right) \\
              \mathtt{u.left.measure + u.right.min - u.left.min}\\ u.other \notin T(u.right) \\
        \end{array} 
        \right. 
        $$
\end{enumerate}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{resources/fig4.PNG}
    \caption{Slu\v{c}ajevi koji se razmatraju pri ra\v{c}unanju mere za \v{c}vor.}
    \label{fig4}
\end{figure}


\section{Poredjenje efikasnosti}
\label{sec:Poredjenje}

\begin{table}[H]
    \begin{tabular}{r|r|r}
    Veličina ulaza & Naivni algoritam (s) & Efikasni algoritam (s) \\
    \hline
    10             & 0.000018 & 0.000017 \\
    100            & 0.001233 & 0.000190 \\
    1000           & 0.174560 & 0.007568 \\
    2000           & 0.671577 & 0.009511 \\
    3500           & 2.344150 & 0.017754 \\
    5000           & 4.626300 & 0.028254 \\
    7500           & 10.646900 & 0.043587 \\
    10000          & 21.231700 & 0.060344
\end{tabular}
\label{tbl:Poredjenje}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{resources/fig5.PNG}
    \label{fig5}
\end{figure}


\section{Testiranje ispravnosti}
\label{sec:Testovi}

Testiranje ispravnosti je sprovedeno kroz testove jedinica koda uz specifi\v{c}ne predodredjene test primere:
\begin{itemize}
    \item \emph{file1Test} - Testiranje dodavanja i izbacivanja iz statusa
    \begin{figure}[H]
        \centering
        \includegraphics[scale=0.35]{resources/fig10.PNG}
    \label{fig10}
    \end{figure}
    \item \emph{file2Test} - Testiranje izbacivanja frontalnog \v{c}vora iz statusa
    \begin{figure}[H]
    \centering
    \includegraphics[scale=0.35]{resources/fig11.PNG}
    \label{fig11}
    \end{figure}
    \item \emph{file3Test} - Testiranje svih slu\v{c}ajeva u \ref{sec:Efikasni}
    \begin{figure}[H]
    \centering
    \includegraphics[scale=0.35]{resources/fig12.PNG}
    \label{fig12}
    \end{figure}
    \item \emph{randomTest5} - Testiranje ispravnosti za slu\v{c}ajni ulaz veli\v{c}ine 5
    \item \emph{randomTest100} - Testiranje ispravnosti za slu\v{c}ajni ulaz veli\v{c}ine 100
\end{itemize}

U svim navedenim testovima se razmatraju izlazi naivnog i efikasnog algoritma i o\v{c}ekuje se jednakost dobijenih povr\v{s}ina. 

%\appendix
%\section{Dodatak}


\end{document}
