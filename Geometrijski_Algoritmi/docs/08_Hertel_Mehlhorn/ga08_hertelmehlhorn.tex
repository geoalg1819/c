\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{float}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=blue}
%filecolor=green,linkcolor=blue,urlcolor=blue

\usepackage{color, colortbl}
\definecolor{PalePink}{rgb}{0.96,0.79,0.84}
\definecolor{Gray}{gray}{0.9}

\newtheorem{teorema}{Teorema}[section]

\title{Hertel-Mehlhorn algoritam \\za konveksno particionisanje poligona\\{\large Geometrijski algoritmi}\vspace{3mm}}

\author{Irena Blagojević\vspace{5mm}\\{\small Matematički fakultet}\\{\small Univerzitet u Beogradu}}

\date{februar 2019.}

\begin{document}
\maketitle

\section{Opis problema}
\label{sec:Opis problema}

Rad sa konveksnim poligonima je lakši od rada sa kompleksnim oblicima, zato je često korisno prvo particionisati kompleksni oblik u konveksne delove \cite{ORourke}. Jedna primena konveksnog particionisanja je {\em{prepoznavanje karaktera}} -- vizuelno skenirani karakteri se mogu predstaviti kao poligoni i mogu se particionisati u konveksne delove. Rezultujuće strukture se mogu porediti sa bazom oblika da bi se identifikovao karakter \cite{FengPavlidis}.

Triangulacija se može posmatrati kao specijalan slučaj konveksnog particionisanja poligona. Pošto postoji optimalni algoritam za triangulaciju, onda postoji i optimalni algoritam za konveksno particionisanje. Postoji dva cilja konveksnog particionisanja, koji su suprotstavljeni:

\begin{itemize}
\item Particionisanje poligona na što manji broj konveksnih delova
\item Izvršavanje particionisanja što je brže moguće
\end{itemize}

Razlikuju se dva tipa particionisanja poligona: particionisanje prema {\em{dijagonalama}} i particionisanje prema {\em{segmentima}}. Razlikuju se po tome što particionisanje prema dijagonalama uzima u obzir samo čvorove poligona (tj. dijagonale moraju spajati dva čvora), dok kod particionisanja prema segmentima, krajevi segmenata moraju biti na rubu poligona (tj. ne moraju biti čvorovi poligona). {\em{Stefan Hertel}} i {\em{Kurt Mehlhorn}} koriste prvi pristup. Njihov algoritam je brz, ali neefikasan po pitanju broja konveksnih delova, čiji je broj ograničen u odnosu na optimalni broj particija \cite{ORourke}.

\subsection{Naivno rešenje}
\label{subsec:Naivno resenje}

Naivni pristup koji od triangulacije pravi konveksno particionisanje uparuje svaka dva poligona (na početku su to trouglovi iz triangulacije), i spaja ih u jedan poligon ukoliko je taj poligon konveksan. 

\subsection{Hertel-Mehlhorn algoritam}
\label{subsec:Hertel-Mehlhorn algoritam}

U konveksnom particionisanju poligona po dijagonalama, dijagonala $d$ se naziva {\em{esencijalnom za čvor $v$}} ukoliko brisanjem dijagonale $d$ nastaje deo koji je nekonveksan u $v$. Ako je $d$ esencijalna, ona mora biti susedna čvoru $v$ i $v$ mora biti refleksni\footnote{Za refleksni (ili konkavni) čvor važi da je unutrašnji ugao poligona u tom čvoru, tj. ugao koji zaklapaju stranice susedne refleksnom čvoru, manji od opruženog.} čvor. Dijagonala koja nije esencijalna ni za jedan svoj kraj naziva se {\em{neesencijalna}}.

Hertel-Mehlhorn algoritam se sastoji iz sledećih koraka:
\vspace{-2mm}
\begin{enumerate}
\item Početi od neke triangulacije poligona $P$ \vspace{-2mm}
\item Izbaciti neesencijalnu dijagonalu \vspace{-2mm}
\item Ponoviti prethodni korak
\end{enumerate}

\noindent Može se izvršiti u linearnom vremenu, ukoliko se koriste odgovarajuće pametne strukture podataka \cite{ORourke}. U implementaciji algoritma u ovom projektu korišćena je struktura podataka {\texttt{Poligon}} koja sadrži niz duži. Duži su predstavljene pomoću strukture {\texttt{Duz}}, koja sadrži dva temena koja čine tu duž i pokazivač na susedni poligon, koji nije poligon čija duž se posmatra. Time se dobija pristup susednom poligonu u konstantnom vremenu.

\begin{teorema}
  Hertel-Mehlhorn algoritam nikada nema četiri ili više puta konveksnih delova nego optimalno particionisanje.
\end{teorema}

\section{Poređenje efikasnosti}
\label{sec:Poredjenje efikasnosti}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.25]{poredjenje_efikasnosti}
  \caption{Poređenje vremena izvršavanja naivnog i naprednog algoritma.}
  \label{fig:kraj}
\end{figure}

\section{Prikaz rada algoritma}
\label{sec:Prikaz rada algoritma}

Nakon što se izabere {\texttt{Hertel-Mehlhorn}} algoritam iz padajućeg menija, može se iz datoteke učitati poligon i njegova triangulacija pritiskom na dugme {\texttt{Ucitaj iz datoteke}}. Pritiskom na naredbu {\texttt{Zapocni}} u oblasti za iscrtavanje prikazuje se polazna triangulacija i počinje se sa particionisanjem. Primer particionisanja na početku i na kraju izvršavanja algoritma može se videti na slikama \ref{fig:pocetak} i \ref{fig:kraj}. Moguće je zaustaviti proces particionisanja pritiskom na {\texttt{Zaustavi/Nastavi}} i pratiti dalje particionisanje korak po korak pomoću dugmeta {\texttt{Sledeci}} ili ponovnim pritiskom na {\texttt{Zaustavi/Nastavi}} nastaviti izvršavanje algoritma.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.25]{particionisanje_pocetak}
  \caption{Izgled particionisanja poligona učitanog iz datoteke {\texttt{08\_hertelmehlhorn\_1.off}} pre izvršenog algoritma, tj. triangulacija poligona.}
  \label{fig:pocetak}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.25]{particionisanje_kraj}
  \caption{Izgled particionisanja poligona učitanog iz datoteke {\texttt{08\_hertelmehlhorn\_1.off}} nakon izvršenog algoritma.}
  \label{fig:kraj}
\end{figure}

Moguće je i generisanje nasumičnog prostog poligona. Potrebno je uneti broj nasumičnih tačaka u naznačeno polje, ukoliko se ne zada broj, podrazumevana vrednost je 20. Pritiskom na dugme {\texttt{Odaberi nasumicne}} generiše se prost poligon i njegova triangulacija, a pritiskom na {\texttt{Zapocni}} izvršava se particionisanje. Primer ovako nastalog particionisanja, pre pokretanja i po završetku algoritma, može se videti na slikama \ref{fig:nasumicne_pocetak} i \ref{fig:nasumicne_kraj}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.25]{nasumicne_pocetak}
  \caption{Izgled particionisanja nasumično izabranog poligona sa 20 temena pre izvršenog algoritma, tj. triangulacija poligona.}
  \label{fig:nasumicne_pocetak}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.25]{nasumicne_kraj}
  \caption{Izgled particionisanja nasumično izabranog poligona sa 20 temena nakon izvršenog algoritma.}
  \label{fig:nasumicne_kraj}
\end{figure}

\section{Testiranje ispravnosti}
\label{sec:Testiranje ispravnosti}

Testovi ispravnosti se izvršavaju korišćenjem {\em{Google Test}} biblioteke. Implementirano je 5 jediničnih testova, čiji se opis može videti na tabeli \ref{tab:testovi_opis}. Ulazni podaci se čitaju iz datoteka ili se nasumično biraju temena, iz kojih se generiše prost poligon i njegova triangulacija.

\renewcommand{\arraystretch}{1.5}
\begin{table}[H]
  \begin{tabular}{llll}
    \rowcolor{PalePink}
    Naziv & Opis & Ulaz & Izlaz\\
    \hline
    {\bf{emptyPolygonTest}} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Prosleđuje se\\prazan skup tačaka\\i očekuje se da\\nema kreiranih\\particija\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Prazna\\triangulacija\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Prazan\\skup\\konveksnih\\particija\end{tabular} \\
    \hline
    {\bf{convexPolygonTest}} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Konstruišu se\\konveksne particije\\već konveksnog\\poligona\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Triangulacija\\konveksnog\\poligona\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Jedna\\konveksna\\particija\end{tabular} \\
    \hline
    {\bf{randomTest}} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Izvršava se\\particionisanje\\poligona sa\\nasumičnim\\temenima\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Triangulacija\\poligona sa\\nasumičnim\\temenima\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Dobijene\\particije\\su\\konveksne\end{tabular} \\
    \hline
    {\bf{fileTest}} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Izvršava se\\napredni algoritam\\za triangulaciju\\iz datoteke\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Triangulacija\\poligona iz\\datoteke\end{tabular} &
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{@{}l@{}}Broj\\particija\\je 8\end{tabular} \\
    \hline
  \end{tabular}
  \caption{Opis testova ispravnosti\label{tab:testovi_opis}.}
\end{table}
\renewcommand{\arraystretch}{1}

\vfill

\renewcommand\refname{Literatura}
\begin{thebibliography}{9}
\bibitem{ORourke}
  J. O'Rourke,
  Computational Geometry in C,
  Cambridge: Cambridge University Press,
  1998
\bibitem{FengPavlidis}
  H. Feng and T. Pavlidis,
  The generation of polygonal outlines of objects from gray level pictures,
  IEEE Transactions on Circuits and Systems,
  1975
\end{thebibliography}
\end{document}
