#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "algoritmi_studentski_projekti/ga01_triangulacija.h"
#include "algoritmi_studentski_projekti/ga02_unija_pravougaonika.h"
#include "algoritmi_studentski_projekti/ga08_hertelmehlhorn.h"
#include "algoritmi_studentski_projekti/ga10_trilateracija.h"
#include "algoritmi_studentski_projekti/ga04_kdtree.h"
#include "algoritmi_studentski_projekti/ga07_ortogonalni_upiti.h"
#include "algoritmi_studentski_projekti/ga06_kolinearneTacke.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _pAlgoritamBaza(nullptr),
    _imeDatoteke(""),
    _duzinaPauze(500),
    _broj_nasumicnih_tacaka(20),
    _naivni(false)
{
    ui->setupUi(this);

    ui->tipAlgoritma->insertSeparator(9);
    animacijaButtonAktivni(false);
    animacijaParametriButtonAktivni(true);

    _pOblastCrtanjaOpenGL = ui->openGLWidget;

    _pOblastCrtanja = new OblastCrtanja(this);
    QBoxLayout *oblastCrtanjaLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    oblastCrtanjaLayout->addWidget(_pOblastCrtanja);
    ui->tab->setLayout(oblastCrtanjaLayout);

    ui->lineEditX1->setValidator(new QIntValidator(this));
    ui->lineEditY1->setValidator(new QIntValidator(this));
    ui->lineEditX2->setValidator(new QIntValidator(this));
    ui->lineEditY2->setValidator(new QIntValidator(this));

    ui->brojNasumicniTacaka->setPlaceholderText("Uneti broj nasumicnih tacaka, podrazumevana vrednost je 20.");
    ui->nUdaljenih->setPlaceholderText("N najblizih tacaka, mora biti <= od broja nasumicnih tacaka ili broja zadataih tacaka");
    ui->pointX->setPlaceholderText("Zadataka tacka x koordinata");
    ui->pointY->setPlaceholderText("Zadataka tacka y koordinata");
    ui->nUdaljenih->setVisible(false);
    ui->pointX->setVisible(false);
    ui->pointY->setVisible(false);

    /* Add chart */
    _optimalSeries = new QLineSeries();
    _naiveSeries = new QLineSeries();

    QChart *chart = new QChart();
    _optimalSeries->append(0,0);
    _naiveSeries->append(0,0);

    _optimalSeries->setName("Optimalni algoritam");
    _naiveSeries->setName("Naivni algoritam");

    chart->addSeries(_optimalSeries);
    chart->addSeries(_naiveSeries);

    chart->legend()->show();

    chart->createDefaultAxes();
    chart->setTitle("Poredjenje efikasnosti");

    if(chart->axisX())
        chart->axisX()->setRange(0, X_MAX_VAL);

    if(chart->axisY())
        chart->axisY()->setRange(0, Y_MAX_VAL);

    // Same formatting
    chart->setBackgroundVisible(false);
    chart->setPlotAreaBackgroundVisible(true);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QBoxLayout* chartBoxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    chartBoxLayout->addWidget(chartView);

    ui->tab_2->setLayout(chartBoxLayout);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_datoteka_dugme_clicked()
{
    QString imeDatoteke = QFileDialog::getOpenFileName(this, tr("Datoteka sa koordinatama tacaka"), "../Geometrijski_Algoritmi/input_files/", "*.*");
    if (imeDatoteke.isEmpty())
        return;

    _imeDatoteke = imeDatoteke.toStdString();

    napraviNoviAlgoritam(RAZLOG_UCITAVANJE);
    animacijaButtonAktivni(true);
}

void MainWindow::on_Nasumicni_dugme_clicked()
{
    if (ui->brojNasumicniTacaka->text() != "" )
    {
        _broj_nasumicnih_tacaka = ui->brojNasumicniTacaka->text().toInt();
    }

    napraviNoviAlgoritam(RAZLOG_NASUMICNE);
    animacijaButtonAktivni(true);
}

void MainWindow::on_Ponisti_dugme_clicked()
{
    ui->tipAlgoritma->setCurrentIndex(0);
    animacijaButtonAktivni(false);
    animacijaParametriButtonAktivni(true);

    _pOblastCrtanja->set_obrisiSve(true);
    _pOblastCrtanja->update();

    _pOblastCrtanjaOpenGL->set_obrisiSve(true);
    _pOblastCrtanjaOpenGL->update();

    ui->brojNasumicniTacaka->clear();
    ui->brojNasumicniTacaka->setPlaceholderText("Uneti broj nasumicnih tacaka, podrazumevana vrednost je 20.");

    ui->nUdaljenih->clear();
    ui->nUdaljenih->setPlaceholderText("N najblizih tacaka, mora biti <= od broja nasumicnih tacaka ili broja zadataih tacaka");
    ui->pointX->clear();
    ui->pointX->setPlaceholderText("Zadataka tacka x koordinata");
    ui->pointY->clear();
    ui->pointY->setPlaceholderText("Zadataka tacka y koordinata");
    ui->nUdaljenih->setVisible(false);
    ui->pointX->setVisible(false);
    ui->pointY->setVisible(false);
}

void MainWindow::on_Zapocni_dugme_clicked()
{
    ui->Zapocni_dugme->setEnabled(false);
    animacijaParametriButtonAktivni(false);

    if (_pAlgoritamBaza)
        _pAlgoritamBaza->pokreniAnimaciju();
}

void MainWindow::on_Zaustavi_dugme_clicked()
{
    if (_pAlgoritamBaza)
        _pAlgoritamBaza->pauzirajIliNastaviAnimaciju();
}

void MainWindow::on_Sledeci_dugme_clicked()
{
    if (_pAlgoritamBaza)
        _pAlgoritamBaza->sledeciKorakAnimacije();
}

void MainWindow::on_Ispocetka_dugme_clicked()
{
    ui->Zapocni_dugme->setEnabled(false);
    ui->Zaustavi_dugme->setEnabled(true);
    ui->Sledeci_dugme->setEnabled(true);

    if (_pAlgoritamBaza)
    {
        _pAlgoritamBaza->zaustaviAnimaciju();
        napraviNoviAlgoritam(RAZLOG_ISPOCETKA);
        _pAlgoritamBaza->pokreniAnimaciju();
    }
}

void MainWindow::on_tipAlgoritma_currentIndexChanged(int index)
{
    QString trenutniAlg = ui->tipAlgoritma->itemText(index);

    if (trenutniAlg == "SA CASOVA VEZBI" || trenutniAlg == "STUDENTSKI PROJEKTI")
    {
        ui->datoteka_dugme->setEnabled(false);
        ui->Nasumicni_dugme->setEnabled(false);
    }
    else
    {
        if(trenutniAlg == "KdTree")
        {
            ui->nUdaljenih->setVisible(true);
            ui->pointX->setVisible(true);
            ui->pointY->setVisible(true);
        }
        ui->datoteka_dugme->setEnabled(true);
        ui->Nasumicni_dugme->setEnabled(true);
    }

    bool ort = (trenutniAlg == "Ortogonalni upiti");
    ui->lineEditX1->setVisible(ort);
    ui->lineEditX2->setVisible(ort);
    ui->lineEditY1->setVisible(ort);
    ui->lineEditY2->setVisible(ort);
    ui->labelOpseg->setVisible(ort);

    bool isTriangulacija = (trenutniAlg == Triangulacija2::tipAlgoritma);
    ui->naivniCheck->setVisible(isTriangulacija);

    if(isTriangulacija)
    {
        ui->naivniCheck->setCheckState(Qt::CheckState::Unchecked);
    }

    animacijaButtonAktivni(false);
}

void MainWindow::on_merenjeButton_clicked()
{
    QString tipAlgoritma = ui->tipAlgoritma->currentText();

    if(tipAlgoritma == Triangulacija2::tipAlgoritma)
    {
        _mThread = new TimeMeasurementThread(tipAlgoritma, Triangulacija2::MINDIM, Triangulacija2::ALGSTEP, Triangulacija2::MAXDIM);
    }
    else
    {
        _mThread = new TimeMeasurementThread(tipAlgoritma, MIN_DIM, STEP, MAX_DIM);
    }

    connect(_mThread, &TimeMeasurementThread::updateChart, this, &MainWindow::on_lineSeriesChange);
    _mThread->start();
}

void MainWindow::na_krajuAnimacije()
{
    ui->tipAlgoritma->setEnabled(true);
    ui->Ponisti_dugme->setEnabled(true);
    animacijaButtonAktivni(false);
    ui->Ispocetka_dugme->setEnabled(true);
}

void MainWindow::animacijaButtonAktivni(bool param_aktivnosti)
{
    ui->Ispocetka_dugme->setEnabled(param_aktivnosti);
    ui->Sledeci_dugme->setEnabled(param_aktivnosti);
    ui->Zaustavi_dugme->setEnabled(param_aktivnosti);
    ui->Zapocni_dugme->setEnabled(param_aktivnosti);
}

void MainWindow::animacijaParametriButtonAktivni(bool param_aktivnosti)
{
    ui->datoteka_dugme->setEnabled(!param_aktivnosti);
    ui->Ponisti_dugme->setEnabled(param_aktivnosti);
    ui->tipAlgoritma->setEnabled(param_aktivnosti);
    ui->Nasumicni_dugme->setEnabled(!param_aktivnosti);
}

void MainWindow::napraviNoviAlgoritam(RazlogPravljenjaAlg razlog)
{
    _pOblastCrtanja->set_obrisiSve(false);
    _pOblastCrtanjaOpenGL->set_obrisiSve(false);

    // U slucaju ortogonalnih upita, zelimo da vrsimo vise upita nad istim skupom tacaka
    if (razlog != RAZLOG_ISPOCETKA && !dynamic_cast<OrtogonalniUpiti *>(_pAlgoritamBaza)) {
        delete _pAlgoritamBaza;
        _pAlgoritamBaza = nullptr;
    }

    QString tipAlgoritma = ui->tipAlgoritma->currentText();

    if (tipAlgoritma == "Demonstracija iscrtavanja")
        _pAlgoritamBaza = new DemoIscrtavanja(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == Triangulacija2::tipAlgoritma)
        _pAlgoritamBaza = new Triangulacija2(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka, _naivni);
    else if (tipAlgoritma == "Brisuca prava")
        _pAlgoritamBaza = new BrisucaPrava(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Konveksni omotac")
        _pAlgoritamBaza = new KonveksniOmotac(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
     else if (tipAlgoritma == "Konveksni omotac 3D")
        _pAlgoritamBaza = new KonveksniOmotac3D(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Preseci duzi")
        _pAlgoritamBaza = new PreseciDuzi(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "DCEL Demo")
        _pAlgoritamBaza = new DCELDemo(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Voronoji dijagram")
        _pAlgoritamBaza = new VoronojiDijagram(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Unija pravougaonika")
        _pAlgoritamBaza = new UnijaPravougaonika(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Hertel-Mehlhorn")
        _pAlgoritamBaza = new HertelMehlhorn(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Trilateracija")
     _pAlgoritamBaza = new Trilateracija(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "Kolinearne tacke")
     _pAlgoritamBaza = new KolinearneTacke(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka);
    else if (tipAlgoritma == "KdTree"){
        if(ui->nUdaljenih->text()!="" && ui->pointX->text()!="" && ui->pointY->text()!="") {
            int n= ui->nUdaljenih->text().toInt();
            int pointX=ui->pointX->text().toInt();
            int pointY=ui->pointY->text().toInt();
            _pAlgoritamBaza = new KdTree(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze,_imeDatoteke, _broj_nasumicnih_tacaka, n,QPoint(pointX,pointY));
        }
        else {
            _pAlgoritamBaza = new KdTree(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze,_imeDatoteke, _broj_nasumicnih_tacaka);
        }
    }
    else if (tipAlgoritma == "Ortogonalni upiti") {
        auto rb = findChild<QRubberBand*>();
        if (rb != nullptr) {
            rb->resize(QSize());
            delete rb;
        }
        if (razlog != RAZLOG_ISPOCETKA) {
            _pAlgoritamBaza = new OrtogonalniUpiti(_pOblastCrtanja, _pOblastCrtanjaOpenGL, _duzinaPauze, _imeDatoteke, _broj_nasumicnih_tacaka, razlog);
        }
        else {
            // Samo ponovo pretrazi isto stablo opsega
            auto upiti = static_cast<OrtogonalniUpiti *>(_pAlgoritamBaza);
            upiti->anulirajQRubberBand();
            upiti->pokreniAlgoritam();
        }
        _pOblastCrtanja->repaint();
    }
    if (_pAlgoritamBaza)
    {
        if (_pAlgoritamBaza->is_3D() == false)
            _pOblastCrtanja->postaviAlgoritamKojiSeIzvrsava(_pAlgoritamBaza);
        else
            _pOblastCrtanjaOpenGL->postaviAlgoritamKojiSeIzvrsava(_pAlgoritamBaza);

        // U slucaju ponovnog iskoriscavanja instance nekog od algoritama, ne dozvoli ponovnu konekciju!
        connect(_pAlgoritamBaza, SIGNAL(animacijaZavrsila()), this, SLOT(na_krajuAnimacije()), Qt::UniqueConnection);
    }
}



void MainWindow::on_lineSeriesChange(double dim, double optimal, double naive)
{

    _optimalSeries->append(dim, optimal);
    _naiveSeries->append(dim, naive);
}

void MainWindow::on_naivniCheck_stateChanged(int arg1)
{
    _naivni = ui->naivniCheck->checkState() == Qt::CheckState::Checked;
}
