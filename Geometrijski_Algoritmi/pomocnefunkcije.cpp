#include "pomocnefunkcije.h"

#include <cmath>

namespace pomocnefunkcije {

double povrsinaTrougla2(QPoint a, QPoint b, QPoint c)
{
    return (b.x() - a.x())*(c.y() - a.y()) -
           (c.x() - a.x())*(b.y() - a.y());
}

double rastojanje(QPoint a, QPoint b)
{
    return sqrt(pow(a.x() - b.x(), 2) + pow(a.y() - b.y(), 2));
}

bool konveksan(QPoint a, QPoint b, QPoint c)
{
    return povrsinaTrougla2(a, b, c) > 0;
}

bool kolinearne3D(QVector3D a, QVector3D b, QVector3D c)
{
    return ((c.z() - a.z()) * (b.y() - a.y()) -
              (b.z() - a.z()) * (c.y() - a.y()) == 0) &&
           ((b.z() - a.z()) * (c.x() - a.x()) -
              (b.x() - a.x()) * (c.z() - a.z()) == 0) &&
           ((b.x() - a.x()) * (c.y() - a.y()) -
              (b.y() - a.y()) * (c.x() - a.x()) == 0);
}


double zapremina(QVector3D a, QVector3D b, QVector3D c, QVector3D d)
{
    double	vol;
    double  bxdx, bydy, bzdz, cxdx, cydy, czdz;

    bxdx=b.x()-d.x();
    bydy=b.y()-d.y();
    bzdz=b.z()-d.z();
    cxdx=c.x()-d.x();
    cydy=c.y()-d.y();
    czdz=c.z()-d.z();
    vol =    (a.z()-d.z())*(bxdx*cydy-bydy*cxdx)
          +  (a.y()-d.y())*(bzdz*cxdx-bxdx*czdz)
          +  (a.x()-d.x())*(bydy*czdz-bzdz*cydy);

    if (fabs(vol) < 0.0001)
        return 0;
    else
        return vol;
}

bool presekDuzi(QLineF l1, QLineF l2, QPointF &presek)
{
    double k1 = (l1.p1().y() - l1.p2().y())/(l1.p1().x() - l1.p2().x());
    double k2 = (l2.p1().y() - l2.p2().y())/(l2.p1().x() - l2.p2().x());

    double n1 = l1.p1().y() - k1*l1.p1().x();
    double n2 = l2.p1().y() - k2*l2.p1().x();

    double dx = (n2-n1)/(k1-k2);

    //Ovim smo izracunali presek dve prave, koje su odredjene duzima l1 i l2.
    //Treba proveriti da li presek pripada zaista duzima (moze biti na pravi, ali van duzi),
    //u suprotnom se ovaj presek zanemaruje
    //zato return na kraju
    presek = QPointF(dx, k1*dx + n1);

    return duzSadrziTacku(l1, presek) && duzSadrziTacku(l2, presek);
}

bool duzSadrziTacku(QLineF l, QPointF p)
{
    if(l.p1().x() < l.p2().x())
    {
        if(p.x() < l.p1().x() || p.x() > l.p2().x())
            return false;
    }else{
        if(p.x() > l.p1().x() || p.x() < l.p2().x())
            return false;
    }

    if(l.p1().y() < l.p2().y()){
        if(p.y() < l.p1().y() || p.y() > l.p2().y())
            return false;
    }else{
        if(p.y() > l.p1().y() || p.y() < l.p2().y())
            return false;
    }

    return true;
}

bool ispod(QPoint a, QPoint b)
{
    if(a.y() < b.y())
        return true;
    else if(a.y() == b.y())
    {
       if(a.x() < b.x()) return true;
    }
    return false;
}

}


