#include "oblastcrtanja.h"

#include "algoritambaza.h"
#include "algoritmi_studentski_projekti/ga07_ortogonalni_upiti.h"

#include <QRubberBand>

OblastCrtanja::OblastCrtanja(QWidget *parent)
    : QWidget(parent), _pAlgoritamBaza(nullptr), _obrisiSve(false)
{
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Qt::white);
    setAutoFillBackground(true);
    setPalette(pal);

    QPainter painter(this);
    // da bi koordinatni sistem pocinjao u donjem levom uglu
    painter.translate(this->rect().bottomLeft());
    painter.scale(1.0, -1.0);
    _transformacija = painter.matrix();

    setMouseTracking(true);
}

void OblastCrtanja::postaviAlgoritamKojiSeIzvrsava(AlgoritamBaza *pAlgoritamBaza)
{
    _pAlgoritamBaza = pAlgoritamBaza;
}

void OblastCrtanja::set_obrisiSve(bool param)
{
    _obrisiSve = param;
}

const QMatrix OblastCrtanja::get_transformacija() const
{
    return _transformacija;
}

void OblastCrtanja::paintEvent(QPaintEvent *)
{
    QPainter qpainter(this);

    // da bi koordinatni sistem pocinjao u donjem levom uglu
    qpainter.translate(this->rect().bottomLeft());
    qpainter.scale(1.0, -1.0);
    _transformacija = qpainter.matrix();

    qpainter.setRenderHint(QPainter::Antialiasing, true);

    QPen p = qpainter.pen();
    p.setColor(Qt::black);
    p.setWidth(3);
    p.setCapStyle(Qt::RoundCap);

    qpainter.setPen(p);

    if (_obrisiSve == false)
    {
        qpainter.drawRect(0, 0, width() - 1, height() - 1);

        if (_pAlgoritamBaza)
            _pAlgoritamBaza->crtajAlgoritam(qpainter);
    }
    else
    {
        qpainter.eraseRect(0, 0, width() - 1, height() - 1);
        auto upiti = static_cast<OrtogonalniUpiti *>(_pAlgoritamBaza);
        if (upiti == nullptr)
            return;
        auto rb = findChild<QRubberBand*>();
        if (rb != nullptr) {
            rb->resize(QSize());
            delete rb;
        }
        upiti->anulirajQRubberBand();
    }
}
